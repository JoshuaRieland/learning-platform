<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'login';

$route['user/signin'] = 'login/signin';
$route['user/signout'] = 'login/signout';
$route['user/update_password'] = 'login/update_password';

$route['student/message_board'] = 'student/message_board';
$route['message_board/send'] = 'student/send_message';
$route['message/viewed'] = 'student/message_viewed';
$route['message/reply'] = 'student/message_reply';
$route['message/download_attachment'] = 'student/download_attachment';

$route['student/new_register'] = 'register/new_register';
$route['student/register1'] = 'register/step_1';
$route['student/register2'] = 'register/step_2';
$route['student/register3'] = 'register/step_3';
$route['student/drop_meeting'] = 'register/drop_meeting';

$route['student/dashboard'] = 'student/dashboard';
$route['student/schedule'] = 'student/schedule';
$route['student/change_schedule'] = 'student/change_schedule';
$route['student/drop_meeting_db'] = 'student/drop_meeting';

$route['mentor/schedule'] = 'register/set_mentor_times';
$route['mentor/clear_blackouts'] = 'register/clear_blackouts';
$route['mentor/all_blackouts'] = 'register/all_blackouts';

$route['admin/dashboard'] = 'admin/dashboard';

$route['admin/show_students'] = 'admin/show_students';
$route['admin/add_students'] = 'admin/add_students';
$route['admin/analyze_students'] = 'admin/analyze_students';
$route['admin/register_new_students'] = 'admin/register_new_students';

$route['admin/show_mentors'] = 'admin/show_mentors';
$route['admin/add_mentors'] = 'admin/add_mentors';
$route['admin/analyze_mentors'] = 'admin/analyze_mentors';

$route['admin/show_chapters'] = 'admin/show_chapters';
$route['admin/add_chapters'] = 'admin/add_chapters';
$route['admin/analyze_chapters'] = 'admin/analyze_chapters';

$route['admin/show_lessons'] = 'admin/show_lessons';
$route['admin/add_lessons'] = 'admin/add_lessons';
$route['admin/register_new_lesson'] = 'admin/register_new_lesson';
$route['admin/register_new_quiz'] = 'admin/register_new_quiz';
$route['admin/analyze_lessons'] = 'admin/analyze_lessons';

$route['course/chapter'] = 'course/goto_chapter';
$route['course/lesson'] = 'course/goto_lesson';
$route['course/quiz'] = 'course/grade_quiz';

$route['goto/step1'] = 'register/goto_step_1';
$route['goto/step2'] = 'register/goto_step_2';
$route['goto/step3'] = 'register/goto_step_3';
$route['goto/step4'] = 'register/goto_step_4';
$route['goto/step5'] = 'register/goto_step_5';
$route['goto/learning-platform'] = 'register/platform';

$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
