<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

	// ADMIN NAVIGATION FUNCTIONS

	public function dashboard(){
		$this->load->view('admin_dashboard');
	}

	// STUDENT FUNCTIONS

	public function show_students(){
		$all_students = $this->admin_model->get_all_students();
		$this->load->view('admin_show_students', array('all_students' => $all_students));
	}

	public function add_students(){
		$this->load->view('admin_add_students');
	}

	public function analyze_students(){
		die('In Admin - Analyze Students');
	}

	public function register_new_student(){
		$new_students_email = $this->input->post('student_email');
		$new_students_start_date = $this->input->post('student_start_date');
		$new_students_program_length = $this->input->post('student_program_length');
		for($idx = 0; $idx < count($new_students_email); $idx++){
			$randPW = random_string2();
			$this->admin_model->register_new_student($new_students_email[$idx], $randPW, $new_students_start_date[$idx], $new_students_program_length[$idx]);
		}
		
	}	

	// MENTOR FUNCTIONS
	
	public function show_mentors(){
		die('In Admin - Show Mentors');
	}

	public function add_mentors(){
		die('In Admin - Add Mentors');
	}

	public function analyze_mentors(){
		die('In Admin - Analyze Mentors');
	}

	// CHAPTER FUNCTIONS
	
	public function show_chapters(){
		die('In Admin - Show Chapters');
	}

	public function add_chapters(){
		die('In Admin - Add Chapters');
	}

	public function analyze_chapters(){
		die('In Admin - Analyze Chapters');
	}

	// LESSON FUNCTIONS
	
	public function show_lessons(){
		$subject = $this->input->post('subject');
		$chapter = $this->input->post('chapter');
		$lesson = $this->input->post('lesson');
		$lesson_ordered = $this->admin_model->get_one_lesson($subject, $chapter, $lesson);
		$this->load->view('preview_lesson', array('lesson' => $lesson_ordered));
	}

	public function add_lessons(){
		$current_chapters_lessons = $this->admin_model->get_current_chapters_lessons();
		$placement = 0;
		$lesson_images = $this->admin_model->get_all_lesson_images();
		$subject_list = $this->admin_model->get_subject_list();
		$chapter_list = $this->admin_model->get_chapter_list();
		$lesson_list = $this->admin_model->get_lesson_list();
		$TorF = ['True', 'False'];
        $this->load->view('admin_add_lesson', array('lesson_images' => $lesson_images, 
        											'placement' => $placement, 
        											'current_chapters_lessons' => $current_chapters_lessons,
        											'subject_list' => $subject_list,
        											'chapter_list' => $chapter_list,
        											'lesson_list' => $lesson_list,
        											'TorF' => $TorF
        											));
		
	}

	public function register_new_lesson(){
		$post = $this->input->post();
		// die(var_dump($post));
		
		// Seperate Lesson Content For Database
		for($idx = 0; $idx < count($post)-9; $idx++){
			$lesson[] = [$post['subject'], $post['subject_title'],
					 	 $post['chapter'], $post['chapter_title'], 
					 	 $post['lesson'], $post['lesson_title'], 
					 	 $post[$idx][0], $post[$idx][2], $post[$idx][1]
					 	 ];
		}
		$lesson[] = 'End of Lesson';
		// Send Each Lesson Content Section To Database
		for($index = 0; $index < count($post)-9; $index++){
			if($lesson[$index][7] == 'b2i' OR $lesson[$index][7] == 'b3i' OR $lesson[$index][7] == 'b4i'){
				$img_roll = join("/&/",$lesson[$index][8]);
				$lesson[$index][8] = $img_roll;
			}
			$this->admin_model->add_lesson($lesson[$index]);
		}

		// // Seperate Quiz Questions For Database
		// for($index = 0; $index < count($post['question']['question']); $index++){
		// 	$quiz_question[] = [$post['subject'],
		// 						$post['chapter'],
		// 						$post['lesson'],
		// 						$post['question']['question'][$index], 
		// 						$post['question']['answer1'][$index], 
		// 						$post['question']['answer2'][$index], 
		// 						$post['question']['answer3'][$index], 
		// 						$post['question']['answer4'][$index],
		// 						$post['question']['correct'][$index]
		// 						];
		// }

		// // Send Each Quiz Question/Answers To Database
		// for($indx = 0; $indx < count($quiz_question); $indx++){
		// 	$this->admin_model->add_quiz_question($quiz_question[$indx]);
		// }

		// // Send Lesson Assignment To Database
		// $lesson_project = [ $post['subject'], $post['chapter'], 
		// 					$post['lesson'], $post['project_title'], 
		// 					$post['project_description']
		// 				   ];
		// $this->admin_model->add_lesson_project($lesson_project);


		// Get New Lesson Content, Order By Position and Display On Preview Page
		$lesson_ordered = $this->admin_model->get_one_lesson($post['subject'], $post['chapter'], $post['lesson']);
		$this->load->view('preview_lesson', array('lesson' => $lesson_ordered));
	}

	public function analyze_lessons(){
		die('In Admin - Analyze Lessons');
	}

	public function register_new_quiz(){
		// die(var_dump($this->input->post()));
		$post = $this->input->post();
		// die(var_dump(count($post['question']['question'])));
		for($index = 0; $index < count($post['question']['question']); $index++){
			$quiz_question[] = [$post['subject'],
								$post['chapter'],
								$post['lesson'],
								$post['question']['question'][$index], 
								$post['question']['answer1'][$index], 
								$post['question']['answer2'][$index], 
								$post['question']['answer3'][$index], 
								$post['question']['answer4'][$index],
								$post['question']['correct'][$index]
								];
		}

		// Send Each Quiz Question/Answers To Database
		for($indx = 0; $indx < count($quiz_question); $indx++){
			$this->admin_model->add_quiz_question($quiz_question[$indx]);
		}
	}

}
