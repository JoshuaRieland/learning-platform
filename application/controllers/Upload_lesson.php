<?php

class Upload_lesson extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
        }

       
        public function do_upload()
        {
        		$chapter = $this->input->post('chapter');
        		$lesson = $this->input->post('lesson');
        		$image_title = $this->input->post('image_title');
        		// die(var_dump($chapter.'/'.$lesson.'/'.$image_title));
                
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['file_name']			= 'chapter'.$chapter.'_lesson'.$lesson.'_'.$image_title;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                        $this->load->view('admin_add_new_lesson');
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $photo_name = $data['upload_data']['file_name'];
                        // die(var_dump($photo_name));
                 	 	$this->load->view('admin_add_lesson');             
                }
        }

     

       
}
?>