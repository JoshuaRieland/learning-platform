<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register extends CI_Controller {

	// REGISTRATION STUDENT

	public function new_register(){
		$register_info = $this->input->post();
		$register_info['randPW'] = random_string2();
		$userID = $this->register_model->new_student_register($register_info);
		$_SESSION['userID'] = $userID['id'];
		$_SESSION['user_email'] = $register_info['email'];
		$_SESSION['access'] = 'student';
		$_SESSION['mentorID'] = -1;
		$_SESSION['registration_step1'] = 'current';
		$_SESSION['registration_step2'] = 'incomplete';
		$_SESSION['registration_step3'] = 'incomplete';
		$_SESSION['registration_step4'] = 'incomplete';
		$_SESSION['registration_step5'] = 'incomplete';
		$_SESSION['program_length'] = $register_info['program_length'];
		$_SESSION['status'] = 'active';

		$this->load->view('update_password');
	}

	public function step_1(){
		$step_1_info = $this->input->post();
		$update_student = $this->register_model->update_step1_check();
		if($update_student == null){
			$registered = $this->register_model->step_1($step_1_info);
		}
		else{
			$step_1_info['studentID'] = $update_student['id'];
			$registered = $this->register_model->update_step_1($step_1_info);
		}
		if($registered){
			die('working');
			$completed = $this->register_model->complete_step_1();
			if($completed){
				$_SESSION['registration_step1'] == 'completed';
				$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
				$mentor_schedule = $this->schedule_model->get_mentor_schedule();
				$this->load->view('registration_step_2');
			}
			else{
				die('Error with db connection');
			}
		}
		else{
			die('Error with db connection');
		}
	}

	public function step_2(){
		$step_2_info = $this->input->post();
		$mentor_assigned = INTVAL($_SESSION['mentorID']);

		
		// IF NOT PREVIOUSLY ASSIGNED √
		if(INTVAL($mentor_assigned == -1)){
			$registered = $this->register_model->step_2($step_2_info);
			$_SESSION['mentorID'] = $mentor_assigned;
			if($registered){
				$completed = $this->register_model->complete_step_2();	
				if($completed){
					$_SESSION['registration_step2'] == 'completed';
					$student_meeting_days = $this->schedule_model->get_student_days();
				$meeting_count = 0;
				foreach($student_meeting_days AS $student){
					if($student != 'none'){
						$meeting_count += 1;
					}
				}
				$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
				$mentor_schedule = $this->schedule_model->get_mentor_schedule();
				$mentor_info = $this->register_model->get_mentor_info();
				$current_count = $this->count_current_meetings($mentor_schedule);
				// $this->load->view('registration_step_3', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));

				}else{die('Error with db connection');}
			}else{die('Error with db connection');}
		}

		// IF CURRENT MENTOR AND SELECT MENTOR ARE SAME
		if(INTVAL($mentor_assigned) === INTVAL($step_2_info['mentorID'])){
			$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
			$mentor_schedule = $this->schedule_model->get_mentor_schedule();
			$mentor_info = $this->register_model->get_mentor_info();
			$current_count = $this->count_current_meetings($mentor_schedule);
			$this->load->view('registration_step_3', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info));
		}

		// IF NEW MENTOR IS SELECTED - REPLACE OLD MENTOR - ADJUST STUDENT COUNTS
		else{
			$mentor['current'] = INTVAL($_SESSION['mentorID']);
			$mentor['new'] = INTVAL($step_2_info['mentorID']);
			$mentor_counts = $this->register_model->step_2_get_mentor_counts($mentor);
			$update_mentor['current_mentor'] = INTVAL($mentor_counts['current']['id']);
			$update_mentor['new_mentor'] = INTVAL($mentor_counts['new']['id']); 
			$update_mentor['current_count'] = INTVAL($mentor_counts['current']['student_count']) - 1;
			$update_mentor['new_count'] = INTVAL($mentor_counts['new']['student_count']) + 1;
			$step_2_info['mentorID'] = INTVAL($step_2_info['mentorID']);

			$registered = $this->register_model->step_2($step_2_info);
			if($registered){
				$_SESSION['mentorID'] = $step_2_info['mentorID'];
				$updated = $this->register_model->update_mentor_counts($update_mentor['current_mentor'], $update_mentor['current_count']);
				if($updated){
					$updated2 = $this->register_model->update_mentor_counts($update_mentor['new_mentor'], $update_mentor['new_count']);
					if($updated2){
						
						$student_meeting_days = $this->schedule_model->get_student_days();
						$meeting_count = 0;
						foreach($student_meeting_days AS $student){
							if($student != 'none'){
								$meeting_count += 1;
							}
						}
						$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
						$mentor_schedule = $this->schedule_model->get_mentor_schedule();
						$mentor_info = $this->register_model->get_mentor_info();
						$current_count = $this->count_current_meetings($mentor_schedule);
						$this->load->view('registration_step_3', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));
					}else{die('Error with db connection');}
				}else{die('Error with db connection');}
			}else{die('Error with db connection');}
		}
	}

	public function step_3(){
		$meetings = $this->input->post();
		$mentor_info = $this->register_model->get_mentor_info();
		$meetings_user = $_SESSION['userID'];
		$meetings_mentor = $_SESSION['mentorID'];

		if(isset($meetings['monday_time_slot'])){
			$monday_meeting = explode('/', $meetings['monday_time_slot']);
			$this->schedule_model->schedule_monday_meeting($meetings_user, $meetings_mentor, $monday_meeting[1]);
			$meeting_days[] = 'monday';
			$meeting_times[] = $monday_meeting[1];
		}
		if(isset($meetings['tuesday_time_slot'])){
			$tuesday_meeting = explode('/', $meetings['tuesday_time_slot']);
			$this->schedule_model->schedule_tuesday_meeting($meetings_user, $meetings_mentor, $tuesday_meeting[1]);
			$meeting_days[] = 'tuesday';
			$meeting_times[] = $tuesday_meeting[1];
		}
		if(isset($meetings['wednesday_time_slot'])){
			$wednesday_meeting = explode('/', $meetings['wednesday_time_slot']);
			$this->schedule_model->schedule_wednesday_meeting($meetings_user, $meetings_mentor, $wednesday_meeting[1]);
			$meeting_days[] = 'wednesday';
			$meeting_times[] = $wednesday_meeting[1];
		}
		if(isset($meetings['thursday_time_slot'])){
			$thursday_meeting = explode('/', $meetings['thursday_time_slot']);
			$this->schedule_model->schedule_thursday_meeting($meetings_user, $meetings_mentor, $thursday_meeting[1]);
			$meeting_days[] = 'thursday';
			$meeting_times[] = $thursday_meeting[1];
		}
		if(isset($meetings['friday_time_slot'])){
			$friday_meeting = explode('/', $meetings['friday_time_slot']);
			$this->schedule_model->schedule_friday_meeting($meetings_user, $meetings_mentor, $friday_meeting[1]);
			$meeting_days[] = 'friday';
			$meeting_times[] = $friday_meeting[1];
		}
		if(isset($meetings['saturday_time_slot'])){
			$saturday_meeting = explode('/', $meetings['monday_time_slot']);
			$this->schedule_model->schedule_saturday_meeting($meetings_user, $meetings_mentor, $saturday_meeting[1]);
			$meeting_days[] = 'saturday';
			$meeting_times[] = $saturday_meeting[1];
		}

		for($idx = 0; $idx < count($meeting_days); $idx++){
				switch($meeting_days[$idx]){
					case 'monday':
						$this->schedule_model->update_monday($meeting_times[$idx]);
						break;
					case 'tuesday':
						$this->schedule_model->update_tuesday($meeting_times[$idx]);
						break;
					case 'wednesday':
						$this->schedule_model->update_wednesday($meeting_times[$idx]);
						break;
					case 'thursday':
						$this->schedule_model->update_thursday($meeting_times[$idx]);
						break;
					case 'friday':
						$this->schedule_model->update_friday($meeting_times[$idx]);
						break;
					case 'saturday':
						$this->schedule_model->update_saturday($meeting_times[$idx]);
						break;				
				};
		}

		$_SESSION['registration_step3'] = 'completed';
		$completed = $this->register_model->complete_step_3();
		if($completed){
			$this->load->view('registration_step_4');
		}else{
			die('Error with connection to database');
		}
	}

	public function drop_meeting(){
		$day = $this->input->post('day');
		$time_slot = $this->input->post('time_slot');
		if($day == 'monday'){
			$this->schedule_model->drop_monday($time_slot);	
		}
		if($day == 'tuesday'){
			$this->schedule_model->drop_tuesday($time_slot);
		}
		if($day == 'wednesday'){
			$this->schedule_model->drop_wednesday($time_slot);	
		}
		if($day == 'thursday'){
			$this->schedule_model->drop_thursday($time_slot);	
		}
		if($day == 'friday'){
			$this->schedule_model->drop_friday($time_slot);	
		}
		if($day == 'saturday'){
			$this->schedule_model->drop_saturday($time_slot);
		}
		// $this->goto_step_3();
		$student_meeting_days = $this->schedule_model->get_student_days();
		$meeting_count = 0;
		foreach($student_meeting_days AS $student){
			if($student != 'none'){
				$meeting_count += 1;
			}
		}
		$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
		$mentor_info = $this->register_model->get_mentor_info();
		$mentor_schedule = $this->schedule_model->get_mentor_schedule();
		$current_count = $this->count_current_meetings($mentor_schedule);
		$this->load->view('registration_step_3', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));
	}

	// REGISTRATION MENTOR

	public function set_mentor_times(){
		$monday = $this->input->post('monday_time_slot');
		$tuesday = $this->input->post('tuesday_time_slot');
		$wednesday = $this->input->post('wednesday_time_slot');
		$thursday = $this->input->post('thursday_time_slot');
		$friday = $this->input->post('friday_time_slot');
		$saturday = $this->input->post('saturday_time_slot');

		if($monday != null){
			for($idx = 0; $idx < count($monday); $idx++){
				$this->mentor_schedule_model->mentor_block_monday_time($monday[$idx]);
			}
		}
		if($tuesday != null){
			for($idx = 0; $idx < count($tuesday); $idx++){
				$this->mentor_schedule_model->mentor_block_tuesday_time($tuesday[$idx]);
			}
		}
		if($wednesday != null){
			for($idx = 0; $idx < count($wednesday); $idx++){
				$this->mentor_schedule_model->mentor_block_wednesday_time($wednesday[$idx]);
			}
		}
		if($thursday != null){
			for($idx = 0; $idx < count($thursday); $idx++){
				$this->mentor_schedule_model->mentor_block_thursday_time($thursday[$idx]);
			}
		}
		if($friday != null){
			for($idx = 0; $idx < count($friday); $idx++){
				$this->mentor_schedule_model->mentor_block_friday_time($friday[$idx]);
			}
		}
		if($saturday != null){
			for($idx = 0; $idx < count($saturday); $idx++){
				$this->mentor_schedule_model->mentor_block_saturday_time($saturday[$idx]);
			}
		}
		$mentor_id = $this->register_model->get_mentor_id();
		$_SESSION['mentorID'] = $mentor_id;
		$mentor_schedule = $this->schedule_model->get_mentor_schedule_mentor();
		$this->register_model->mentor_complete_registration();
		$this->load->view('mentor_dashboard');
		// $this->load->view('mentor_registration2', array('mentor_schedule' => $mentor_schedule));
	}

	public function clear_blackouts(){
		$day = $this->input->post('day');
		$mentor_schedule = $this->mentor_schedule_model->get_mentor_schedule();
		$times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'];
		switch($day){
			case 'monday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['monday'][$times[$idx]] == -99){
						$this->mentor_schedule_model->clear_monday_blackout($times[$idx]);
					}
				}
				break;
			case 'tuesday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['tuesday'][$times[$idx]] == -99){
						$this->mentor_schedule_model->clear_tuesday_blackout($times[$idx]);
					}
				}
				break;
			case 'wednesday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['wednesday'][$times[$idx]] == -99){
						$this->mentor_schedule_model->clear_wednesday_blackout($times[$idx]);
					}
				}
				break;
			case 'thursday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['thursday'][$times[$idx]] == -99){
						$this->mentor_schedule_model->clear_thursday_blackout($times[$idx]);
					}
				}
				break;
			case 'friday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['friday'][$times[$idx]] == -99){
						$this->mentor_schedule_model->clear_friday_blackout($times[$idx]);
					}
				}
				break;
			case 'saturday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['saturday'][$times[$idx]] == -99){
						$this->mentor_schedule_model->clear_saturday_blackout($times[$idx]);
					}
				}
				break;
		}
		$mentor_id = $this->register_model->get_mentor_id();
		$_SESSION['mentorID'] = $mentor_id;
		$mentor_schedule = $this->schedule_model->get_mentor_schedule_mentor();
		$this->load->view('mentor_registration2', array('mentor_schedule' => $mentor_schedule));
	}

	public function all_blackouts(){
		$day = $this->input->post('day');
		$mentor_schedule = $this->mentor_schedule_model->get_mentor_schedule();
		$times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'];
		switch($day){
			case 'monday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['monday'][$times[$idx]] == -1){
						$this->mentor_schedule_model->mentor_block_monday_time($times[$idx]);
					}
				}
				break;
			case 'tuesday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['tuesday'][$times[$idx]] == -1){
						$this->mentor_schedule_model->mentor_block_tuesday_time($times[$idx]);
					}
				}
				break;
			case 'wednesday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['wednesday'][$times[$idx]] == -1){
						$this->mentor_schedule_model->mentor_block_wednesday_time($times[$idx]);
					}
				}
				break;
			case 'thursday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['thursday'][$times[$idx]] == -1){
						$this->mentor_schedule_model->mentor_block_thursday_time($times[$idx]);
					}
				}
				break;
			case 'friday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['friday'][$times[$idx]] == -1){
						$this->mentor_schedule_model->mentor_block_friday_time($times[$idx]);
					}
				}
				break;
			case 'saturday':
				for($idx = 0; $idx < 48; $idx++){
					if($mentor_schedule['saturday'][$times[$idx]] == -1){
						$this->mentor_schedule_model->mentor_block_saturday_time($times[$idx]);
					}
				}
				break;
		}
		$mentor_id = $this->register_model->get_mentor_id();
		$_SESSION['mentorID'] = $mentor_id;
		$mentor_schedule = $this->schedule_model->get_mentor_schedule_mentor();
		$this->load->view('mentor_registration2', array('mentor_schedule' => $mentor_schedule));
	}

	// REGISTRATION NAVIGATION
	public function goto_step_1(){
		$student_info = $this->register_model->get_student_info();
		$this->load->view('registration_step_1', array('student_info' => $student_info));
	}
	public function goto_step_2(){
		$all_mentors = $this->register_model->get_all_mentors();
		$mentor_info = $this->register_model->get_mentor_info();
		$this->load->view('registration_step_2', array('all_mentors' => $all_mentors, 'mentor_info' => $mentor_info));
	}
	public function goto_step_3(){
		$student_meeting_days = $this->schedule_model->get_student_days();
		$meeting_count = 0;
		foreach($student_meeting_days AS $student){
			if($student != 'none'){
				$meeting_count += 1;
			}
		}
		$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
		$mentor_info = $this->register_model->get_mentor_info();
		$mentor_schedule = $this->schedule_model->get_mentor_schedule();
		$current_count = $this->count_current_meetings($mentor_schedule);
		$this->load->view('registration_step_3', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));
	}
	public function goto_step_4(){
		$completed = $this->register_model->complete_step_3();
		if($completed){
			$this->load->view('registration_step_4');
		}else{
			die('Error with connection to database');
		}
	}
	public function goto_step_5(){
		$completed = $this->register_model->complete_step_4();
		if($completed){
			$this->load->view('registration_step_5');
		}else{
			die('Error with connection to database');
		}
	}

	public function count_current_meetings($mentor_schedule){
			$times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'];
		 	$current_count = 0;
			for($idx = 0; $idx < 48; $idx++){
				if($mentor_schedule['monday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['tuesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
		    	if($mentor_schedule['wednesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['thursday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['friday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['saturday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
			}
			return $current_count;
	}

	public function platform(){
					$completed = $this->register_model->complete_step_5();
					$all_chapters = $this->course_model->get_all_chapters();
					$mentor_info = $this->register_model->get_mentor_info();
					$student_info = $this->register_model->get_student_info();
					$student_meeting_days = $this->schedule_model->get_student_days();
					$subject_list = $this->course_model->get_subject_list();
					$chapter_list = $this->course_model->get_all_chapters();
					$all_lesson_list = $this->course_model->get_all_lessons();
					$this->load->view('student_dashboard', array('student_info' => $student_info, 'student_meeting_days' => $student_meeting_days, 'subject_list' => $subject_list, 'mentor_info' => $mentor_info, 'all_chapters' => $all_chapters, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list));
	}

}
