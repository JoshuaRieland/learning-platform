<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student extends CI_Controller {

	public function dashboard(){
		$all_chapters = $this->course_model->get_all_chapters();
					$mentor_info = $this->register_model->get_mentor_info();
					$student_info = $this->register_model->get_student_info();
					$student_meeting_days = $this->schedule_model->get_student_days();
					$subject_list = $this->course_model->get_subject_list();
					// $lesson_list = $this->course_model->get_chapter_lessons($subject, $chapter);
		$chapter_list = $this->course_model->get_all_chapters();
		$all_lesson_list = $this->course_model->get_all_lessons();
					$this->load->view('student_dashboard', array('student_info' => $student_info, 'student_meeting_days' => $student_meeting_days, 'subject_list' => $subject_list, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list, 'mentor_info' => $mentor_info, 'all_chapters' => $all_chapters));
	}

	public function message_board(){
		$_SESSION['upload_attachment_msg'] = '';
		$mentor_userID = $this->student_model->get_mentor_userID($_SESSION['mentorID']);
		$inbox = $this->student_model->get_inbox();
		$outbox = $this->student_model->get_outbox();
		$subject_list = $this->course_model->get_subject_list();
		$chapter_list = $this->course_model->get_all_chapters();
		$all_lesson_list = $this->course_model->get_all_lessons();
		$this->load->view('message_board', array('mentor_userID' => $mentor_userID, 
												 'inbox' => $inbox, 
												 'outbox' => $outbox, 
												 'subject_list' => $subject_list,
												 'chapter_list' => $chapter_list,
												 'all_lesson_list' => $all_lesson_list
												 ));
	}

	public function send_message(){
		$message = $this->input->post();
		
		die(var_dump($message));
		$message['viewed'] = 'no';
		$message['attachment'] = 'none'; 
		$this->student_model->send_message($message);
		$this->message_board();
	}

	public function schedule(){
				$student_meeting_days = $this->schedule_model->get_student_days();
				$meeting_count = 0;
				foreach($student_meeting_days AS $student){
					if($student != 'none'){
						$meeting_count += 1;
					}
				}
				$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
				$mentor_schedule = $this->schedule_model->get_mentor_schedule();
				$mentor_info = $this->register_model->get_mentor_info();
				$current_count = $this->count_current_meetings($mentor_schedule);
				$this->load->view('student_schedule', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));
	}

	public function count_current_meetings($mentor_schedule){
			$times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'];
		 	$current_count = 0;
			for($idx = 0; $idx < 48; $idx++){
				if($mentor_schedule['monday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['tuesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
		    	if($mentor_schedule['wednesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['thursday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['friday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['saturday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
			}
			return $current_count;
	}

	public function change_schedule(){
		$meetings = $this->input->post();
		$mentor_info = $this->register_model->get_mentor_info();
		$meetings_user = $_SESSION['userID'];
		$meetings_mentor = $_SESSION['mentorID'];

		if(isset($meetings['monday_time_slot'])){
			$monday_meeting = explode('/', $meetings['monday_time_slot']);
			$this->schedule_model->schedule_monday_meeting($meetings_user, $meetings_mentor, $monday_meeting[1]);
			$meeting_days[] = 'monday';
			$meeting_times[] = $monday_meeting[1];
		}
		if(isset($meetings['tuesday_time_slot'])){
			$tuesday_meeting = explode('/', $meetings['tuesday_time_slot']);
			$this->schedule_model->schedule_tuesday_meeting($meetings_user, $meetings_mentor, $tuesday_meeting[1]);
			$meeting_days[] = 'tuesday';
			$meeting_times[] = $tuesday_meeting[1];
		}
		if(isset($meetings['wednesday_time_slot'])){
			$wednesday_meeting = explode('/', $meetings['wednesday_time_slot']);
			$this->schedule_model->schedule_wednesday_meeting($meetings_user, $meetings_mentor, $wednesday_meeting[1]);
			$meeting_days[] = 'wednesday';
			$meeting_times[] = $wednesday_meeting[1];
		}
		if(isset($meetings['thursday_time_slot'])){
			$thursday_meeting = explode('/', $meetings['thursday_time_slot']);
			$this->schedule_model->schedule_thursday_meeting($meetings_user, $meetings_mentor, $thursday_meeting[1]);
			$meeting_days[] = 'thursday';
			$meeting_times[] = $thursday_meeting[1];
		}
		if(isset($meetings['friday_time_slot'])){
			$friday_meeting = explode('/', $meetings['friday_time_slot']);
			$this->schedule_model->schedule_friday_meeting($meetings_user, $meetings_mentor, $friday_meeting[1]);
			$meeting_days[] = 'friday';
			$meeting_times[] = $friday_meeting[1];
		}
		if(isset($meetings['saturday_time_slot'])){
			$saturday_meeting = explode('/', $meetings['monday_time_slot']);
			$this->schedule_model->schedule_saturday_meeting($meetings_user, $meetings_mentor, $saturday_meeting[1]);
			$meeting_days[] = 'saturday';
			$meeting_times[] = $saturday_meeting[1];
		}

		for($idx = 0; $idx < count($meeting_days); $idx++){
				switch($meeting_days[$idx]){
					case 'monday':
						$this->schedule_model->update_monday($meeting_times[$idx]);
						break;
					case 'tuesday':
						$this->schedule_model->update_tuesday($meeting_times[$idx]);
						break;
					case 'wednesday':
						$this->schedule_model->update_wednesday($meeting_times[$idx]);
						break;
					case 'thursday':
						$this->schedule_model->update_thursday($meeting_times[$idx]);
						break;
					case 'friday':
						$this->schedule_model->update_friday($meeting_times[$idx]);
						break;
					case 'saturday':
						$this->schedule_model->update_saturday($meeting_times[$idx]);
						break;				
				};
		}
		$this->dashboard();
	}

	public function drop_meeting(){
		$day = $this->input->post('day');
		$time_slot = $this->input->post('time_slot');
		if($day == 'monday'){
			$this->schedule_model->drop_monday($time_slot);	
		}
		if($day == 'tuesday'){
			$this->schedule_model->drop_tuesday($time_slot);
		}
		if($day == 'wednesday'){
			$this->schedule_model->drop_wednesday($time_slot);	
		}
		if($day == 'thursday'){
			$this->schedule_model->drop_thursday($time_slot);	
		}
		if($day == 'friday'){
			$this->schedule_model->drop_friday($time_slot);	
		}
		if($day == 'saturday'){
			$this->schedule_model->drop_saturday($time_slot);
		}
		// $this->goto_step_3();
		$student_meeting_days = $this->schedule_model->get_student_days();
		$meeting_count = 0;
		foreach($student_meeting_days AS $student){
			if($student != 'none'){
				$meeting_count += 1;
			}
		}
		$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
		$mentor_info = $this->register_model->get_mentor_info();
		$mentor_schedule = $this->schedule_model->get_mentor_schedule();
		$current_count = $this->count_current_meetings($mentor_schedule);
		$this->load->view('student_schedule', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));
	}

	public function message_viewed(){
		$info = $_GET['message_id'];
		$this->student_model->message_viewed($info);
		$this->message_board();
	}

	public function message_reply(){
		$reply = $this->input->post(); //Send_from_id Send_to_id Subject Content
		$this->student_model->message_reply($reply);
		$this->message_board();
	}

	public function download_attachment(){
			// die(var_dump($this->input->post()));
			$this->load->helper('download');
			$attachment = $this->input->post('attachment');
			//set the textfile's content 
			$data = file_get_contents('./uploads/'.$attachment);

			//set the textfile's name
			$name = '$attachment';
			//use this function to force the session/browser to download the created file
			force_download($name, $data);

	}

}
