<?php

class Upload extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
        }

        public function do_upload_lesson_image()
        {
                        $chapter = $this->input->post('chapter');
                        $lesson = $this->input->post('lesson');
                        $image_title = $this->input->post('image_title');
                        // die(var_dump($chapter.'/'.$lesson.'/'.$image_title));
                
                $config['upload_path']          = './lesson_images/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['max_size']             = 10000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['file_name']                    = 'CH'.$chapter.'_'.$lesson.'_'.$image_title;

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                       $upload_status = 'Failed';
                }
                else
                {
                        $upload_status = 'Success';
                        $data = array('upload_data' => $this->upload->data());
                        $photo_name = $data['upload_data']['file_name'];
                        $this->admin_model->add_lesson_image($photo_name);
                        // $lesson_images = $this->admin_model->get_all_lesson_images();
                        // $this->load->view('admin_add_lesson', array('lesson_images' => $lesson_images));             
                }
                 $current_chapters_lessons = $this->admin_model->get_current_chapters_lessons();
                        $placement = 0;
                        $lesson_images = $this->admin_model->get_all_lesson_images();
                        $subject_list = $this->admin_model->get_subject_list();
                        $chapter_list = $this->admin_model->get_chapter_list();
                        $lesson_list = $this->admin_model->get_lesson_list();
                        $TorF = ['True', 'False'];
                        $this->load->view('admin_add_lesson', array('lesson_images' => $lesson_images, 
                                                                    'placement' => $placement, 
                                                                    'current_chapters_lessons' => $current_chapters_lessons,
                                                                    'subject_list' => $subject_list,
                                                                    'chapter_list' => $chapter_list,
                                                                    'lesson_list' => $lesson_list,
                                                                    'TorF' => $TorF,
                                                                    'upload_status' => $upload_status
                                                                    ));
        }

        public function do_upload_mentor()
        {
                $step_1_mentor_info = $this->input->post();
                $this->step_1_mentor($step_1_mentor_info);

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['file_name']            = 'mentor_profile_pic_'.$_SESSION['userID'];

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                        $this->load->view('mentor_registration');
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $photo_name = $data['upload_data']['file_name'];
                        $this->register_model->update_profile_pic_mentor($photo_name);
                        $days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
                        $mentor_schedule = $this->schedule_model->get_mentor_schedule_mentor();
                        $this->load->view('mentor_registration2', array('days' => $days, 'mentor_schedule' => $mentor_schedule));
                }
        }
       

        public function do_upload()
        {
                $this->step_1();

                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpg|jpeg|png';
                $config['max_size']             = 1000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                $config['file_name']            = 'student_profile_pic_'.$_SESSION['userID'].'#';

                $this->load->library('upload', $config);

                if ( ! $this->upload->do_upload())
                {
                        $student_info = $this->register_model->get_student_info();
                        $this->load->view('registration_step_1', array('student_info' => $student_info));
                }
                else
                {
                        $data = array('upload_data' => $this->upload->data());
                        $photo_name = $data['upload_data']['file_name'];
                        $this->register_model->update_profile_pic($photo_name);
                        // die(var_dump($data['upload_data']['file_name']));
                        $student_meeting_days = $this->schedule_model->get_student_days();
                      
                        // $meeting_count = 0;
                        // foreach($student_meeting_days AS $student){
                                // if($student != 'none'){
                                        // $meeting_count += 1;
                                // }
                        // }
                        // $days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
                        // $mentor_info = $this->register_model->get_mentor_info();
                        // $mentor_schedule = $this->schedule_model->get_mentor_schedule();
                        // $current_count = $this->count_current_meetings($mentor_schedule);
                        // $this->load->view('registration_step_2', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));

                        $all_mentors = $this->register_model->get_all_mentors();
                        $mentor_info = $this->register_model->get_mentor_info();
                        $this->load->view('registration_step_2', array('all_mentors' => $all_mentors, 'mentor_info' => $mentor_info));
                }
        }

        public function count_current_meetings($mentor_schedule){
                $times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'];
                $current_count = 0;
                for($idx = 0; $idx < 48; $idx++){
                        if($mentor_schedule['monday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
                        if($mentor_schedule['tuesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
                if($mentor_schedule['wednesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
                        if($mentor_schedule['thursday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
                        if($mentor_schedule['friday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
                        if($mentor_schedule['saturday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
                }
                return $current_count;
        }

        public function step_1(){
                $step_1_info = $this->input->post();
                $update_student = $this->register_model->update_step1_check();
                if($update_student == null){
                        $registered = $this->register_model->step_1($step_1_info);
                }
                else{
                        $step_1_info['studentID'] = $update_student['id'];
                        $registered = $this->register_model->update_step_1($step_1_info);
                }
                if($registered){
                        $completed = $this->register_model->complete_step_1();
                        if($completed){
                                $_SESSION['registration_step1'] == 'completed';
                                $days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
                                $mentor_schedule = $this->schedule_model->get_mentor_schedule();
                                return;
                        }
                        else{
                                die('Error with db connection');
                        }
                }
                else{
                        die('Error with db connection');
                }
        }

        public function step_1_mentor($step_1_mentor_info){
                $id = $this->register_model->update_step1_mentor_check();

                if($id == null){
                        $this->register_model->insert_mentor_info($step_1_mentor_info);
                        $this->register_model->complete_step_1();
                        return;
                }else{
                        $this->register_model->update_mentor_info($id, $step_1_mentor_info);
                        $this->register_model->complete_step_1();
                        return;
                }
        }  

       
}
?>