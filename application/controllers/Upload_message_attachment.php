<?php

class Upload_message_attachment extends CI_Controller {

        public function __construct()
        {
                parent::__construct();
                $this->load->helper(array('form', 'url'));
        }

        public function do_upload()
        {
                $config['upload_path']          = './uploads/';
                $config['allowed_types']        = 'jpg|jpeg|png|doc|docx|log|rtf|txt|csv|dat|xml|xls|xlsx|html|css|zip';
                $config['max_size']             = 100000;
                $config['max_width']            = 1024;
                $config['max_height']           = 1024;
                // $config['file_name']            = 'student_message_attachment_#'.$_SESSION['userID'].'#';
               
                if($this->input->post('attachment') == 'yes'){
                        $this->load->library('upload', $config);
                        $data = array('upload_data' => $this->upload->data());
                        $attachment_name = $data['upload_data']['file_name']; 
                        if ( ! $this->upload->do_upload())
                        {
                                $_SESSION['upload_attachment_msg'] = 'Unable to upload attachment';
                                $mentor_userID = $this->student_model->get_mentor_userID($_SESSION['mentorID']);
                                $this->load->view('message_board', array('mentor_userID' => $mentor_userID));
                        }
                        else
                        {
                                $_SESSION['upload_attachment_msg'] = '';
                                $data = array('upload_data' => $this->upload->data());
                                $attachment_name = $data['upload_data']['file_name'];
                                $this->message_with_attachment($attachment_name);
                        }
                }else{
                        $this->message_no_attachment();
                }
        

        }

        public function message_with_attachment($attachment_name){
                $message = $this->input->post();
                $message['attachment'] = $attachment_name;
                $message['viewed'] = 'no';
                $this->student_model->send_message($message);
                $this->message_board();
        }

        public function message_no_attachment(){
                $message = $this->input->post();
                $message['viewed'] = 'no';
                $message['attachment'] = 'none'; 
                $this->student_model->send_message($message);
                $this->message_board();
        }
        
        public function message_board(){
                $_SESSION['upload_attachment_msg'] = '';
                $mentor_userID = $this->student_model->get_mentor_userID($_SESSION['mentorID']);
                $inbox = $this->student_model->get_inbox();
                $outbox = $this->student_model->get_outbox();
                $subject_list = $this->course_model->get_subject_list();
                $chapter_list = $this->course_model->get_all_chapters();
                $all_lesson_list = $this->course_model->get_all_lessons();
                $this->load->view('message_board', array('mentor_userID' => $mentor_userID, 'inbox' => $inbox, 'outbox' => $outbox, 'subject_list' => $subject_list, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list));
        }

  
       
}
?>