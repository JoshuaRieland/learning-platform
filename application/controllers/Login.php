<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	public function index(){
		if(!isset($_SESSION)){
		session_start();
		}
		$this->load->view('landing_page');
	}

	public function signin(){
		$login_attempt = $this->input->post();
		$login_credentials = $this->login_model->getLoginCredentials($login_attempt['email']);
		if($login_credentials == null || ($login_credentials['salt'] == 'unsalted' && $login_attempt['password'] != $login_credentials['password'])){
			$this->load->view('unknown_user');
		}
		else{
			$_SESSION['userID'] = INTVAL($login_credentials['id']);
			$_SESSION['user_email'] = $login_attempt['email'];
			$_SESSION['access'] = $login_credentials['access'];
			$_SESSION['mentorID'] = INTVAL($login_credentials['mentorID']);
			$_SESSION['registration_step1'] = $login_credentials['registration_step1'];
			$_SESSION['registration_step2'] = $login_credentials['registration_step2'];
			$_SESSION['registration_step3'] = $login_credentials['registration_step3'];
			$_SESSION['registration_step4'] = $login_credentials['registration_step4'];
			$_SESSION['registration_step5'] = $login_credentials['registration_step5'];
			$_SESSION['program_length'] = INTVAL($login_credentials['program_length']);
			$_SESSION['status'] = 'active';

		 	if($login_credentials['salt'] == 'unsalted'){
				$this->load->view('update_password');
			}
			else if($login_credentials['salt'] != 'unsalted'){
				$password_check = crypt($login_attempt['password'], $login_credentials['salt']);

				if($password_check != $login_credentials['password']){
					$this->load->view('unknown_user');
				}
				else{
					$this->dashboard_selection();
				}
			}
		}	


	}

	public function update_password(){
		$password_info = $this->input->post();
		$salt = random_string();
		$password_info['salt'] = $salt;
		$password_info['password'] = crypt($password_info['password'], $salt);
		$updated = $this->login_model->updatePassword($password_info);
		if($updated){
			$this->dashboard_selection();
		}
		else{
			die('Error with the db connection come back later.');
		}
	}

	public function dashboard_selection(){
		if($_SESSION['access'] == 'student'){
			$_SESSION['student_info'] = $this->register_model->get_student_info();
		}else if ($_SESSION['access'] == 'mentor'){
			$_SESSION['mentor_info'] = $this->register_model->get_mentor_info();
		}



		if($_SESSION['access'] == 'student' && $_SESSION['registration_step5'] != 'completed'){
			if($_SESSION['registration_step1'] == 'current'){
				$this->load->view('registration_step_1', array('student_info' => $_SESSION['student_info']));
			}
			else if($_SESSION['access'] == 'student' && $_SESSION['registration_step2'] == 'current'){
				$all_mentors = $this->register_model->get_all_mentors();
				$mentor_info = $this->register_model->get_mentor_info();
				$this->load->view('registration_step_2', array('all_mentors' => $all_mentors, 'mentor_info' => $mentor_info));
			}
			else if($_SESSION['access'] == 'student' && $_SESSION['registration_step3'] == 'current'){
				$student_meeting_days = $this->schedule_model->get_student_days();
				$meeting_count = 0;
				foreach($student_meeting_days AS $student){
					if($student != 'none'){
						$meeting_count += 1;
					}
				}
				$days = ['Mon', 'Tues', 'Wed', 'Thur', 'Fri', 'Sat'];
				$mentor_schedule = $this->schedule_model->get_mentor_schedule();
				$mentor_info = $this->register_model->get_mentor_info();
				$current_count = $this->count_current_meetings($mentor_schedule);
				$this->load->view('registration_step_3', array('days' => $days, 'mentor_schedule' => $mentor_schedule, 'current_count' => $current_count, 'mentor_info' => $mentor_info, 'student_meeting_days' => $student_meeting_days, 'meeting_count' => $meeting_count));
			}
			else if($_SESSION['access'] == 'student' && $_SESSION['registration_step4'] == 'current'){
				$this->load->view('registration_step_4');
			}
			else if($_SESSION['access'] == 'student' && $_SESSION['registration_step5'] == 'current'){
				$this->load->view('registration_step_5');
			}
		}
		else{
			switch($_SESSION['access']){
				case 'student':
					$all_chapters = $this->course_model->get_all_chapters();
					$mentor_info = $this->register_model->get_mentor_info();
					$student_info = $this->register_model->get_student_info();
					$student_meeting_days = $this->schedule_model->get_student_days();
					$subject_list = $this->course_model->get_subject_list();
					$chapter_list = $this->course_model->get_all_chapters();
					$all_lesson_list = $this->course_model->get_all_lessons();
					$this->load->view('student_dashboard', array('student_info' => $student_info, 'student_meeting_days' => $student_meeting_days, 'subject_list' => $subject_list, 'mentor_info' => $mentor_info, 'all_chapters' => $all_chapters, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list));
					break;
				case 'mentor':
					if($_SESSION['registration_step1'] != 'completed'){
						$this->load->view('mentor_registration');
					}else if($_SESSION['registration_step2'] != 'completed'){
						$mentor_id = $this->register_model->get_mentor_id();
						$_SESSION['mentorID'] = $mentor_id;
						$mentor_schedule = $this->schedule_model->get_mentor_schedule_mentor();
						$this->load->view('mentor_registration2', array('mentor_schedule' => $mentor_schedule));
					}else{
						$this->load->view('mentor_dashboard');
					}
					break;
				case 'admin':
					$this->load->view('admin_dashboard');
					break;			
			}
		}
	}

	public function signout(){
		session_destroy();
		$this->load->view('landing_page');
	}

	public function count_current_meetings($mentor_schedule){
			$times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'];
		 	$current_count = 0;
			for($idx = 0; $idx < 48; $idx++){
				if($mentor_schedule['monday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['tuesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
		    	if($mentor_schedule['wednesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['thursday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['friday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
				if($mentor_schedule['saturday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;}
			}
			return $current_count;
	}
	
}
