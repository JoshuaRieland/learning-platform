<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course extends CI_Controller {

	public function goto_chapter(){
		$subject = $this->input->post('subject');
		$chapter = $this->input->post('chapter'); 
		$chapter_content = $this->course_model->get_one_chapter($subject, $chapter);
		$lesson_list = $this->course_model->get_chapter_lessons($subject, $chapter);
		$subject_list = $this->course_model->get_subject_list();
		$chapter_list = $this->course_model->get_all_chapters();
		$all_lesson_list = $this->course_model->get_all_lessons();
		$this->load->view('course_chapter', array('chapter_content' => $chapter_content, 'lesson_list' => $lesson_list, 'subject_list' => $subject_list, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list));
	}

	public function goto_lesson(){
		$subject = $this->input->post('subject');
		$chapter = $this->input->post('chapter');
		$lesson = $this->input->post('lesson');
		$quiz_lesson = $subject.'_'.$chapter.'_'.$lesson;
		$chapter_content = $this->course_model->get_one_lesson($subject, $chapter, $lesson);
		$lesson_list = $this->course_model->get_chapter_lessons($subject, $chapter);
		$subject_list = $this->course_model->get_subject_list();
		$chapter_list = $this->course_model->get_all_chapters();
		$all_lesson_list = $this->course_model->get_all_lessons();
		$quiz = $this->course_model->get_quiz($subject, $chapter, $lesson);
		$quiz_score = $this->course_model->get_quiz_score($quiz_lesson);
		$this->load->view('course_chapter', array('chapter_content' => $chapter_content, 'lesson_list' => $lesson_list, 'subject_list' => $subject_list, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list, 'quiz' => $quiz, 'quiz_score' => $quiz_score));
	}

	public function grade_quiz(){
		$score = 0;
		$total = ((count($this->input->post())-4)/2); 		// Get total # of Q/A combinations
		for($idx = 1; $idx <= $total; $idx++){
			$question = $this->input->post('question'.$idx);
			$answer = $this->input->post('answer'.$idx);
			if($question == $answer){
				$score++;
			}
		}
		$grade = ($score/$total);							// Get final grade for quiz
		$quiz_lesson = $this->input->post('quiz_lesson');
		$this->course_model->quiz_grade($grade, $quiz_lesson);
		
		$subject = $this->input->post('subject');
		$chapter = $this->input->post('chapter');
		$lesson = $this->input->post('lesson');
		$this->quiz_goto_lesson($subject, $chapter, $lesson, $quiz_lesson);
	}

	public function quiz_goto_lesson($subject, $chapter, $lesson, $quiz_lesson){
		$chapter_content = $this->course_model->get_one_lesson($subject, $chapter, $lesson);
		$lesson_list = $this->course_model->get_chapter_lessons($subject, $chapter);
		$subject_list = $this->course_model->get_subject_list();
		$chapter_list = $this->course_model->get_all_chapters();
		$all_lesson_list = $this->course_model->get_all_lessons();
		$quiz = $this->course_model->get_quiz($subject, $chapter, $lesson);
		$quiz_score = $this->course_model->get_quiz_score($quiz_lesson);
		$this->load->view('course_chapter', array('chapter_content' => $chapter_content, 'lesson_list' => $lesson_list, 'subject_list' => $subject_list, 'chapter_list' => $chapter_list, 'all_lesson_list' => $all_lesson_list, 'quiz' => $quiz, 'quiz_score' => $quiz_score));
	}

}
