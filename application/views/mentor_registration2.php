<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Mentor Scheduling</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/step_bar.css">
	<script src="/assets/js/JQueryLib.js"></script>
	
	

</head>
<BODY>
	<?php $current_month = Date('M'); ?>
	<?php $weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']?>
	<?php $times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'] ?>
	<?php $timesFull = ['12:00 AM', '12:30 AM', '1:00 AM', '1:30 AM','2:00 AM', '2:30 AM', '3:00 AM', '3:30 AM', '4:00 AM', '4:30 AM', '5:00 AM', '5:30 AM', '6:00 AM', '6:30 AM', '7:00 AM', '7:30 AM', '8:00 AM', '8:30 AM', '9:00 AM', '9:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '1:00 PM', '1:30 PM','2:00 PM', '2:30 PM', '3:00 PM', '3:30 PM', '4:00 PM', '4:30 PM', '5:00 PM', '5:30 PM', '6:00 PM', '6:30 PM', '7:00 PM', '7:30 PM', '8:00 PM', '8:30 PM', '9:00 PM', '9:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', '11:30 PM'] ?>
	<?php $days = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday'] ?>
	<div class='registration_container'>	
		
		<a href="/user/signout" class='pull-right'><h3>LogOut <i class='glyphicon glyphicon-log-out'></i></h3></a>	
		
		<div class='row' style='margin-top: 50px;'>
			<div style='border: 2px solid black; padding: 5px; text-align: center; width: 400px;'>
				<h2>Set Your Availability</h2>
				<h2>For Student Meetings</h2>
			</div>
		</div>
		
		<form action='/mentor/schedule' method='POST' >
			<input type='submit' value='Set Black Out Times' style='width: 20%; margin-left: 40%; margin-top: 15px;'>
			<div class='row' style='margin-top: 10px; height: 450px; border: 2px solid black; padding: 5px; overflow-y: scroll;'>	
				<table class='table table-responsive' style='table-layout: fixed;'>
					<thead>
						<th class='schedule_table_cell'>Time</th>
						<?php foreach($days AS $day){ ?>
							<div id='<?=$day?>' class='calendar_header'>
								<th style='font-size: 12px; text-align: center; border: 2px solid black'><?=$day?></th>
							</div>
						<?php } ?>
					</thead>
					<tbody>
						
						<?php for($idx = 0; $idx < 48; $idx++){ ?>
							<tr>
								<th class='schedule_table_cell'><?= $timesFull[$idx] ?></th>
							
							<!-- Monday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if(INTVAL($mentor_schedule['monday'][$times[$idx]]) == -1){ ?>
										<input type='checkbox' name='monday_time_slot[]' value='<?= $times[$idx] ?>'>
									<?php }else if(INTVAL($mentor_schedule['monday'][$times[$idx]]) == -99){ ?>
										Black Out
									<?php }else if(INTVAL($mentor_schedule['monday'][$times[$idx]]) > 0){ ?>
										<p  style='background-color: yellow;'>Booked</p>
									<?php } ?>				
								</th>
							
							<!-- Tuesday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) == -1){ ?>
										<input type='checkbox' name='tuesday_time_slot[]' value='<?= $times[$idx] ?>'>
									<?php }else if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) == -99){ ?>
										Black Out
									<?php }else if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) > 0){ ?>
										<p  style='background-color: yellow;'>Booked</p>
									<?php } ?>
								</th>

							<!-- Wednesday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) == -1){ ?>
										<input type='checkbox' name='wednesday_time_slot[]' value='<?= $times[$idx] ?>'>
									<?php }else if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) == -99){ ?>
										Black Out
									<?php }else if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) > 0){ ?>
										<p  style='background-color: yellow;'>Booked</p>
									<?php } ?>
								</th>

							<!-- Thursday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) == -1){ ?>
										<input type='checkbox' name='thursday_time_slot[]' value='<?= $times[$idx] ?>'>
									<?php }else if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) == -99){ ?>
										Black Out
									<?php }else if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) > 0){ ?>
										<p  style='background-color: yellow;'>Booked</p>
									<?php } ?>
								</th>

							<!-- Friday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if(INTVAL($mentor_schedule['friday'][$times[$idx]]) == -1){ ?>
										<input type='checkbox' name='friday_time_slot[]' value='<?= $times[$idx] ?>'>
									<?php }else if(INTVAL($mentor_schedule['friday'][$times[$idx]]) == -99){ ?>
										Black Out
									<?php }else if(INTVAL($mentor_schedule['friday'][$times[$idx]]) > 0){ ?>
										<p  style='background-color: yellow;'>Booked</p>
									<?php } ?>
								</th>

							<!-- Saturday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) == '-1'){ ?>
										<input type='checkbox' name='saturday_time_slot[]' value='<?= $times[$idx] ?>'>
									<?php }else if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) == -99){ ?>
										Black Out
									<?php }else if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) > 0){ ?>
										<p  style='background-color: yellow;'>Booked</p>
									<?php } ?>
								</th>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</form>
		<table class='table table-responsive' style='table-layout: fixed;'>
			<thead>
				<tr>
					<th class='schedule_table_cell'></th>
						<?php foreach($days AS $day){ ?>
							<div id='<?=$day?>' class='calendar_header'>
								<th style='font-size: 12px; text-align: center;'>
									<form action='/mentor/clear_blackouts' method='POST'>
										<input type='hidden' name='day' value='<?=$day?>'>
										<input type='submit' value='Clear Black Outs'>
									</form>
								</th>
							</div>
						<?php } ?>
				</tr>
				<tr>
					<th class='schedule_table_cell'></th>
						<?php foreach($days AS $day){ ?>
							<div id='<?=$day?>_off' class='calendar_header'>
								<th style='font-size: 12px; text-align: center;'>
									<form action='/mentor/all_blackouts' method='POST'>
										<input type='hidden' name='day' value='<?=$day?>'>
										<input type='submit' value='Day Off'>
									</form>
								</th>
							</div>
						<?php } ?>
				</tr>
			</thead>
		</table>	


	</div>
</body>
</html>


