<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Dashboard</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/student_dashboard.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
	<style type="text/css">
		.dashboard_div{
			display: inline-block;
			width: 60%;
			vertical-align: top;
			min-width: 500px;
		}
		.dashboard_div_large{
			display: inline-block;
			width: 80%;
			vertical-align: top;
			margin: 10%;
		}
		#mailbox_body{
			min-width: 800px;
		}
	</style>
</head>
<body id='mailbox_body'>
	<div class='registration_container'>
		<?php $this->load->view('navigation_student'); ?>
		<?php $this->load->view('student_sidebar_dashboard'); ?>
		
		<div class='dashboard_div_large' id='dashboard_content'>
			
			<!-- WELCOME MESSAGE -->
			<div class='row'>
				<div class='welcome_message'>
					<h1 class='centerText fantasy'>Student Dashboard</h1>
					<h2 class='centerText fantasy'>Welcome <?=$student_info['first_name']?>&nbsp<?=$student_info['last_name']?></h2>
				</div>
			</div>

			<!-- PROFILE PICTURE -->
			<!-- <div>
				<div id='profile_pic_div'>
					<?php if($student_info['profile_pic'] != 'none'){ ?>
						<img src="/uploads/<?=$student_info['profile_pic']?>" class='profile_picture pull-right'>
					<?php }else{ ?>
						<?php if($student_info['gender'] == 'M'){ ?>
							<img src="/assets/images/male_silhouette.png" class='profile_picture pull-right'>
						<?php }else if($student_info['gender'] == 'F'){ ?>
							<img src="/assets/images/female_silhouette.png" class='profile_picture pull-right'>
						<?php }else{ ?>
							<img src="/assets/images/silhouette.png" class='profile_picture pull-right'>
						<?php } ?>
					<?php } ?>
				</div>
			</div> -->
			<hr>
			<div class='row'>
				<div class='col-xs-12 col-xs-offset-0 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2' id='mentor_connection'>
					<div class='col-xs-6 col-xs-offset-3 col-sm-3 col-sm-offset-0' id='mentor_img_div'>
						<img src="/assets/images/silhouette.png" id='mentor_image' style='margin-left: -25px;'>
					</div>
					<div class='col-xs-8 col-xs-offset-2 col-sm-offset-1'>
						<p><?=$mentor_info['bio']?></p>

						
					</div>
				</div>

			</div>
			<div class='row' id='mentor_communication'>
					<div class='col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-1'>
						<fieldset style='width: 100%; height: 200px; border: 1px solid black;'>
							<legend class='centerText'>Meeting Schedule</legend>
							<?php if ($student_meeting_days['monday'] != 'none'){?>
								<p class='my_meetings'>Mondays: <?= $student_meeting_days['monday'] ?></p>
						<?php } ?>
						<?php if ($student_meeting_days['tuesday'] != 'none'){?>
								<p class='my_meetings'>Tuesdays: <?= $student_meeting_days['tuesday'] ?></p>
						<?php } ?>
						<?php if ($student_meeting_days['wednesday'] != 'none'){?>
								<p class='my_meetings'>Wednesdays: <?= $student_meeting_days['wednesday'] ?></p>
						<?php } ?>
						<?php if ($student_meeting_days['thursday'] != 'none'){?>
								<p class='my_meetings'>Thursdays: <?= $student_meeting_days['thursday'] ?></p>
						<?php } ?>
						<?php if ($student_meeting_days['friday'] != 'none'){?>
								<p class='my_meetings'>Fridays: <?= $student_meeting_days['friday'] ?></p>
						<?php } ?>
						<?php if ($student_meeting_days['saturday'] != 'none'){?>
								<p class='my_meetings'>Saturdays: <?= $student_meeting_days['saturday'] ?></p>
						<?php } ?>
						<a href="/student/schedule"><button class='mentor_action_button'>Change Schedule</button></a>
						</fieldset>
					</div>
					<div class='col-xs-10 col-xs-offset-1 col-sm-6 col-sm-offset-0 col-md-4 col-md-offset-1'>
						<fieldset>
							<legend class='centerText'>Communications</legend>
								<p class='centerText'>New Messages: #</p>
								<a href="/student/message_board"><button class='mentor_action_button'>Mailbox</button></a>
								<a href="#"><button class='mentor_action_button'>Discussions - Class</button></a>
								<a href="#"><button class='mentor_action_button'>Discussions - General</button></a>
						</fieldset>
					</div>
			</div>
			<hr>
		</div>
	</div> <!-- END CONTAINER -->
</body>
</html>
