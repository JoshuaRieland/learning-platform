<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Admin Dashboard - New Students</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#addField').click(function(){
				$('#moreFields').append("<div class='form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'><input type='email' class='col-md-6 form-control' name='student_email[]' placeholder='New Student Email' required><br><input type='date' name='student_start_date[]' class='col-md-6 form-control' required><select name='student_program_length[]' class='col-md-6 form-control'><option value='6 Weeks'>6 Weeks</option><option value='12 Weeks'>12 Weeks</option></select></div>");
			});
		});
	</script>

</head>
<body style='padding: 50px;'>
	<div class='row'>
		<a href="/admin/dashboard"><button class='pull-right'>Admin Dashboard</button></a>
	</div>
	<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
		<button id='addField'>Add Another Student</button>
	</div>
	<form action='/admin/register_new_student' method='POST'>
		
		 	<div class='form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
		 		<input type='email' name='student_email[]' class='col-md-6 form-control' placeholder='New Student Email' required>
		 		
		 		<input type='date' name='student_start_date[]' class='col-md-6 form-control' required>

		 		<select name='student_program_length[]' class='col-md-6 form-control'>
		 			<option value='6 Weeks'>6 Weeks</option>
		 			<option value='12 Weeks'>12 Weeks</option>
		 		</select>

		 		<!-- ADD PROGRAM LENGTH SELECTION / START DATE -->
		 	</div>
		  
		  <p id='moreFields'></p>

		  	<div class='form-group col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
		  		<input type='submit' value='Submit New Students'>
		  	</div>
	</form>
	
</body>
</html>

