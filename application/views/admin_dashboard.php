<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Admin Dashboard</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	

</head>
<body>
	<div class='registration_container'>
		<a href="/user/signout" class='pull-right'><h3>LogOut <i class='glyphicon glyphicon-log-out'></i></h3></a>	
		<div class='col-xs-12 col-sm-10 col-sm-offset-1 col-md-6 col-md-offset-3'>
			<ul class='admin_links'>
				<li>
					<fieldset>
						<legend>Students</legend>
						<ul class='admin_links'>
							<li><a href="/admin/show_students">Show All Students</a></li>
							<li><a href="/admin/add_students">Add New Students</a></li>
							<li><a href="/admin/analyze_students">Analyze Students</a></li>
						</ul>
					</fieldset>
				</li>
				<li>
					<fieldset>
						<legend>Mentors</legend>
						<ul class='admin_links'>
							<li><a href="/admin/show_mentors">Show All Mentors</a></li>
							<li><a href="/admin/add_mentors">Add New Mentors</a></li>
							<li><a href="/admin/analyze_mentors">Analyze Mentors</a></li>
						</ul>
					</fieldset>
				</li>
				<!-- <li>
					<fieldset>
						<legend>Course Chapters</legend>
						<ul class='admin_links'>
							<li><a href="/admin/show_chapters">Show All Course Chapters</a></li>
							<li><a href="/admin/add_chapters">Add New Course Chapters</a></li>
							<li><a href="/admin/analyze_chapters">Analyze Course Chapters</a></li>
						</ul>
					</fieldset>
				</li> -->
				<li>
					<fieldset>
						<legend>Chapter Lessons</legend>
						<ul class='admin_links'>
							<!-- <li><a href="/admin/show_lessons">Show All Chapter Lessons</a></li> -->
							<!-- <li><a href="/admin/add_lesson_image">Add New Lesson Image</a></li> -->
							<li><a href="/admin/add_lessons">Add New Chapter Lessons</a></li>
							<li><a href="/admin/analyze_lessons">Analyze Chapter Lessons</a></li>
						</ul>
					</fieldset>
				</li>
			</ul>
		</div>
	</div>
</body>
</html>

