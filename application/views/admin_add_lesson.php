<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Admin - Add Lesson</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
	<script type="text/javascript">
		$(document).ready(function(){
			
			var counter = 0;

			$('#add_h1').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
				.append($("<label class='course_setup_label'>Heading 1</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:counter+'[]' }))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'h1'}))
     			.appendTo("#page_content_form");
  				counter++;
			});
			$('#add_h2').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Heading 2</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:counter+'[]' }))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'h2'}))
     			.appendTo("#page_content_form");
  				counter++;
			});
			$('#add_p').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Paragraph</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:counter+'[]' }))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'p'}))
     			.appendTo("#page_content_form");
  				counter++;
			});
			$('#add_link').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Link</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:counter+'[]' }))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'l'}))
     			.appendTo("#page_content_form");
  				counter++;
			});
			$('#add_video').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Video</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<textarea>", {'class':'course_setup_input',  type: "text", name:counter+'[]' }))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'v'}))
     			.appendTo("#page_content_form");
  				counter++;
			});
			$('#add_image').click(function(){
				<?php $IMGcounter = 'hi' ?>
 				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Image</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'i'}))
     			.appendTo("#page_content_form");
  				counter++;			
			});
			$('#add_list').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>List</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:counter+'[]', placeholder:'Seperate List Items With "/"' }))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'p'}))
     			.appendTo("#page_content_form");
  				counter++;
			});
			$('#add_2image').click(function(){
				<?php $IMGcounter = 'hi' ?>
 				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Image</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<select name='"+counter+'[1][]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<select name='"+counter+'[1][]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'b2i'}))
     			.appendTo("#page_content_form");
  				counter++;			
			});
			$('#add_3image').click(function(){
				<?php $IMGcounter = 'hi' ?>
 				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Image</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'b3i'}))
     			.appendTo("#page_content_form");
  				counter++;			
			});
			$('#add_4image').click(function(){
				<?php $IMGcounter = 'hi' ?>
 				$("<div />", { "class":"form-group page_sections col-xs-8 col-xs-offset-2", id:"content"+counter })
     			.append($("<label class='course_setup_label'>Image</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', name:counter+'[]', value:counter}))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<select name='"+counter+'[]'+"'><?php foreach($lesson_images AS $image){ ?><option value=<?=$image['photo_name']?>><?=$image['photo_name']?></option><?php } ?>"))
     			.append($("</select>"))
     			.append($("<input>", {type:'hidden', name:counter+'[]', value:'b4i'}))
     			.appendTo("#page_content_form");
  				counter++;			
			});
			var Qcounter = 0;
			$('#add_question_4').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-12", id:"quiz"+Qcounter })
     			.append($("<label class='course_setup_label'>Question "+(Qcounter+1)+"</label>"))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:'question[question][]', placeholder:'Enter Question Here' }))
     			.append($("<label class='course_setup_label'>Answer #1</label>"))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:'question[answer1][]', placeholder:'Enter Answer #1 Here' }))
     			.append($("<label class='course_setup_label'>Answer #2</label>"))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:'question[answer2][]', placeholder:'Enter Answer #2 Here' }))
     			.append($("<label class='course_setup_label'>Answer #3</label>"))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:'question[answer3][]', placeholder:'Enter Answer #3 Here' }))
     			.append($("<label class='course_setup_label'>Answer #4</label>"))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:'question[answer4][]', placeholder:'Enter Answer #4 Here' }))
     			.append($("<label class='course_setup_label'>Correct Answer #</label>"))
     			.append($("<input>", {'class':'course_positions', type: 'number', step:'1', min:'1', max:'4', name:'question[correct][]'}))
     			.appendTo("#quiz_questions");
  				Qcounter++;
			});
			$('#add_question_2').click(function(){
				$("<div />", { "class":"form-group page_sections col-xs-12", id:"quiz"+Qcounter })
     			.append($("<label class='course_setup_label'>Question "+(Qcounter+1)+"</label>"))
     			.append($("<textarea>", {'class':'course_setup_input', type: "text", name:'question[question][]', placeholder:'Enter Question Here' }))
     			.append($("<label class='course_setup_label'>Correct Answer True/False</label>"))
     			.append($("<select name='question[correct][]'><?php foreach($TorF AS $answer){ ?><option value=<?=$answer?>><?=$answer?></option><?php } ?>"))
     			.append($("</select>"))
     			

     			// .append($("<input>", {'class':'course_setup_input', type: 'text', name:'question[correct][]'}))
     			.append($("<input>", {type: 'hidden', name:'question[answer1][]', value:'True'}))
     			.append($("<input>", {type: 'hidden', name:'question[answer2][]', value:'False'}))
     			.append($("<input>", {type: 'hidden', name:'question[answer3][]', value:'NA'}))
     			.append($("<input>", {type: 'hidden', name:'question[answer4][]', value:'NA'}))
     			.appendTo("#quiz_questions");
  				Qcounter++;
			});

			// Tabs

			$(function() {
    			$( "#tabs" ).tabs();
  			});

			var activeLi = $(".tabs > ul > li.active");
			var activeIndex = $(".tabs > ul > li").index(activeLi);
			$(".tab-content:first").show();
			var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
			$("#tab-container .arrow").css("left", arrowPos);

			$(window).resize(function() {
			  var activeLi = $(".tabs > ul > li.active");
			  var activeIndex = $(".tabs > ul > li").index(activeLi);
			  var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
			  $("#tab-container .arrow").css("left", arrowPos);
			});

			$(".tabs > ul > li ").click(function() {
			  var listItem = $(this).closest("li");
			  if ($(listItem).hasClass("active")) {
			    return;
			  }
			  $(".tab-content").hide();
			  var activeTab = $(listItem).attr("rel");
			  var tabList = $(".tabs > ul > li");
			  var arrow = $("#tab-container .arrow");
			  var index = tabList.index($(listItem));
			  var width = $(listItem).width();
			  var leftValue = width * (index + 0.5) - 12;
			  arrow.css("left", leftValue);
			  $("#" + activeTab).fadeIn();
			  
			  tabList.removeClass("active");
			  $(listItem).addClass("active");
			});


		});
	</script>

<style type="text/css">
	#admin_lesson_options{
		/*border: 1px solid black;*/
	}
	

	/* Tabs */
	.tabs .arrow {
	  position: absolute;
	  width: 0;
	  height: 0;
	  border-left: 12px solid transparent;
	  border-right: 12px solid transparent;
	  border-bottom: 10px solid rgb(95,150,215);
	  top: -3px;
	  -webkit-transform: rotate(180deg);     /* Chrome and other webkit browsers */
	  -moz-transform: rotate(180deg);        /* FF */
	  -o-transform: rotate(180deg);          /* Opera */
	  -ms-transform: rotate(180deg);         /* IE9 */
	  transform: rotate(180deg);
	  transition: all 0.3s ease;
	}
	.tabs ul {
	  width: 100%;
	  box-sizing: border-box;
	  margin: 0;
	  padding: 0;
	  list-style: none;
	}
	.tabs ul li {
	  font-weight: bold;
	  color: rgb(212,212,212);
	  font-size: 24px;
	  background-color: rgb(247,247,247);
	  text-align: center;
	  float: left;
	  padding: 16px 0;
	  box-sizing: border-box;
	  display: inline-block;
	  width: 20%;
	}
	.tabs ul li.active {
	  border-bottom: 3px solid rgb(95,150,215);
	  color: black;
	}
	.tabs ul {
	  color: #000;
	  text-decoration: none;
	}
	.tabs #tab-container {
	  /*padding-top: 25px;*/
	  position: relative;
	  /*min-height: 400px;*/
	  background-color: #eeeeee;
	  clear: both;
	}
	.tabs .tab-content {
	  padding: 25px;
	  display: none;
	}
	.action_text{
		text-indent: 50px;
	}
	.photo_block{
		width: 100%; 
		border: 1px solid black; 
		overflow: hidden;
	}
		.photo_card{
			height: 400px;
			border-bottom: 3px solid black;
			margin-bottom: 10px;
		}
		.photo_details{
			text-align: center;
			width: 40%;
			margin-left: 30%;
			margin-right: 30%;
		}
		.upload_title{
			text-align: center;
		}

</style>


</head>
<body id='admin_lesson_body'>
	<div class='row'>
		<a href="/admin/dashboard"><button class='pull-right'>Admin Dashboard</button></a>
	</div>

	<div class='row' id='admin_lesson_options'>
		<div  style='background-color: white; width: (100%); '>
				<div class="tabs" style='background-color: white;'>
				  <ul>
				    <li class="active" style='background-color: white;' rel="tab1">Table Of Contents</li>
				    <li style='background-color: white;' rel="tab2">Image Control</li>
				    <li style='background-color: white;' rel="tab3">Lesson Builder</li>
				    <li style='background-color: white;' rel="tab4">Assignment Builder</li>
				    <li style='background-color: white;' rel="tab5">Quiz Builder</li>
				  </ul>
				  
				  <div id="tab-container">
				    <div class="arrow"></div>
				    
				    <!-- Display Current Course Content Directory List -->
				    <div id="tab1" class="tab-content">
						<table class='table table-responsive table-striped'>
							<thead>
								<tr>
									<th>Subject #</th>
									<th>Subject Title</th>
									<th>Chapter #</th>
									<th>Chapter Title</th>
									<th>Lesson #</th>
									<th>Lesson Title</th>
									<th class='action_text'>Actions</th>
								</tr>
							</thead>
							<tbody>
								<?php foreach ($current_chapters_lessons as $section) { ?>
									<tr>
										<td><?=$section['subject']?></td>
										<td><?=$section['subject_title']?></td>
										<td><?=$section['chapter']?></td>
										<td><?=$section['chapter_title']?></td>
										<td><?=$section['lesson']?></td>
										<td><?=$section['lesson_title']?></td>
										<td>
											<form action='/admin/show_lessons' method='POST' class='action_forms'>
											<input type='hidden' name='subject' value='<?=$section['subject']?>'>
											<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
											<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
											<button type='submit'><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button>
											</form>
											&nbsp&nbsp&nbsp
											<form action='#' method='POST' class='action_forms'>
											<input type='hidden' name='subject' value='<?=$section['subject']?>'>
											<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
											<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
											<button type='submit'><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
											</form>
											&nbsp&nbsp&nbsp
											<form action='#' method='POST' class='action_forms'>
											<input type='hidden' name='subject' value='<?=$section['subject']?>'>
											<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
											<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
											<button type='submit'><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></button>
											</form>
											&nbsp&nbsp&nbsp
											<form action='#' method='POST' class='action_forms'>
											<input type='hidden' name='subject' value='<?=$section['subject']?>'>
											<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
											<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
											<button type='submit'><img src='/assets/images/quiz.png' style='width: 20px; height: 20px;' aria-hidden="true"></button>
											</form>
										</td>
									</tr>	
								<?php } ?>
							</tbody>
						</table>
				    </div>

				    <!-- Upload Form For Course Content Photos -->
				    <div id="tab2" class="tab-content">
			    		<div class='row'>
							<div class='col-sm-4 col-sm-offset-4'>
								<h3 class='upload_title'>Upload Image</h3>
								<h5 class='upload_title'>Max File Size: 1024x1024 (10Mb)</h5>
								<?php echo form_open_multipart('upload/do_upload_lesson_image');?>
									<input type='number' class='photo_details' name='chapter' placeholder='Chapter #' required><br>
									<input type='number' class='photo_details' name='lesson' placeholder='Lesson #' required><br>
									<input type='text' class='photo_details' name='image_title' placeholder='Image Title' required><br>
								  	<div class='row'>
										<input type="file" name="userfile" size="20" required='true' title="Please upload a profile picture." style='width: 200px; margin: 0 auto; margin-bottom: 15px;'/>
										<button type="submit" class="btn btn-default photo_details">Upload</button>
								  	</div>
									<br /><br />
								</form>
							</div>
						</div>
							<br>
							<hr>
							<div class='row'>
								<?php foreach($lesson_images AS $image){ ?>
									<div class='col-xs-3 photo_card'>
										<img src="/lesson_images/<?=$image['photo_name']?>" class='photo_block'>
										<p class='photo_details'><?=$image['photo_name']?></p>
										<p class='photo_details'>
											<?php 
												list($width, $height, $attr) = getimagesize('./lesson_images/'.$image['photo_name']);
												
												echo "Image width " .$width;
												echo "<BR>";
												echo "Image height " .$height;
											?>
										</p>

									</div>
								<?php } ?>
							</div>
					</div>

				    </div>
				    <div id="tab3" class="tab-content">
				      	
				      	<!-- Dynamic Buttons For Form Inputs -->
				      	<div class='col-xs-2'>
							<!-- <div class='admin_container'> -->
								<h3 class='centerItem'>Add To Lesson</h3>
								<button class='content_insert' id='add_h1'>H1</button>
								<button class='content_insert' id='add_h2'>H2</button>
								<button class='content_insert' id='add_p'>P</button>
								<button class='content_insert' id='add_link'>Link</button>
								<button class='content_insert' id='add_video'>Video</button>
								<button class='content_insert' id='add_list'>List</button>
								<button class='content_insert' id='add_image'>Image</button>
								<button class='content_insert' id='add_2image'>Img x2</button>
								<button class='content_insert' id='add_3image'>Img x3</button>
								<button class='content_insert' id='add_4image'>Img x4</button>
								<hr>
							<!-- </div> -->
						</div>
				     	<!-- New Lesson Form -->
						<div class='col-xs-10'>
							<form action='/admin/register_new_lesson' method='POST'>
								<div class='col-xs-8 col-xs-offset-2'>
									<label for='#subject' class='chapter_lesson_label'>Subject</label>
									<input type='number' name='subject' class='chapter_lesson_input' id='subject'>
									<input type='text' name='subject_title' class='title_input' id='subject_title' placeholder='Subject Title'>
									<br>
									<label for='#chapter' class='chapter_lesson_label'>Chapter</label>
									<input type='number' name='chapter' class='chapter_lesson_input' id='chapter'>
									<input type='text' name='chapter_title' class='title_input' id='chapter_title' placeholder='Chapter Title'>
									<br> 
									<label for='#lesson' class='chapter_lesson_label'>Lesson</label>
									<input type='number' name='lesson' class='chapter_lesson_input' id='lesson'>
									<input type='text' name='lesson_title' class='title_input' id='lesson_title' placeholder='Lesson Title'>
									<br>
								</div>
								<div id='page_content_form'>
									<!-- Page Content Loaded Here Thorugh JQuery (H1, H2, P, Link, Video, Image) -->
								</div>
								<br><br>
								<div class='col-xs-4 col-xs-offset-4'>
									<input type='submit' value='Add Lesson' class='lesson_submit'>
								</div>
							</form>
						</div>
				    </div>

				    <!-- New Project Form -->
				    <div id="tab4" class="tab-content">
				    	<div class='row'>
							<div class='col-xs-4 col-xs-offet-2'>
								<p>Enter the title as you wish to show at the beginning of the assignment tab.</p>
								<p>	This is the title in which students and mentors will be referencing off of.</p>
								<p>Each assignment title should be uniquie to the respective lesson's content and learning objectives.</p>
								<hr>
								<p>Enter a descriptive context for the assignment. The structure is as follows:</p>
								<ol>
									<li>Background</li>
									<li>Purpose</li>
									<li>Requirements</li>
									<li>Grading</li>
								</ol>

							</div>

							<form action='/admin/register_new_project' method='POST'>
								<div class='col-xs-4 col-xs-offset-1'>
									<h3 class='cenerText'>Add New Lesson Assignment:</h3>
									<label for='#project_title' class='course_setup_label'>Project Title</label>
									<input type='text' class='course_setup_input' name='project_title' placeholder='Project Title'>
									<label for='#project_description' class='course_setup_label'>Project Description</label>
									<textarea name='project_description' class='course_setup_input' placeholder='Project Description'></textarea>
									<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
									<input type='submit' value='Add Project' class='lesson_submit'>
								</div>
								</div>
							</form>
						</div>
				    </div>

				    <!-- New Quiz Form -->
				    <div id="tab5" class='tab-content'>
						<div class='row'>
							<form action='/admin/register_new_quiz' method='POST'>
								<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
									<h3 class='centerText'>Quiz Set Up</h3>
								<div class='row'>	
									<label for='#subject' class='chapter_lesson_label'>Subject</label>
									<input type='number' name='subject' class='chapter_lesson_input' id='subject'>
									<br>
									<label for='#chapter' class='chapter_lesson_label'>Chapter</label>
									<input type='number' name='chapter' class='chapter_lesson_input' id='chapter'>
									<br> 
									<label for='#lesson' class='chapter_lesson_label'>Lesson</label>
									<input type='number' name='lesson' class='chapter_lesson_input' id='lesson'>
								</div>
									<div id='add_question_4'>Multiple Choice</div>
									<div id='add_question_2'>True/False</div>
									<div id='quiz_questions'></div>
									<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
									<input type='submit' value='Add Quiz' class='lesson_submit'>
									</div>
								</div>
							</form>
						</div>
				    </div>
				  </div> <!-- End: tab-container -->

				</div>
			</div>
	</div>

	<!-- Display Current Course Content Directory List -->
	<!-- <div class='row'>
		<table class='table table-responsive table-striped'>
			<thead>
				<tr>
					<th>Subject #</th>
					<th>Subject Title</th>
					<th>Chapter #</th>
					<th>Chapter Title</th>
					<th>Lesson #</th>
					<th>Lesson Title</th>
					<th>Actions</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ($current_chapters_lessons as $section) { ?>
					<tr>
						<td><?=$section['subject']?></td>
						<td><?=$section['subject_title']?></td>
						<td><?=$section['chapter']?></td>
						<td><?=$section['chapter_title']?></td>
						<td><?=$section['lesson']?></td>
						<td><?=$section['lesson_title']?></td>
						<td>
							<form action='/admin/show_lessons' method='POST' class='action_forms'>
							<input type='hidden' name='subject' value='<?=$section['subject']?>'>
							<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
							<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
							<button type='submit'><span class="glyphicon glyphicon-eye-open" aria-hidden="true"></span></button>
							</form>
							&nbsp&nbsp&nbsp
							<form action='#' method='POST' class='action_forms'>
							<input type='hidden' name='subject' value='<?=$section['subject']?>'>
							<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
							<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
							<button type='submit'><span class="glyphicon glyphicon-trash" aria-hidden="true"></span></button>
							</form>
							&nbsp&nbsp&nbsp
							<form action='#' method='POST' class='action_forms'>
							<input type='hidden' name='subject' value='<?=$section['subject']?>'>
							<input type='hidden' name='chapter' value='<?=$section['chapter']?>'>
							<input type='hidden' name='lesson' value='<?=$section['lesson']?>'>
							<button type='submit'><span class="glyphicon glyphicon-wrench" aria-hidden="true"></span></button>
							</form>
						</td>
					</tr>	
				<?php } ?>
			</tbody>
		</table>
	</div> -->
<!-- <hr> -->
	<!-- Upload Form For Course Content Photos -->
	<!-- <div class='row'>
			<div class='col-sm-4 col-sm-offset-4'>
				<h3>Add New Image For Lessons</h3>
				<?php echo form_open_multipart('upload/do_upload_lesson_image');?>
					<input type='number' name='chapter' placeholder='Chapter #' required>
					<input type='number' name='lesson' placeholder='Lesson #' required>
					<input type='text' name='image_title' placeholder='Image Title'>
				  	<div class='row'>
						<input type="file" name="userfile" size="20" required='true' title="Please upload a profile picture." style='width: 200px; margin: 0 auto; margin-bottom: 15px;'/>
						<button type="submit" style='width: 100%' class="btn btn-default">Save & Continue</button>
				  	</div>
					<br /><br />
				</form>
			</div>
		</div>
	</div> -->
<!-- <hr> -->
	<!-- Dynamic Buttons For Form Inputs -->
	<!-- <div class='row'>
		<div class='admin_container'>
			<h3 class='centerItem'>Add Sections For Lesson</h3>
			<button class='content_insert' id='add_h1'>H1</button>
			<button class='content_insert' id='add_h2'>H2</button>
			<button class='content_insert' id='add_p'>P</button>
			<button class='content_insert' id='add_link'>Link</button>
			<button class='content_insert' id='add_video'>Video</button>
			<button class='content_insert' id='add_list'>List</button>
			<button class='content_insert' id='add_image'>Image</button>
			<button class='content_insert' id='add_2image'>Img x2</button>
			<button class='content_insert' id='add_3image'>Img x3</button>
			<button class='content_insert' id='add_4image'>Img x4</button>
			<hr>
		</div>
	</div> -->
	
	<!-- New Lesson Form -->
	<!-- <div class='row'>
		<form action='/admin/register_new_lesson' method='POST'>
			<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
				<label for='#subject' class='chapter_lesson_label'>Subject</label>
				<input type='number' name='subject' class='chapter_lesson_input' id='subject'>
				<input type='text' name='subject_title' class='title_input' id='subject_title' placeholder='Subject Title'>
				<br>
				<label for='#chapter' class='chapter_lesson_label'>Chapter</label>
				<input type='number' name='chapter' class='chapter_lesson_input' id='chapter'>
				<input type='text' name='chapter_title' class='title_input' id='chapter_title' placeholder='Chapter Title'>
				<br> 
				<label for='#lesson' class='chapter_lesson_label'>Lesson</label>
				<input type='number' name='lesson' class='chapter_lesson_input' id='lesson'>
				<input type='text' name='lesson_title' class='title_input' id='lesson_title' placeholder='Lesson Title'>
				<br>
			</div>
			<div id='page_content_form'>
				
			</div>
			<br><br>
			<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
				<input type='submit' value='Add Lesson' class='lesson_submit'>
			</div>
		</form>
	</div> -->
	<!-- <hr> -->
	<!-- <div class='row'>
		<form action='/admin/register_new_project' method='POST'>
			<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
				<h3 class='cenerText'>Add New Lesson Project:</h3>
				<label for='#project_title' class='course_setup_label'>Project Title</label>
				<input type='text' class='course_setup_input' name='project_title' placeholder='Project Title'>
				<label for='#project_description' class='course_setup_label'>Project Description</label>
				<textarea name='project_description' class='course_setup_input' placeholder='Project Description'></textarea>
				<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
				<input type='submit' value='Add Project' class='lesson_submit'>
			</div>
			</div>
		</form>
	</div> -->
	
	<hr>
	<!-- New Quiz Form -->
	<!-- <div class='row'>
		<form action='/admin/register_new_quiz' method='POST'>
			<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
				<h3 class='centerText'>Quiz Set Up</h3>
			<div class='row'>	
				<label for='#subject' class='chapter_lesson_label'>Subject</label>
				<input type='number' name='subject' class='chapter_lesson_input' id='subject'>
				<br>
				<label for='#chapter' class='chapter_lesson_label'>Chapter</label>
				<input type='number' name='chapter' class='chapter_lesson_input' id='chapter'>
				<br> 
				<label for='#lesson' class='chapter_lesson_label'>Lesson</label>
				<input type='number' name='lesson' class='chapter_lesson_input' id='lesson'>
			</div>
				<div id='add_question_4'>Multiple Choice</div>
				<div id='add_question_2'>True/False</div>
				<div id='quiz_questions'></div>
				<div class='col-xs-12 col-sm-8 col-sm-offset-2 col-md-4 col-md-offset-4'>
				<input type='submit' value='Add Quiz' class='lesson_submit'>
				</div>
			</div>
		</form>
	</div> -->
</body>
</html>

<!-- br><br>
<div class='row'>
	<br>
	<hr>
<?php var_dump($current_chapters_lessons) ?>
<hr>
<?php var_dump($subject_list) ?>
<hr>
<?php var_dump($chapter_list) ?>
<hr>
<?php var_dump($lesson_list) ?>
</div>

<br>
 -->
 <!-- // <?php var_dump($TorF) ?> -->


