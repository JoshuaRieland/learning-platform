<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Account Security</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/style.scss">
</head>
<body>
	<div class='container'>
		<form class="form-horizontal" action='/user/update_password' method='POST'>
		  	<div class="form-group">
		    	<label for="password" class="col-sm-2 control-label">New Password</label>
		    	<div class="col-sm-10">
		      		<input type="password" class="form-control" name='password' id="password" placeholder="New Email">
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<label for="confirm_password" class="col-sm-2 control-label">Confirm Password</label>
		    	<div class="col-sm-10">
		      		<input type="password" class="form-control" name='confirm_password' id="confirm_password" placeholder="Re-enter Password">
		    	</div>
		  	</div>
		  	<div class="form-group">
		    	<div class="col-sm-offset-2 col-sm-10">
		      		<button type="submit" class="btn btn-default">Sign in</button>
		    	</div>
		  	</div>
		</form>
	</div>
</body>
</html>