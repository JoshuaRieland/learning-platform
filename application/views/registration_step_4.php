<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Registration Step 4</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/step_bar.css">
</head>
<body>
	<div class='registration_container'>	
		
	<div class='row'>
			<div class='progress_bar'>
				<ol class="stepBar step5">
					
					<li class="step">My Info</li>
					<li class='step'>Mentor</li>
					<li class="step">Meeting</li>
					<li class="step completed current">Materials</li>
					<li class="step">Pre-course</li>
				</ol>
			</div>
				<a href="/user/signout" class='pull-right'>LogOut <i class='glyphicon glyphicon-log-out'></i></a>
			<div class='progress_bar_small' hidden='true'>
				dhdh
			</div>
		</div>
		<div class='prep_content'>
			<div class='row'>
				<h4 class='col-md-5 col-sm-6 col-xs-8'>
					What you need before beginning this course:
				</h4>
			</div>
			<div class='row'>
				<h4>To Do:</h4>
				<ul>
					<li><a href="http://www.skype.com/en/">Download & Install Skype</a></li>
					<li><a href="http://www.photoshop.com/">Download & Install PhotoShop</a> / <a href="http://www.basalmiq.com/">Basalmiq</a></li>
					<li>Create A Github / Bitbucket Account</li>
					<li>Establish Media Accounts</li>
					<ul>
						<li><a href="http://www.twitter.com">Twitter</a></li>
						<li><a href="http://www.linkedin.com">LinkedIn</a></li>
						<li><a href="http://www.pinterest.com">Pinterest</a></li>
					</ul>
					<li>Complete Pre-Course Assignments</li>
				</ul>
			</div>
			<div class='row'>
				<h4>To Know:</h4>
				<ul>
					<li>Basic HTML/CSS Knowledge & Understanding</li>
					<li>Each week you will meet with your mentor 2-3 times.</li>
					<li>25-40 hours/week needs to be dedicated to course material.</li>
					<li>An alternate place to go in of a power outage.</li>
					<li>Lastly, you one step away from becoming a UX/UI designer!</li>
				</ul>
			</div>
		<div class='col-md-5 col-sm-6 col-xs-8 step_btn'>
			<a href="/goto/step5"><button>Continue To Pre-Course</button></a>
		</div>
		</div>
	</div>
</body>
</html>