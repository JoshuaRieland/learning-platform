<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Registration Step 1</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/step_bar.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script src="/assets/js/JQueryLib.js"></script>

</head>
<body>
	<div class='registration_container'>

		<div class='row'>
			<div class='progress_bar'>
				<ol class="stepBar step5">		
					<li class="step completed current">My Info</li>
					<li class='step'>Mentor</li>
					<li class="step">Meeting</li>
					<li class="step">Materials</li>
					<li class="step">Pre-course</li>
				</ol>
			</div>
				<a href="/user/signout" class='pull-right'>LogOut <i class='glyphicon glyphicon-log-out'></i></a>
			<div class='progress_bar_small' hidden='true'>
				dhdh
			</div>

		</div>
		<div class='row'>
			<div class='col-sm-4 col-sm-offset-4'>
				

				
				<?php echo form_open_multipart('upload/do_upload');?>



				<input type='hidden' name='userID' value='<?=$_SESSION['userID']?>'>
				 <!--  <div class="form-group">
				    <label for="profile_picture">Profile Picture:</label>
				    <input type="file" name='profile_picture' id="profile_picture">
				    <p class="help-block">Profile picture is optional if you do not wish to upload a photo.</p>
				  </div> -->
				  	
				  	
				  	<div class="form-group">
				   		<label for="first_name">First Name:</label>
				    	<input type="text" class="form-control" name='first_name' id="first_name" placeholder="First Name" required='true' title='Please enter your first name' <?php if($student_info != null){?> value='<?=$student_info['first_name']?>'<?php } ?> >
				  	</div>
				  	

				  	<div class="form-group">
				    	<label for="last_name">Last Name:</label>
				    	<input type="text" class="form-control" name='last_name' id="last_name" placeholder="Last Name" required='true' title='Please enter your last name.' <?php if($student_info != null){?> value='<?=$student_info['last_name']?>'<?php } ?> >
				  	</div>
				  	<div class="form-group">
				    	<label for="gender">Gender:</label>
				    	<select class='form-control' name='gender' id='gender' required='true' title='Please select the gender you identify with.'>
							<option selected disabled hidden value=''>Select an option</option>
							<option value="M">Male</option>
							<option value="F">Female</option>
							<option value="AL">Prefer not to say</option>
						</select>				
				  	</div>
				  	<div class="form-group">
				   		<label for="age">Age:</label>
				   		<input type="number" class="form-control" name='age' id="age" placeholder="Age" min='13' required='true' title="Please enter your age." <?php if($student_info != null){?> value='<?=$student_info['age']?>'<?php } ?> >
				  	</div>
				  	<div class="form-group">
				    	<label for="city">City:</label>
				    	<input type="text" class="form-control" name='city' id="city" placeholder="City" required='true' title='Please select the city in which you reside.' <?php if($student_info != null){?> value='<?=$student_info['city']?>'<?php } ?> > 
				  	</div>
				  	<div class="form-group">
				    	<label for="city">State:</label>
				    	<select class='form-control' name='state' id='state' required='true' title='Please select the state in which you reside.' <?php if($student_info != null){?> value='<?=$student_info['state']?>'<?php } ?> >
							<option selected disabled hidden value=''>Select a state</option>
							<option value="AL">Alabama</option>
							<option value="AK">Alaska</option>
							<option value="AZ">Arizona</option>
							<option value="AR">Arkansas</option>
							<option value="CA">California</option>
							<option value="CO">Colorado</option>
							<option value="CT">Connecticut</option>
							<option value="DE">Delaware</option>
							<option value="DC">District Of Columbia</option>
							<option value="FL">Florida</option>
							<option value="GA">Georgia</option>
							<option value="HI">Hawaii</option>
							<option value="ID">Idaho</option>
							<option value="IL">Illinois</option>
							<option value="IN">Indiana</option>
							<option value="IA">Iowa</option>
							<option value="KS">Kansas</option>
							<option value="KY">Kentucky</option>
							<option value="LA">Louisiana</option>
							<option value="ME">Maine</option>
							<option value="MD">Maryland</option>
							<option value="MA">Massachusetts</option>
							<option value="MI">Michigan</option>
							<option value="MN">Minnesota</option>
							<option value="MS">Mississippi</option>
							<option value="MO">Missouri</option>
							<option value="MT">Montana</option>
							<option value="NE">Nebraska</option>
							<option value="NV">Nevada</option>
							<option value="NH">New Hampshire</option>
							<option value="NJ">New Jersey</option>
							<option value="NM">New Mexico</option>
							<option value="NY">New York</option>
							<option value="NC">North Carolina</option>
							<option value="ND">North Dakota</option>
							<option value="OH">Ohio</option>
							<option value="OK">Oklahoma</option>
							<option value="OR">Oregon</option>
							<option value="PA">Pennsylvania</option>
							<option value="RI">Rhode Island</option>
							<option value="SC">South Carolina</option>
							<option value="SD">South Dakota</option>
							<option value="TN">Tennessee</option>
							<option value="TX">Texas</option>
							<option value="UT">Utah</option>
							<option value="VT">Vermont</option>
							<option value="VA">Virginia</option>
							<option value="WA">Washington</option>
							<option value="WV">West Virginia</option>
							<option value="WI">Wisconsin</option>
							<option value="WY">Wyoming</option>
						</select>				
				  	</div>
				  	<div class="form-group">
				   		<label for="zip_code">Zip Code:</label>
				   		<input type="number" class="form-control" name='zip_code' id="zip_code" placeholder="Zip/Postal Code" required='true' title="Please enter your zip/postal code." <?php if($student_info != null){?> value='<?=$student_info['zip_code']?>'<?php } ?> >
				  	</div>
				  	<div class='row'>
						<input type="file" name="userfile" size="20" required='true' title="Please upload a profile picture." style='width: 200px; margin: 0 auto; margin-bottom: 15px;'/>
						<?php if($student_info['profile_pic'] != 'none'){ ?>
							<div style='width: 200px; height: 200px; background-image: url("/uploads/<?=$student_info['profile_pic']?>"); background-size: cover; '>
							
							</div>
						<?php } ?>
					<button type="submit" style='width: 100%' class="btn btn-default">Save & Continue</button>
				  	</div>
					<br /><br />
				</form>
			</div>
		</div>
	</div>

</body>
</html>

<?php var_dump($student_info) ?>
<br><br><br>

<?php var_dump($_SESSION) ?>


