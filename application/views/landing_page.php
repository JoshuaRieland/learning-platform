<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Welcome To UX Academy</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/animate.css">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>
	<script type="text/javascript">
		
	</script>
	<style type="text/css">
		h2:hover{
			animation: rubberBand 10s linear infinite;
		}
	</style>
</head>
<body id='landing'>
	<div class='container'>

		<h2 class='animated bounceIn' id='signin_header'><i  id='1' class="fa fa-spin fa-life-ring"></i>  Student Sign In  <i class="fa fa-spin fa-life-ring"></i></h2>


		<form class="form-horizontal" action='/user/signin' method='POST'>
		  	<div class="form-group animated bounceIn">
		    	<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		      		<input type="email" class="form-control" name='email' id="email" placeholder="Email">
		    	</div>
		  	</div>
		  	<div class="form-group animated bounceIn">
		    	<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		      		<input type="password" class="form-control" name='password' id="password" placeholder="Password">
		    	</div>
		  	</div>
		  	<div class="form-group animated bounceIn">
		    	<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		      		<button type="submit" class="btn btn-default">Sign in</button>
		    	</div>
		  	</div>
		</form>
		<div class='margin50'></div>
		<h4 class='animated bounceIn' id='new_student'>Not a student yet?</h4>
		<h4 class='animated bounceIn' id='new_student'>Register Today!</h4>
		
		<div class='margin50'></div>
		
		<form class='form-horizontal' action='/student/new_register' method='POST'>
			<div class="form-group animated bounceIn">
		    	<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		      		<input type="email" class="form-control" name='email' id="email" placeholder="New Student Email">
		    	</div>
		  	</div>
		  	<div class='form-group animated bounceIn'>
		  		<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		  			<label for'input'>Start Date</label>
				  	<input type='date' name='start_date' class='col-md-6 form-control' required>
				</div>
			</div>
			<div class='form-group animated bounceIn'>
				<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		 			<label for='select'>Program Length</label>
		 			<select name='program_length' class='col-md-6 form-control'>
		 				<option value='6 Weeks'>6 Weeks</option>
		 				<option value='12 Weeks'>12 Weeks</option>
		 			</select>
		 		</div>
		 	</div>
		  	<div class="form-group animated bounceIn">
		    	<div class='col-xs-10 col-xs-offset-1 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3'>
		      		<button type="submit" class="btn btn-default">Register</button>
		    	</div>
		  	</div>
		</form>
	</div>
</body>
</html>