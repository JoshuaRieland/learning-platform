<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Registration Step 2</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/step_bar.css">

</head>
<body>
	<div class='registration_container'>	
		
	<div class='row'>
			<div class='progress_bar'>
				<ol class="stepBar step5">		
					<li class="step">My Info</li>
					<li class='step completed current'>Mentor</li>
					<li class="step">Meeting</li>
					<li class="step">Materials</li>
					<li class="step">Pre-course</li>
				</ol>
			</div>
				<a href="/user/signout" class='pull-right'>LogOut <i class='glyphicon glyphicon-log-out'></i></a>
			<div class='progress_bar_small' hidden='true'>
				dhdh
			</div>
		</div>
		

		<?php if($_SESSION['mentorID'] != -1 ){ ?>
		<div class='row' style='margin-top: 50px;'>
			<div class='col-xs-12 col-md-8 col-md-offset-2' style='border: 2px solid black; padding: 5px; text-align: center;'>
				<div class='row'>
					<div class='col-xs-3'>
						<img src="/uploads/<?=$mentor_info['profile_pic']?>" style='width: 100px; height: 100px; border-radius: 100%;'>
					</div>
					<div class='col-xs-6'>
						<h2>Your current mentor is:</h2>
						<h3><?= $mentor_info['first_name'] ?>  <?= $mentor_info['last_name'] ?></h3>
						<h3><?= $mentor_info['city'] ?>, <?= $mentor_info['state'] ?></h3>
					</div>
				</div>
				
				<br><br>
				<h3><?= $mentor_info['intro_message'] ?></h3>
				<br>
				<h3><?= $mentor_info['bio'] ?></h3>
			</div>
		</div>
		<?php }else{ ?>


		<div class='row'>
			<?php foreach($all_mentors AS $mentor){ ?>
				<form action='/student/register2' method='POST' id='mentor_list'>
					<div class='row col-md-offset-3 col-xs-offset-0'>
						<div class='col-md-8 col-md-offset-2 col-xs-12 col-xs-offset-0 mentor_info'>
							<div class='col-11 col-offset-1'>
								<p><img src="#"></p>
								<p><?= $mentor['first_name'] ?>&nbsp<?= $mentor['last_name']  ?></p>
								<p>#<?= $mentor['id']?></p>
							</div>
							<div class='col-11 col-offset-1'>
								<input type='hidden' name='mentorID' value='<?=$mentor['id']?>'>
								<?php if($_SESSION['mentorID'] != $mentor['id']){ ?>
								<input type='submit' value='Choose Mentor' class='mentor_select_btn'>
								<?php } else { ?>
								<input type='submit' value='CHOSEN' class='mentor_current_btn' disabled>
								<?php } ?>
								<br><br>
							</div>
							<div class='col-xs-12'>
								<p><?= $mentor['intro_message'] ?></p>
								<br><br>
								<p><?= $mentor['bio'] ?></p>
							</div>
						</div>
					</div>
				</form>
			<?php } ?>
		</div>
		<?php } ?>
	</div>
</body>
</html>


<?php var_dump($mentor_info) ?>
<br><br><br><br>
<?php var_dump($all_mentors) ?>
<?php var_dump($_SESSION) ?>