<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Registration Step 3</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/step_bar.css">
	<script src="/assets/js/JQueryLib.js"></script>
	
	<script type="text/javascript">
		$(document).ready(function(){


			if(<?= $_SESSION['program_length'] ?> == '12'){	
				var max = 3 - <?= INTVAL($meeting_count)-2 ?>;
			}else if(<?= $_SESSION['program_length'] ?> == '6'){
				var max = 3 - <?= INTVAL($meeting_count)-2 ?>;
			}

			var checkboxes = $('input[type="checkbox"]');
			checkboxes.change(function(){
				var count = checkboxes.filter(':checked').length;
				if(count >= max){
					checkboxes.filter(':not(:checked)').attr('disabled', true);
				}else{
					// checkboxes.filter(':not(:checked)').removeAttr('disabled');
				}
			});

			// Disable Rest Of Monday If 1 Checked
			var mondayBoxes = $('input[name="monday_time_slot"]');               
    		mondayBoxes.change(function(){
        		var currentMonday = mondayBoxes.filter(':checked').length;
        		if(currentMonday > 0){
        			mondayBoxes.filter(':not(:checked)').attr('disabled', true);
        		}else{
        			mondayBoxes.filter(':not(:checked)').removeAttr('disabled');
        		}
    		});
    		// Disable Rest Of Tuesday If 1 Checked
			var tuesdayBoxes = $('input[name="tuesday_time_slot"]');               
    		tuesdayBoxes.change(function(){
        		var currentTuesday = tuesdayBoxes.filter(':checked').length;
        		if(currentTuesday > 0){
        			tuesdayBoxes.filter(':not(:checked)').attr('disabled', true);
        		}else{
        			tuesdayBoxes.filter(':not(:checked)').removeAttr('disabled');
        		}
    		});
    		// Disable Rest Of Wednesday If 1 Checked
			var wednesdayBoxes = $('input[name="wednesday_time_slot"]');               
    		wednesdayBoxes.change(function(){
        		var currentWednesday = wednesdayBoxes.filter(':checked').length;
        		if(currentWednesday > 0){
        			wednesdayBoxes.filter(':not(:checked)').attr('disabled', true);
        		}else{
        			wednesdayBoxes.filter(':not(:checked)').removeAttr('disabled');
        		}
    		});
    		// Disable Rest Of Thursday If 1 Checked
			var thursdayBoxes = $('input[name="thursday_time_slot"]');               
    		thursdayBoxes.change(function(){
        		var currentThursday = thursdayBoxes.filter(':checked').length;
        		if(currentThursday > 0){
        			thursdayBoxes.filter(':not(:checked)').attr('disabled', true);
        		}else{
        			thursdayBoxes.filter(':not(:checked)').removeAttr('disabled');
        		}
    		});
    		// Disable Rest Of Friday If 1 Checked
			var fridayBoxes = $('input[name="friday_time_slot"]');               
    		fridayBoxes.change(function(){
        		var currentFriday = fridayBoxes.filter(':checked').length;
        		if(currentFriday > 0){
        			fridayBoxes.filter(':not(:checked)').attr('disabled', true);
        		}else{
        			fridayBoxes.filter(':not(:checked)').removeAttr('disabled');
        		}
    		});
    		// Disable Rest Of Saturday If 1 Checked
			var saturdayBoxes = $('input[name="saturday_time_slot"]');               
    		saturdayBoxes.change(function(){
        		var currentSaturday = saturdayBoxes.filter(':checked').length;
        		if(currentSaturday > 0){
        			saturdayBoxes.filter(':not(:checked)').attr('disabled', true);
        		}else{
        			saturdayBoxes.filter(':not(:checked)').removeAttr('disabled');
        		}
    		});
		});


	</script>


</head>
<BODY>
	<?php $current_month = Date('M'); ?>
	<?php $weekdays = ['monday', 'tuesday', 'wednesday', 'thursday', 'friday', 'saturday']?>
	<?php $times = ['1200AM', '1230AM', '100AM', '130AM','200AM', '230AM', '300AM', '330AM', '400AM', '430AM', '500AM', '530AM', '600AM', '630AM', '700AM', '730AM', '800AM', '830AM', '900AM', '930AM', '1000AM', '1030AM', '1100AM', '1130AM', '1200PM', '1230PM', '100PM', '130PM','200PM', '230PM', '300PM', '330PM', '400PM', '430PM', '500PM', '530PM', '600PM', '630PM', '700PM', '730PM', '800PM', '830PM', '900PM', '930PM', '1000PM', '1030PM', '1100PM', '1130PM'] ?>
	<?php $timesFull = ['12:00 AM', '12:30 AM', '1:00 AM', '1:30 AM','2:00 AM', '2:30 AM', '3:00 AM', '3:30 AM', '4:00 AM', '4:30 AM', '5:00 AM', '5:30 AM', '6:00 AM', '6:30 AM', '7:00 AM', '7:30 AM', '8:00 AM', '8:30 AM', '9:00 AM', '9:30 AM', '10:00 AM', '10:30 AM', '11:00 AM', '11:30 AM', '12:00 PM', '12:30 PM', '1:00 PM', '1:30 PM','2:00 PM', '2:30 PM', '3:00 PM', '3:30 PM', '4:00 PM', '4:30 PM', '5:00 PM', '5:30 PM', '6:00 PM', '6:30 PM', '7:00 PM', '7:30 PM', '8:00 PM', '8:30 PM', '9:00 PM', '9:30 PM', '10:00 PM', '10:30 PM', '11:00 PM', '11:30 PM'] ?>
	
	<div class='registration_container'>	
		
	
	<div class='row'>	
		<button class='pull-right'><a href="/student/dashboard">Student Dashboard</a></button>
		<a href="/user/signout" class='pull-right'>LogOut <i class='glyphicon glyphicon-log-out'></i></a>
	</div>
		
		<div class='row' style='margin-top: 50px;'>
			<div style='border: 2px solid black; padding: 5px; text-align: center; width: 400px;'>
				<h2>Set A Meeting With:</h2>
				<h3><?= $mentor_info['first_name'] ?>  <?= $mentor_info['last_name'] ?></h3>
			</div>
		</div>

		<div class='row'>

				<div class='col-xs-12 col-md-6 col-md-offset-3' style='border: 2px solid black; margin-top: 25px; padding: 5px; min-height: 100px;'>
					<?php if ($student_meeting_days['monday'] != 'none'){?>
							<p class='my_meetings'>Mondays: <?= $student_meeting_days['monday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting_db' method='POST'>
								<input type='hidden' name='day' value='monday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['monday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['tuesday'] != 'none'){?>
							<p class='my_meetings'>Tuesdays: <?= $student_meeting_days['tuesday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting_db' method='POST'>
								<input type='hidden' name='day' value='tuesday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['tuesday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['wednesday'] != 'none'){?>
							<p class='my_meetings'>Wednesdays: <?= $student_meeting_days['wednesday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting_db' method='POST'>
								<input type='hidden' name='day' value='wednesday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['wednesday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['thursday'] != 'none'){?>
							<p class='my_meetings'>Thursdays: <?= $student_meeting_days['thursday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting_db' method='POST'>
								<input type='hidden' name='day' value='thursday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['thursday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['friday'] != 'none'){?>
							<p class='my_meetings'>Fridays: <?= $student_meeting_days['friday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting_db' method='POST'>
								<input type='hidden' name='day' value='friday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['friday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['saturday'] != 'none'){?>
							<p class='my_meetings'>Saturdays: <?= $student_meeting_days['saturday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting_db' method='POST'>
								<input type='hidden' name='day' value='saturday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['saturday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
				</div>	
			</div>
		
		<?php $meeting_count -= 2; ?>
		
	  	<?php		if($meeting_count < 3){ ?>
		<form action='/student/change_schedule' method='POST' >
			<input type='submit' value='Set Schedule' style='width: 20%; margin-left: 40%; margin-top: 15px;'>
			<div class='row' style='margin-top: 10px; height: 450px; border: 2px solid black; padding: 5px; overflow-y: scroll;'>	
				<table class='table table-responsive' style='table-layout: fixed;'>
					<thead>
						<th class='schedule_table_cell'>Time</th>
						<?php foreach($days AS $day){ ?>
							<div id='<?=$day?>' class='calendar_header'>
								<th style='font-size: 12px; text-align: center; border: 2px solid black'><?=$day?></th>
							</div>
						<?php } ?>
					</thead>
					<tbody>
						<?php $current_count = 0; ?>
						<?php for($idx = 0; $idx < 48; $idx++){ ?>
						<?php if($mentor_schedule['monday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;} ?>
						<?php if($mentor_schedule['tuesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;} ?>
						<?php if($mentor_schedule['wednesday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;} ?>
						<?php if($mentor_schedule['thursday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;} ?>
						<?php if($mentor_schedule['friday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;} ?>
						<?php if($mentor_schedule['saturday'][$times[$idx]] == $_SESSION['userID']){ $current_count++;} ?>

							<tr>
								<th class='schedule_table_cell'><?= $timesFull[$idx] ?></th>
							
							<!-- Monday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if($student_meeting_days['monday'] == 'none'){ ?>
										<?php if(INTVAL($mentor_schedule['monday'][$times[$idx]]) == -1){ ?>
											<input type='checkbox' name='monday_time_slot' value='monday/<?= $times[$idx] ?>'>
										<?php }else if(INTVAL($mentor_schedule['monday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['monday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>				
									<?php }else{ ?>
										<?php if(INTVAL($mentor_schedule['monday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['monday'][$times[$idx]]) == -1){ ?>
											<!-- <input type='checkbox' name='monday_time_slot' value='monday/<?= $times[$idx] ?>' disabled> -->
											----
										<?php }else if(INTVAL($mentor_schedule['monday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>		
									<?php } ?>
								</th>
							
							<!-- Tuesday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if($student_meeting_days['tuesday'] == 'none'){ ?>
										<?php if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) == -1){ ?>
											<input type='checkbox' name='tuesday_time_slot' value='tuesday/<?= $times[$idx] ?>'>
										<?php }else if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php }else{ ?>
										<?php if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) == -1){ ?>
											<!-- <input type='checkbox' name='tuesday_time_slot' value='tuesday/<?= $times[$idx] ?>' disabled> -->
											----
										<?php }else if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['tuesday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php } ?>
								</th>

							<!-- Wednesday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if($student_meeting_days['wednesday'] == 'none'){ ?>
										<?php if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) == -1){ ?>
											<input type='checkbox' name='wednesday_time_slot' value='wednesday/<?= $times[$idx] ?>'>
										<?php }else if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php }else{ ?>
										<?php if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) == -1){ ?>
											<!-- <input type='checkbox' name='wednesday_time_slot' value='wednesday/<?= $times[$idx] ?>' disabled> -->
											----
										<?php }else if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['wednesday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php } ?>
								</th>

							<!-- Thursday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if($student_meeting_days['thursday'] == 'none'){ ?>
										<?php if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) == -1){ ?>
											<input type='checkbox' name='thursday_time_slot' value='thursday/<?= $times[$idx] ?>'>
										<?php }else if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php }else{ ?>
										<?php if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) == -1){ ?>
											<!-- <input type='checkbox' name='thursday_time_slot' value='thursday/<?= $times[$idx] ?>' disabled> -->
											----
										<?php }else if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['thursday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php } ?>
								</th>

							<!-- Friday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if($student_meeting_days['friday'] == 'none'){ ?>
										<?php if(INTVAL($mentor_schedule['friday'][$times[$idx]]) == -1){ ?>
											<input type='checkbox' name='friday_time_slot' value='friday/<?= $times[$idx] ?>'>
										<?php }else if(INTVAL($mentor_schedule['friday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['friday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php }else{ ?>
										<?php if(INTVAL($mentor_schedule['friday'][$times[$idx]]) == -1){ ?>
											<!-- <input type='checkbox' name='friday_time_slot' value='friday/<?= $times[$idx] ?>' disabled> -->
											----
										<?php }else if(INTVAL($mentor_schedule['friday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['friday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php } ?>
								</th>

							<!-- Saturday Time Slots -->
								<th class='schedule_table_cell'>
									<?php if($student_meeting_days['saturday'] == 'none'){ ?>
										<?php if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) == '-1'){ ?>
											<input type='checkbox' name='saturday_time_slot' value='saturday/<?= $times[$idx] ?>'>
										<?php }else if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php }else{ ?>
										<?php if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) == '-1'){ ?>
											<!-- <input type='checkbox' name='saturday_time_slot' value='saturday/<?= $times[$idx] ?>' disabled> -->
											----
										<?php }else if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) == $_SESSION['userID']){ ?>
											My Meeting
										<?php }else if(INTVAL($mentor_schedule['saturday'][$times[$idx]]) > 0){ ?>
											Booked
										<?php } ?>
									<?php } ?>
								</th>
							</tr>
						<?php } ?>
					</tbody>
				</table>
			</div>
		</form>
		<?php 	}else{ ?>
			<div class='row' style='text-align: center; margin-top: 25px; color: red;'>
				<h4>You have reached the maximum number of weekly meetings.</h4>
				<h4>Drop a current meeting or contact your mentor to unlock additional time slots.</h4>
			</div>
			<!-- <div class='row'>

				<div class='col-xs-12 col-md-6 col-md-offset-3' style='border: 2px solid black; margin-top: 25px; padding: 5px; min-height: 100px;'>
					<?php if ($student_meeting_days['monday'] != 'none'){?>
							<p class='my_meetings'>Mondays: <?= $student_meeting_days['monday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting' method='POST'>
								<input type='hidden' name='day' value='monday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['monday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['tuesday'] != 'none'){?>
							<p class='my_meetings'>Tuesdays: <?= $student_meeting_days['tuesday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting' method='POST'>
								<input type='hidden' name='day' value='tuesday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['tuesday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['wednesday'] != 'none'){?>
							<p class='my_meetings'>Wednesdays: <?= $student_meeting_days['wednesday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting' method='POST'>
								<input type='hidden' name='day' value='wednesday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['wednesday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['thursday'] != 'none'){?>
							<p class='my_meetings'>Thursdays: <?= $student_meeting_days['thursday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting' method='POST'>
								<input type='hidden' name='day' value='thursday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['thursday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['friday'] != 'none'){?>
							<p class='my_meetings'>Fridays: <?= $student_meeting_days['friday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting' method='POST'>
								<input type='hidden' name='day' value='friday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['friday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
					<?php if ($student_meeting_days['saturday'] != 'none'){?>
							<p class='my_meetings'>Saturdays: <?= $student_meeting_days['saturday'] ?></p>
							<form class='drop_meeting' action='/student/drop_meeting' method='POST'>
								<input type='hidden' name='day' value='saturday'>
								<input type='hidden' name='time_slot' value='<?= $student_meeting_days['saturday'] ?>'>
								<input type='submit' value='Drop Time Slot' class='drop_meeting_btn'>
							</form>
					<?php } ?>
				</div>	
			</div> -->
		<?php 	} ?>
		<!-- <div class='col-xs-7' style='border: 2px solid black; margin-top: 50px; height: 450px;'>

		</div> -->

	</div>
</body>
</html>
<?php
var_dump($student_meeting_days);
var_dump($_SESSION['program_length']);
var_dump($meeting_count);
var_dump($_SESSION['userID']);
?>


