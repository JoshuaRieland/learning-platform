<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Admin - Add Lesson</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>



</head>
<body style='padding: 50px;'>
	<div class='row'>
		<a href="/admin/dashboard"><button class='pull-right'>Admin Dashboard</button></a>
	</div>
<hr>
	<table class='table table-responsive table-striped'>
		<thead>
			<tr>
				<th></th>
				<th>Student ID</th>
				<th>Email</th>
				<th>Name</th>
				<th>Age</th>
				<th>City</th>
				<th>State</th>
				<th>Mentor</th>
				<th>Start Date</th>
				<th>Program<br>Length</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($all_students AS $student){ ?>
				<tr>
					<?php if($student['profile_pic'] != 'none'){ ?>
						<td><img src="/uploads/<?=$student['profile_pic']?>" style='width: 75px; height: 75px;'></td>
					<?php }else{ ?>
						<td><img src="/assets/images/silhouette.png" style='width: 75px; height: 75px;'></td>
					<?php } ?>
					<td><?=$student['id']?></td>
					<td><?=$student['email']?></td>
					<td><?=$student['first_name']?>&nbsp<?=$student['last_name']?></td>
					<td><?=$student['age']?></td>
					<td><?=$student['city']?></td>
					<td><?=$student['state']?></td>
					<td><?=$student['mentor_firstname']?>&nbsp<?=$student['mentor_lastname']?></td>
					<td><?=$student['start_date']?></td>
					<td><?=$student['program_length']?></td>
				</tr>
			<?php } ?>
		</tbody>
	</table>

</body>
</html>


