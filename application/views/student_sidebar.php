<html>
<head>
	<title></title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/student_dashboard.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			$('#nav_icon_div').toggleClass('moveDown');
			
			var left_div = $('#content_courses');
    		var right_div = $('#progression_track');
    		if(left_div.height() < right_div.height()){
	    		right_div.height(left_div.height()+100);
    		}else{
    			left_div.height(right_div.height());
    		}

    		// var nav_pane = $('.navigation_pane');
    		// var course_pane = $('#course_body');
    		// nav_pane.height(course_pane.height()+180);

			$('#nav_courses').click(function(){
 				$('#nav_pane_div').toggleClass('nav_pane_div_show');
 				$('#nav_pane_div').toggleClass('nav_pane_div');
 				$('#lesson_div').toggleClass('lesson_div');
 				$('#lesson_div').toggleClass('lesson_div_show');
 				// $('#nav_courses').toggleClass('current_btn');
 				$('#assignment_section').toggleClass('marginMe');
 				$('#assignment_section').toggleClass('marginMe2');
 				$('#dashboard_content').toggleClass('dashboard_div');
 				$('#dashboard_content').toggleClass('dashboard_div_large');
 				$('#mailbox_div').toggleClass('mail_div_show');
 				$('#mailbox_div').toggleClass('mail_div');
 				$('#lesson_div_test').toggleClass('aa1');
 				$('#lesson_div_test').toggleClass('aa2');
 				$('#nav_icon_div').toggleClass('moveDown');
 				$('#nav_icon_div').toggleClass('icon_start');
 				$('#progress_bar').toggleClass('hide_progress');
 				$('#progress_bar').toggleClass('show_progress');
		    	$('#progression_track').removeAttr('height');
		    	$('#content_courses').removeAttr('height');
		    	$('#navigation_pane').removeAttr('height');
 				$('#search_bar').toggleClass('search_bar');
 				$('#search_bar').toggleClass('search_bar_compress');
 				$('#search_box').toggleClass('search_box');
 				$('#search_box_compress').toggleClass('search_box_compress');
 				$('#nav_courses_open').toggleClass('nav_courses_open');
 				$('#nav_courses_open').toggleClass('nav_courses_closed');
 				$('#nav_courses').toggleClass('nav_btns_2');
 				$('#nav_courses').toggleClass('nav_btns_2_blue');
 				$('#nav_courses').toggleClass('nav_courses_closed_btn');
 				$('#nav_courses').toggleClass('nav_courses_open_btn');


 				var subject = "<?php echo $chapter_content[0]['subject'] ?>";
 				var chapter = "<?php echo $chapter_content[0]['chapter'] ?>";
 				$('#'+subject+'a'+chapter).toggleClass('hideMe');


				var left_div = $('#content_courses');
	    		var right_div = $('#progression_track');
	    		if(left_div.height() < right_div.height()){
		    		right_div.height(left_div.height()+100);
	    		}else{
	    			left_div.height(right_div.height());
	    		}

	    		var nav_pane = $('.navigation_pane');
	    		var course_pane = $('#course_body');
	    		nav_pane.height(course_pane.height()+180);
		   		
		    	var activeLi = $(".tabs > ul > li.active");
				var activeIndex = $(".tabs > ul > li").index(activeLi);
				$(".tab-content:first").show();
				var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
				$("#tab-container .arrow").css("left", arrowPos);

				$(window).resize(function() {
				  var activeLi = $(".tabs > ul > li.active");
				  var activeIndex = $(".tabs > ul > li").index(activeLi);
				  var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
				  $("#tab-container .arrow").css("left", arrowPos);
				});

				$(".tabs > ul > li ").click(function() {
				  var listItem = $(this).closest("li");
				  if ($(listItem).hasClass("active")) {
				    return;
				  }
				  $(".tab-content").hide();
				  var activeTab = $(listItem).attr("rel");
				  var tabList = $(".tabs > ul > li");
				  var arrow = $("#tab-container .arrow");
				  var index = tabList.index($(listItem));
				  var width = $(listItem).width();
				  var leftValue = width * (index + 0.5) - 12;
				  arrow.css("left", leftValue);
				  $("#" + activeTab).fadeIn();
				  
				  tabList.removeClass("active");
				  $(listItem).addClass("active");
				});
 			});
 			
 			$('.lesson_btns').hover(		
               function () {
                  $(this).css({"color":"rgb(95,150,215)"});
               }, 
               function () {
                  $(this).css({"color":"ghostwhite"});
               }
            );
 			$(function () {
  				$('#nav_courses').tooltip();
  				$('#nav_precourses').tooltip();
  				$('#nav_dashboard').tooltip();
  				$('#nav_messages').tooltip();
			});

		})
	</script>
	<style type="text/css">
		.hideMe{
			display: none;
		}
		.cursor_hand{
			cursor: pointer;
			cursor: hand;
		}
		.moveDown{
			margin-top: 16px;
		}
		.icon_start{
			margin-top: 30px;
		}
		.les_btns{
			background-color: none;
			background: none;
			color: ghostwhite;
			border: none;
			margin-left: 20px;
			font-size: 12px;
		}
		.lesson_btns_current{
			list-style-type:disc;
			color: rgba(58,153,216,1.02);
			font-size: 20px;
		}
		.color_white{
			color:ghostwhite;
			font-size: 12px;
		}
		.nav_courses_open{
			width: 50px; 
			height: 53px; 
			background-color: rgba(52,152,219,1.02); 
			margin-left: -15px;
			margin-top: -22px;
			padding-top: 19px;
			padding-left: 15px;
		}
		.nav_courses_closed{
			width: 50px; 
			height: 53px;
			margin-left: -15px; 
			padding-top: 19px;
			padding-left: 15px;
			margin-top: -22px;
		}
		#nav_courses_blue_bar{
			height: 53px;
			width: 222px;
			background-color: rgba(52,152,219,1.02);
			font-size: 16px;
			font-family: 'Source Sans Pro';
			font-weight: bold;
			padding-top: 15px;
			padding-left: 15px;
			color: white;
			position: fixed;
		}
	</style>
</head>
<body>
	

	<div style='display: inline-block;'>
			<div class='col-xs-2' class='icon_start' id='nav_icon_div'>
				<!-- <a href="/student/dashboard"><button id='nav_dashboard' data-toggle="tooltip" data-placement="right" title="Dashboard" class='nav_btns'></button></a> -->
				<!-- <button id='nav_precourses' data-toggle="tooltip" data-placement="right" title="Pre-Course Cirriculum" class='nav_btns'></button> -->
				<div id='nav_courses_open' class='nav_courses_closed'>
					<button id='nav_courses' data-toggle="tooltip" data-placement="right" title="Course Cirriculum" class='nav_btns_2 nav_courses_closed_btn'></button>
				</div>
				<!-- <a href="/student/message_board"><button id='nav_messages' data-toggle="tooltip" data-placement="right" title="Mailbox" class='nav_btns'></button></a> -->
				<!-- <a href="#"><button id='nav_schedule' data-toggle="tooltip" data-placement="right" title="Mailbox" class='nav_btns_2'></button></a> -->
			</div>

			<div class='nav_pane_div' id='nav_pane_div'>
					<div id='nav_courses_blue_bar'>
						Foundations
					</div>
				<div class='navigation_pane'>
					<?php foreach($subject_list AS $subject){ ?>
						<div class='col-offset-1'>
							<p class='nav_subject_title'><?=$subject['subject_title']?></p>
							<?php foreach($chapter_list AS $chapter){ ?>
								<?php if($chapter['subject'] == $subject['subject']){ ?>
									<div class='cursor_hand'>
										<?php if($chapter_content[0]['subject'] == $chapter['subject'] && $chapter_content[0]['chapter'] == $chapter['chapter']){ ?>
											<p class='nav_chapter_title' id='<?=$subject['subject']?>_<?=$chapter['chapter']?>'><i class="fa fa-caret-right" id='<?=$chapter['chapter']?>i<?=$chapter['subject']?>'></i><i class="fa fa-caret-down" id='<?=$chapter['chapter']?>i<?=$chapter['subject']?>open'></i>&nbsp&nbsp<?=$chapter['chapter_title']?></p>
											<script type="text/javascript">
													$('#<?=$subject['subject']?>a<?=$chapter['chapter']?>').toggleClass('hideMe');
													$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>').toggleClass('hideMe');
													$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>open').toggleClass('hideMe');
											</script>
										<?php }else{ ?>
											<p class='nav_chapter_title' id='<?=$subject['subject']?>_<?=$chapter['chapter']?>'><i class="fa fa-caret-right" id='<?=$chapter['chapter']?>i<?=$chapter['subject']?>'></i><i class="fa fa-caret-down" id='<?=$chapter['chapter']?>i<?=$chapter['subject']?>open'></i>&nbsp&nbsp<?=$chapter['chapter_title']?></p>
										<?php } ?>
									</div>
									<div id='<?=$subject['subject']?>a<?=$chapter['chapter']?>' >
										
										<script type="text/javascript">
											$(document).ready(function(){
												$('#<?=$subject['subject']?>a<?=$chapter['chapter']?>').toggleClass('hideMe');
												$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>').toggleClass('hideMe');
												$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>').toggleClass('hideMe');
												$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>open').toggleClass('hideMe');
											})
											$('#<?=$subject['subject']?>_<?=$chapter['chapter']?>').click(function(){				
													$('#<?=$subject['subject']?>a<?=$chapter['chapter']?>').toggleClass('hideMe');
													$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>').toggleClass('hideMe');
													$('#<?=$chapter['chapter']?>i<?=$chapter['subject']?>open').toggleClass('hideMe');
											})
										</script>


										<?php foreach($all_lesson_list AS $lessons){ ?>
											<?php if($lessons['subject'] == $subject['subject'] && $lessons['chapter'] == $chapter['chapter']){ ?>
												<?php if($lessons['subject'] == $chapter_content[0]['subject'] && $lessons['chapter'] == $chapter_content[0]['chapter'] && $lessons['lesson'] == $chapter_content[0]['lesson']){ ?>
													<form action='/course/lesson' method='POST'>
														<input type='hidden' name='subject' value='<?=$lessons['subject']?>'>
														<input type='hidden' name='chapter' value='<?=$lessons['chapter']?>'>
														<input type='hidden' name='lesson' value='<?=$lessons['lesson']?>'>
														<button type='submit' class='lesson_btns_current'><li><span class='color_white'><?=$lessons['lesson_title']?><span></li></button>
													</form>
												<?php }else{ ?>
													<form action='/course/lesson' method='POST'>
														<input type='hidden' name='subject' value='<?=$lessons['subject']?>'>
														<input type='hidden' name='chapter' value='<?=$lessons['chapter']?>'>
														<input type='hidden' name='lesson' value='<?=$lessons['lesson']?>'>
														<button type='submit' class='lesson_btns'><?=$lessons['lesson_title']?></button>
													</form>
												<?php } ?>
											<?php } ?>
										<?php } ?>
									</div>
								<?php } ?>
							<?php } ?>
						</div>
					<?php } ?>
				</div>
		
 
			</div>
		</div>
</body>
</html>





