<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Admin - Add Lesson</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
	


</head>
<body style='padding: 50px'>
	<div class='row'>
		<button class='pull-right'><a href="/admin/dashboard">Admin Dashboard</a></button>
		<button class='pull-right'><a href="/admin/add_lessons">Return To Add Lesson</a></button>
	</div>
	<div class='col-xs-6 col-xs-offset-3'>
		<div class='row'>
			<h3>Chapter: <?=$lesson[0]['chapter']?> / Lesson: <?=$lesson[0]['lesson']?></h3>
			<h3><?=$lesson['0']['lesson_title']?></h3>
		</div>

		<?php for($idx = 0; $idx < count($lesson); $idx++){ ?>
				<div class='row'>
				<?php if($lesson[$idx]['tag'] == 'h1'){ ?>
					<h1><?=$lesson[$idx]['content']?></h1>
				<?php }else if($lesson[$idx]['tag'] == 'h2'){ ?>  
					<h2><?=$lesson[$idx]['content']?></h2>
				<?php }else if($lesson[$idx]['tag'] == 'p'){ ?>
					<p><?=$lesson[$idx]['content']?></p>
				<?php }else if($lesson[$idx]['tag'] == 'l'){ ?>
					<a href="http://<?=$lesson[$idx]['content']?>"><?=$lesson[$idx]['content']?></a>
				<?php }else if($lesson[$idx]['tag'] == 'i'){ ?>
					<img src="/lesson_images/<?=$lesson[$idx]['content']?>">
				<?php }else if($lesson[$idx]['tag'] == 'v'){ ?>
					<p>Add Video Upload -- <?=$lesson[$idx]['content']?></p>
				<?php } ?>

			</div>
		<?php } ?>
	</div>
</body>
</html>
<?php var_dump($lesson) ?>
