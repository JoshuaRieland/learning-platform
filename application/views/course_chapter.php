<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Course</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/student_dashboard.css">
	<link rel="stylesheet" href="//code.jquery.com/ui/1.11.4/themes/smoothness/jquery-ui.css">
  	<link rel="stylesheet" href="/resources/demos/style.css">
  	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">
	<script type="text/javascript" src='/assets/js/JQueryLib.js'></script>
  	<script src="//code.jquery.com/ui/1.11.4/jquery-ui.js"></script>
  	<script>
  		$(document).ready(function(){
 			$(function() {
    			$( "#menu" ).menu();
  			});
  			// Assignment Tabs
 			$(function() {
    			$( "#tabs" ).tabs();
  			});

 			// Toggle On Hover Navigation Title
 			$(function () {
  				$('#award1').tooltip();
  				$('#award2').tooltip();
  				$('#award3').tooltip();
  				$('#award4').tooltip();
  				$('#award5').tooltip();
  				$('#award6').tooltip();
			});
 			var left_div = $('#content_courses');
		    var right_div = $('#progression_track');
		    
	    		right_div.height(left_div.height() + 100);
	    	

	    	var activeLi = $(".tabs > ul > li.active");
			var activeIndex = $(".tabs > ul > li").index(activeLi);
			$(".tab-content:first").show();
			var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
			$("#tab-container .arrow").css("left", arrowPos);

			$(window).resize(function() {
			  var activeLi = $(".tabs > ul > li.active");
			  var activeIndex = $(".tabs > ul > li").index(activeLi);
			  var arrowPos = activeLi.width() * (activeIndex + 0.5) - 12;
			  $("#tab-container .arrow").css("left", arrowPos);
			});

			$(".tabs > ul > li ").click(function() {
			  var listItem = $(this).closest("li");
			  if ($(listItem).hasClass("active")) {
			    return;
			  }
			  $(".tab-content").hide();
			  var activeTab = $(listItem).attr("rel");
			  var tabList = $(".tabs > ul > li");
			  var arrow = $("#tab-container .arrow");
			  var index = tabList.index($(listItem));
			  var width = $(listItem).width();
			  var leftValue = width * (index + 0.5) - 12;
			  arrow.css("left", leftValue);
			  $("#" + activeTab).fadeIn();
			  
			  tabList.removeClass("active");
			  $(listItem).addClass("active");
			});




		  		})
  	</script>

</head>
<body id='lesson_body'>

	<?php for($idx = 0; $idx < count($all_lesson_list); $idx++){
		if($all_lesson_list[$idx]['subject'] == $chapter_content[0]['subject'] && $all_lesson_list[$idx]['chapter'] == $chapter_content[0]['chapter'] && $all_lesson_list[$idx]['lesson'] == $chapter_content[0]['lesson']){
			$spot = $idx;
		}
	} ?>


	<?php $this->load->view('navigation_student'); ?>


<style type="text/css">
	html{
		background-color: rgb(45,52,66);
	}
	.assignment_section{ 
		margin-left: 50px;
		width: 100%;
	}
	.aa1{
		margin-top: 50px;
		display: inline-block;
		width: (100%-50px);
		margin-left: 50px;
		min-width: 800px;
		vertical-align: top;
	}
	.aa2{
		margin-top: 50px;
		display: inline-block;
		width: 79%;
		vertical-align: top;
	}
	#lesson_body{
		min-width: 1100px;
	}
	#lesson_div_test{
		min-width: 800px;
		/*margin: 25px;*/
		/*padding: 25px;*/
		height: 100%;
		position: absolute;
	}
	#progression_track{
		color: rgb(103,106,108);
		display: inline-block; 
		/*height: 100%;*/
		width: 20%; 
		vertical-align: top;
		margin-top: 10px;
		border-top: 5px solid rgb(234,234,234);
  		background-color: rgb(242,242,242);
  		/*padding: 25px;*/
	}
	#content_courses{
		color: rgb(103,106,108);
		display: inline-block; 
	    width: 78%; 
	        vertical-align: top; 
		border-top: 5px solid rgb(234,234,234);
     	background-color: rgb(242,242,242); 
		padding: 50px; 
		/*margin-left: 75px; */
		margin-top: 10px;
	}
	.prevNextBtn{
		font-size: 14px;
		color: rgb(92,149,218);
	}
	.course_wrapper{
		border-bottom:2px solid rgb(242,242,242);
		width: 100%;
		padding: 15px;
		background-color: rgb(247,247,247);
	}
		.course_lesson_title{
			font-size: 24px;
		}
		.h1_content{
			font-size: 24px;
			font-weight: bold;
		}
		.h2_content{
			font-size: 20px;
			font-weight: bold;
		}
		.p_content{
			font-size: 15px;
		}
		.boldMe{
			font-weight: bold;
		}

</style>





	<!-- Side Navigation Pane Icons-->
	<div id='course_body'>	
		<?php $this->load->view('student_sidebar'); ?>

		<div class='aa1' id='lesson_div_test'>
			<div class='course_wrapper'>
				<!-- Course Content -->
				<div id='content_courses'>
					<!-- Lesson Title -->
					<div class='row'>
						<p class='course_lesson_title'><span class='boldMe'>Lesson&nbsp<?=$chapter_content[0]['chapter']?>.<?=$chapter_content[0]['lesson']?>:</span>&nbsp&nbsp<?=$chapter_content[0]['lesson_title']?></p>
					</div>
					<!-- Lesson Content -->
					<?php for($idx = 0; $idx < count($chapter_content); $idx++){ ?>
						<div class='row'>
							<?php if($chapter_content[$idx]['tag'] == 'h1'){ ?>
								<p class='h1_content'><?=$chapter_content[$idx]['content']?></h1>
							<?php }else if($chapter_content[$idx]['tag'] == 'h2'){ ?>  
								<p class='h2_content'><?=$chapter_content[$idx]['content']?></h2>
							<?php }else if($chapter_content[$idx]['tag'] == 'p'){ ?>
								<p class='p_content'><?=$chapter_content[$idx]['content']?></p>
							<?php }else if($chapter_content[$idx]['tag'] == 'l'){ ?>
								<a href="http://<?=$chapter_content[$idx]['content']?>"><?=$chapter_content[$idx]['content']?></a>
							<?php }else if($chapter_content[$idx]['tag'] == 'i'){ ?>
								<img src="/lesson_images/<?=$chapter_content[$idx]['content']?>">
							<?php }else if($chapter_content[$idx]['tag'] == 'v'){ ?>
								<p>Add Video Upload -- <?=$chapter_content[$idx]['content']?></p>
							<?php }else if($chapter_content[$idx]['tag'] == 'b2i'){ ?>
								<?php $pictures = explode('/&/', $chapter_content[$idx]['content']) ?>
								<div class='col-xs-6'>
									<img src="/lesson_images/<?=$pictures[0]?>">
								</div>
								<div class='col-xs-6'>
									<img src="/lesson_images/<?=$pictures[1]?>">
								</div>
							<?php }else if($chapter_content[$idx]['tag'] == 'b3i'){ ?>
								<?php $pictures = explode('/&/', $chapter_content[$idx]['content']) ?>
								<div class='col-xs-4'>
									<img src="/lesson_images/<?=$pictures[0]?>">
								</div>
								<div class='col-xs-4'>
									<img src="/lesson_images/<?=$pictures[1]?>">
								</div>
								<div class='col-xs-4'>
									<img src="/lesson_images/<?=$pictures[2]?>">
								</div>
							<?php }else if($chapter_content[$idx]['tag'] == 'b4i'){ ?>
								<?php $pictures = explode('/&/', $chapter_content[$idx]['content']) ?>
								<div class='col-xs-3'>
									<img src="/lesson_images/<?=$pictures[0]?>">
								</div>
								<div class='col-xs-3'>
									<img src="/lesson_images/<?=$pictures[1]?>">
								</div>	
								<div class='col-xs-3'>
									<img src="/lesson_images/<?=$pictures[2]?>">
								</div>	
								<div class='col-xs-3'>
									<img src="/lesson_images/<?=$pictures[3]?>">
								</div>	
							<?php } ?>

						</div>
					<?php } ?>
					<!-- Prev & Next Lesson -->
					<div class='row'>
						<?php if($spot != (count($all_lesson_list)-1)){ ?>
						<form action='/course/lesson' method='POST' class='prev_next_btns pull-right'>
							<input type='hidden' name='subject' value='<?=$all_lesson_list[$spot+1]['subject']?>'>
							<input type='hidden' name='chapter' value='<?=$all_lesson_list[$spot+1]['chapter']?>'>
							<input type='hidden' name='lesson' value='<?=$all_lesson_list[$spot+1]['lesson']?>'>
							<button type='submit' class='prev_next_btns' style='width: 100%;'><p class='prevNextBtn'><span class='prev_next_btn_bold'>Next Lesson:&nbsp</span><?=$all_lesson_list[$spot+1]['lesson_title']?>&nbsp<i class="fa fa-long-arrow-right"></i></p></button>
						</form>
						<?php } ?>
						<?php if($spot != 0){ ?>
						<form action='/course/lesson' method='POST' class='prev_next_btns pull-left'>
							<input type='hidden' name='subject' value='<?=$all_lesson_list[$spot-1]['subject']?>'>
							<input type='hidden' name='chapter' value='<?=$all_lesson_list[$spot-1]['chapter']?>'>
							<input type='hidden' name='lesson' value='<?=$all_lesson_list[$spot-1]['lesson']?>'>
							<button type='submit' class='prev_next_btns' style='width: 100%;'><p class='prevNextBtn'><i class="fa fa-long-arrow-left"></i><span class='prev_next_btn_bold'>&nbspPrevious Lesson:&nbsp</span><?=$all_lesson_list[$spot-1]['lesson_title']?></p></button>
						</form>
						<?php } ?>
					</div>

				</div>
				<!-- Progression Track -->
				<div class='pull-right' id='progression_track'>
					<div style='width: 100%; height: 40px; border-bottom: 2px solid rgb(234,234,234); padding-top: 9px; padding-left: 20px;'>
						<span style='font-size: 14px; font-weight: bold; color: rgb(103,106,108);'><i class="fa fa-caret-down"></i> Milestones</span>
						<i class='fa fa-gear pull-right' style='margin-right: 25px; margin-top: 2px; color: rgb(103,106,108)'></i>
					</div>
					<p style='padding: 15px;'>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
					tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
					quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
					consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
					cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
					proident, sunt in culpa qui officia deserunt mollit anim id est laborum.lorem
				</p>
				</div>
			</div>
			<!-- Assignment -->
			<div  style='background-color: white; width: (100%); '>
				<div class="tabs" style='background-color: white;'>
				  <ul>
				    <li class="active" style='background-color: white;' rel="tab1">Quiz</li>
				    <li style='background-color: white;' rel="tab2">Assignment</li>
				    <li style='background-color: white;' rel="tab3">Feedback</li>
				    <li style='background-color: white;' rel="tab4">Resources</li>
				  </ul>
				  
				  <div id="tab-container">
				    <div class="arrow"></div>
				    <div id="tab1" class="tab-content">
			    		<div class='row'>
			    			<div style='width:70%; display: inline-block; padding: 20px;'>
				    			<p class='quiz_title'><?=$chapter_content[0]['lesson_title']?>&nbspQuiz</p>
				    			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    			tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    			quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    			consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    			cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    			proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    		</p>
				    		</div>
				    		<div style='width:150px; margin-left: 75px;  margin-top: 25px; border: 1px solid black; height: 50px; display: inline-block; vertical-align: top; background-color: rgb(255,255,255); border: 4px solid  rgb(234,234,234);'>
				    			<p style='display:inline-block; height: 25px; margin-top: 12px; vertical-align: top; margin-left: 10px; font-weight: bold;'>Your Score:</p>
				    			<?php if($quiz_score[$chapter_content[0]['subject'].'_'.$chapter_content[0]['chapter'].'_'.$chapter_content[0]['lesson']] == null){ ?>
				    				<p style='display:inline-block; color: rgb(95,150,215); height: 25px; margin-top: 6px; vertical-align: top; font-size: 20px;'>&nbspN/A</p>
				    			<?php }else{ ?>
				    				<p style='display:inline-block; color: rgb(95,150,215); height: 25px; margin-top: 6px; vertical-align: top; font-size: 20px;'>&nbsp<?=(number_format((float)$quiz_score[$chapter_content[0]['subject'].'_'.$chapter_content[0]['chapter'].'_'.$chapter_content[0]['lesson']], 2, '.','')*100)?>%</p>
				    			<?php } ?>
				    		</div>
			    		</div>
			    		<form action='/course/quiz' method='POST' id='quiz_form'>
				    			<input type='hidden' name='subject' value='<?=$chapter_content[0]['subject']?>'>
				    			<input type='hidden' name='chapter' value='<?=$chapter_content[0]['chapter']?>'>
				    			<input type='hidden' name='lesson' value='<?=$chapter_content[0]['lesson']?>'>
				    			<input type='hidden' name='quiz_lesson' value="<?=$chapter_content[0]['subject']?>_<?=$chapter_content[0]['chapter']?>_<?=$chapter_content[0]['lesson']?>">
				    			<?php $quiz_num = 0; ?>
				    			<?php foreach($quiz AS $q){ ?>
				    				<?php $quiz_num++; ?>
					    			<div class='question_box'>
						    			<p class='question_line'><span class='quiz_question_number'><?=$quiz_num?></span><?=$q['question']?></p>
						    			<div class='row quiz_answer_row'><input type='radio' class='quiz_answers' name='question<?=$quiz_num?>' value='1'>&nbsp&nbsp <?=$q['answer1']?></input></div>
						    			<div class='row quiz_answer_row'><input type='radio' class='quiz_answers' name='question<?=$quiz_num?>' value='2'>&nbsp&nbsp <?=$q['answer2']?></input></div>
						    			<?php if($q['answer3'] != 'NA'){ ?>
						    				<div class='row quiz_answer_row'><input type='radio' class='quiz_answers' name='question<?=$quiz_num?>' value='3'>&nbsp&nbsp <?=$q['answer3']?></input></div>
						    			<?php } ?>
						    			<?php if($q['answer4'] != 'NA'){ ?>
						    				<div class='row quiz_answer_row'><input type='radio' class='quiz_answers' name='question<?=$quiz_num?>' value='4'>&nbsp&nbsp <?=$q['answer4']?></input></div>
						    			<?php } ?>
						    			<input type='hidden' name='answer<?=$quiz_num?>' value='<?=$q['correct']?>'>
					    			</div>
				    			<?php } ?>

				    		<div class='row'>
				    			<button type='submit' id='quiz_btn'>Submit For Results</button>
				    		</div>
				    	</form>
				    </div>
				    <div id="tab2" class="tab-content">
			    		<p>Proin elit arcu, rutrum commodo, vehicula tempus, commodo a, risus. Curabitur nec arcu. 
			    		Donec sollicitudin mi sit amet mauris. Nam elementum quam ullamcorper ante. Etiam aliquet massa et lorem. 
			    		Mauris dapibus lacus auctor risus. Aenean tempor ullamcorper leo. Vivamus sed magna quis ligula eleifend adipiscing. 
			    		Duis orci. Aliquam sodales tortor vitae ipsum. Aliquam nulla. Duis aliquam molestie erat. Ut et mauris vel pede 
			    		varius sollicitudin. Sed ut dolor nec orci tincidunt interdum. Phasellus ipsum. Nunc tristique tempus lectus.Lorem 
			    		ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
			    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
			    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
			    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
			    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
			    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
			    		<hr>
				    	<p class='centerText'>Accepted Formats</p>
				    	<p class='centerText'> .html / .doc / .xsl / .php / .css / .csv / .zip</p>
				    	<hr>
				    	<p class='centerText'>Max File Size: 20 MB</p>
			    		<hr>
				    	<?php echo form_open_multipart('upload_project/do_upload') ?>
			  				<div class='col-sm-10 col-sm-offset-1'>
			  					<input type='file' class='form-control' name='userfile' id='userfile'>
			  				</div>
				  			<br><br>
				  			<div class='col-sm-10 col-sm-offset-1'>
								<button type='submit' style='width: 100%;'>Submit For Review&nbsp<span class='glyphicon glyphicon-import'></button>
							</div>
				    	</form>
				    </div>
				    <div id="tab3" class="tab-content">
				      <label for='#a1'>Mentor</label>
				    	<p class='project_discussion' id='a1'>
				    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    	</p>
				    	<label for='#a2'>Me</label>
				    	<p class='project_discussion' id='a2'>
				    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    	</p>
				    	<label for='#a3'>Mentor</label>
				    	<p class='project_discussion' id='a3'>
				    		Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				    		tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				    		quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				    		consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				    		cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				    		proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
				    	</p>
				    	<!-- <form action='' method='POST'> -->
				    		<textarea name='content'></textarea>
				    		<button type='submit'>Post</button>
				    	<!-- </form> -->
				    </div>
				    <div id="tab4" class="tab-content">
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br>
				      Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
				      tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
				      quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
				      consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
				      cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
				      proident, sunt in culpa qui officia deserunt mollit anim id est laborum.<br><br>
				    </div>
				  </div>
				</div>
			</div>
			
			
		
		</div>

<style type="text/css">
	.marginMe{
		margin-left: 125px;
		display: inline-block;
		vertical-align: top;
	}
	.marginMe2{
		margin-left: 325px;
		display: inline-block;
		vertical-align: top;
	}

</style>
	</div>
</body>
</html>

<!-- 
<br><br><br><br>
<div class='col-xs-12'>

<hr>
<?php var_dump($subject_list) ?>
<hr>
<?php var_dump($chapter_list) ?>
<hr>

<?php var_dump($all_lesson_list) ?>

<hr>
<?php var_dump($lesson_list) ?>
<hr>
<?php var_dump($chapter_content) ?>
<hr>
</div> -->