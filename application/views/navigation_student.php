<html>
<head>
	<title>Navigation - Student</title>
	<link href='http://fonts.googleapis.com/css?family=Source+Sans+Pro:400,700,300,600' rel='stylesheet' type='text/css'>
	<style type="text/css">
		#logo_div{
			position: fixed;
			background-color: rgb(95,150,215);
			width: 101%;
			min-width: 925px;
			height: 55px;
			z-index: 900;
		}
		#logo{
			display: inline-block;
			background-image: url('/assets/images/ux-academy-logo.png');
			background-size: cover;
			height: 55px;
			width: 303px;
		}
		#nav_mail{
			display: inline-block;
			height: 40px;
			width: 40px;
			color: black;
			background: none;
			border: none;
		}
		.navLinks{
			display: inline-block;
			/*margin-left: 10px;*/
			color: rgb(48,80,125);
		}
			.navLinksColor{
				color: rgb(48,80,125);
			}
		.logout_move_up{
			margin-top: 10px;
			margin-right: 5px;
		}
		#profile_pic{
			margin-left: 15px;
			width: 50px;
			height: 55px;
			display: inline-block;
			background-color: rgb(32,37,44);
		}
		.show_progress{
			margin-left: 65px;
			width: 222px;
			height: 55px;
			display: inline-block;
			background-color: rgb(32,37,44);
		}
		.hide_progress{
			display: none;
		}
		
		@media screen and (min-width: 1100px) {
			#search_bar{
				display: inline-block;
				vertical-align: top;
				margin-top: 10px;
			}
				.search_bar{
					width: 400px;
					margin-left: 80px;
				}
				.search_bar_compress{
					margin-left: 10px;
				}
					#search_box{
						background-color: rgb(63,105,157);
						border: rgb(63,105,157);
						height: 33px;
						padding-left: 3px;
						color: ghostwhite;
						display: inline-block;
						vertical-align: top;		
					}
						.search_box{
							width: 280px;
						}
						.search_box_compress{
							width: 280px;
						}
		}

		@media screen and (max-width: 1099px) {
			#search_bar{
				display: inline-block;
				vertical-align: top;
				margin-top: 10px;
			}
				.search_bar{
					width: 400px;
					margin-left: 80px;
				}
				.search_bar_compress{
					margin-left: 10px;
					width: 200px;
				}
					#search_box{
						background-color: rgb(63,105,157);
						border: rgb(63,105,157);
						height: 33px;
						padding-left: 3px;
						color: ghostwhite;
						display: inline-block;
						vertical-align: top;		
					}
						.search_box{
							width: 280px;
						}
						.search_box_compress{
							width: 150px;
						}
		}


			::-webkit-input-placeholder { color:   ghostwhite;}		/* WebKit, Blink, Edge */
			:-moz-placeholder { color:   ghostwhite; opacity:  1;}	/* Mozilla Firefox 4 to 18 */
			::-moz-placeholder { color:   ghostwhite;opacity:  1;}  /* Mozilla Firefox 19+ */
			:-ms-input-placeholder { color:   ghostwhite;}			/* Internet Explorer 10-11 */
			
			#search_icon_wrapper{
				display: inline-block;
				margin-left: -5px;
				background-color: rgb(53,88,159);
				border: rgb(53,88,159); 
				height: 33px;
				width: 43px;
			}
			#search_icon{
				vertical-align: top;
				background-color: rgb(53,88,159);
				border: rgb(53,88,159); 
				background-image: url('/assets/images/search_icon1.png');
				background-size: cover;
				background-repeat: none;
				background-position: center;
				width: 75%;
				height: 75%;
				margin-top: 12.5%;
				margin-left: 12.5%;
			}
			
			#progression_bar{
				height: 10px;
				margin-top: 0px;
			}
			#progress_color{
				background-color: rgba(58,153,216,1.02);
			}

	</style>
</head>
<body>
	<div class='row' id='logo_div'>
		<div id='profile_pic' style='position: fixed;'>
			<!-- <div style='width: 60px; height: 60px; margin-left: 22px; margin-top: 5px; background-color: red; border-radius: 50%; border: 3px ridge green;'> -->
				<a href="/student/dashboard"><img src="/uploads/student_profile_pic_17.jpg" style='width: 25px;  border-radius: 50%; margin-left: 25%; margin-top: 25%'></a>
			<!-- </div> -->
		</div>
		<div class='hide_progress' id='progress_bar'>
			<div style='width: 80%; margin-left: 10%;'>
				<p class='progression'>Student Progress: 70%</p>
				<div class="progress" id='progression_bar'>
  					<div class="progress-bar" role="progressbar" aria-valuenow="70" aria-valuemin="0" aria-valuemax="100" style="width:70%" id='progress_color'>
  					</div>
  				</div>
			</div>
		</div>
		<!-- <div id='logo'></div> -->
		
		<div class='search_bar' id='search_bar'>
			<input type='text' name='search_criteria' class='search_box' id='search_box' placeholder='Search course...'>
			<div id='search_icon_wrapper'>
				<button type='submit' id='search_icon'></button>
			</div>
		</div>
		
		
		<ul class='pull-right logout_move_up'>
			<!-- <li class='navLinks'>Welcome to our learning platform.</li> -->
			<li class='navLinks'><a href="#"><button id='nav_mail' data-toggle="tooltip" data-placement="right" title="Notifications" ><span class='glyphicon glyphicon-bell navLinksColor'></span></button></a></li>
			<li class='navLinks'><a href="/student/message_board"><button id='nav_mail' data-toggle="tooltip" data-placement="right" title="Mailbox" ><span class='glyphicon glyphicon-envelope navLinksColor'></span></button></a></li>
			<li class='navLinks'><a href="/user/signout" class='logout'><h5 class='navLinksColor'>LogOut <i class='glyphicon glyphicon-log-out'></i></h5></a></li>
		</ul>
		
		
		
	</div>
</body>
</html>