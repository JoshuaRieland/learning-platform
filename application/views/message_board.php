<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Message Board</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/student_dashboard.css">
	<script src="/assets/js/JQueryLib.js"></script>
	<script src="/assets/js/bootstrap.min.js"></script>
	<script type="text/javascript">
	  $(document).ready(function(){
	 	$('#outbox').hide();
	 	$('#message_mentor').hide();
	 	$('#show_inbox').removeClass('btn_shadow').addClass('btn_pressed');

	 	$('#show_inbox').click(function(){
	 		$('#outbox').hide();
	 		$('#message_mentor').hide();
	 		$('#inbox').show();
	 		$('#show_inbox').removeClass('btn_shadow').addClass('btn_pressed');
	 		$('#show_outbox').removeClass('btn_pressed').addClass('btn_shadow');
	 		$('#show_message_mentor').removeClass('btn_pressed').addClass('btn_shadow');
	 	});
	 	$('#show_outbox').click(function(){
	 		$('#inbox').hide();
	 		$('#message_mentor').hide();
	 		$('#outbox').show();
	 		$('#show_outbox').removeClass('btn_shadow').addClass('btn_pressed');
	 		$('#show_inbox').removeClass('btn_pressed').addClass('btn_shadow');
	 		$('#show_message_mentor').removeClass('btn_pressed').addClass('btn_shadow');
	 	});
	 	$('#show_message_mentor').click(function(){
	 		$('#outbox').hide();
	 		$('#inbox').hide();
	 		$('#message_mentor').show();
	 		$('#show_message_mentor').removeClass('btn_shadow').addClass('btn_pressed');
	 		$('#show_outbox').removeClass('btn_pressed').addClass('btn_shadow');
	 		$('#show_inbox').removeClass('btn_pressed').addClass('btn_shadow');
	 		$('#subject').focus();
	 	})
	  	$(function() {
	    	$("input:file").change(function (){
	    	   	$('#attachment').attr('value', 'yes');
	 	    });
	  	});
	  })
</script>
</head>
<BODY>
	<style type="text/css">
		body{
			min-width: 1100px;
		}
	</style>
	<?php $this->load->view('navigation_student'); ?>
		<?php $this->load->view('student_sidebar_mail'); ?>
	<div class='mail_div_show' id='mailbox_div'>	
		<div class='row'>
			<div class='col-xs-2'>
				<button class='mailbox_btns btn_shadow' id='show_inbox'>Inbox</button>
				<button class='mailbox_btns btn_shadow' id='show_outbox'>Outbox</button>
				<button class='mailbox_btns btn_shadow' id='show_message_mentor'>Message Mentor</button>	
			</div>
			<!-- START OUTBOX -->
			<div class='col-xs-10' id='outbox'>
				<table class='table table-responsive table-hover'>
					<thead>
						<tr>
							<th style='width: 40px;'></th> <!-- Viewed Open/Closed Envelope Icon -->
							<th style='width: 40px;'><span class='glyphicon glyphicon-paperclip'></span></th> <!-- Attachment? Paperclip Icon - yes / empty - no -->
							<th style='width: 100px;'>Date</th>
							<th style='width: 100px;'>Time</th>
							<th>Subject</th>
						</tr>
					</thead>
					<tbody>
						<?php foreach($outbox AS $message){ ?>	
							<tr>
								<!-- Viewed Message? -->
								<td>
									<?php if($message['viewed'] == 'no' AND $message['send_from_id'] != $_SESSION['userID']){ ?>
										<span class='glyphicon glyphicon-envelope' style='color: limegreen'></span>
									<?php }else if($message['viewed'] == 'yes' AND $message['send_from_id'] != $_SESSION['userID']){ ?>
										<span class='glyphicon glyphicon-envelope'></span>
									<?php }else{ ?>
										<span class='glyphicon glyphicon-send'></span>
									<?php } ?>
								</td>
								<!-- Attachment? -->
									<?php if($message['attachment'] == 'none'){ ?>
										<td></td>
									<?php }else{ ?>
										<td id='download_attached_<?=$message['id']?>'>
											<form action='/message/download_attachment' method='POST'>
												<input type='hidden' name='attachment' value='<?=$message['attachment']?>'>
												<button type='submit'><span class='glyphicon glyphicon-paperclip'></span></button>
											</form>
											
										</td>
									<?php } ?>
								<!-- Sent Date -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?=date('m/d/y',strtotime($message['created_at']))?>
								</td>
								<!-- Sent Time -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?=date('h:i a',strtotime($message['created_at']))?>
								</td>
								<!-- Subject -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?=$message['subject']?>
								</td>
							</tr>					
						 	<div class="modal fade" id="message<?=$message['id']?>" tabindex="-1" role="dialog" aria-labelledby="modal_label_<?=$message['id']?>">
						  		<div class="modal-dialog" role="document">
						    		<div class="modal-content">
						    			<div class="modal-header">
						        			<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						        			<h4 class="modal-title centerText" id="modal_label_<?=$message['id']?>"><?=$message['subject']?></h4>
						      			</div>
						      			<div class="modal-body">
						        			<div>
						        				<?=$message['content']?>
						        			</div>
							        		<br><br>
						      			</div>
						      			<div class="modal-footer">
						        			<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
						      			</div>
						    		</div>
						  		</div>
							</div>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<!-- END OUTBOX -->
			<!-- START INBOX -->
			<div class='col-xs-10' id='inbox'>
				<table class='table table-responsive table-hover'>
					<thead>
						<tr>
							<th style='width: 40px;'></th> <!-- Viewed Open/Closed Envelope Icon -->
							<th style='width: 40px;'><span class='glyphicon glyphicon-paperclip'></span></th> <!-- Attachment? Paperclip Icon - yes / empty - no -->
							<th style='width: 100px;'>Date</th>
							<th style='width: 100px;'>Time</th>
							<th style='width: 150px;'>Sender</th>
							<th>Subject</th>
							<th style='width: 40px'></th> <!-- Actions: Delete - Trash Icon -->
						</tr>
					</thead>
					<tbody>
						<?php foreach($inbox AS $message){ ?>
							
							<form action='/message/viewed' action='POST' id='form<?=$message['id']?>'>
								<input type='hidden' name='message_id' value='<?=$message['id']?>'>
							</form>
							<tr>
								<!-- Viewed Message? -->
								<td>
									<?php if($message['viewed'] == 'no' AND $message['send_from_id'] != $_SESSION['userID']){ ?>
										<span class='glyphicon glyphicon-envelope' style='color: limegreen'></span>
									<?php }else if($message['viewed'] == 'yes' AND $message['send_from_id'] != $_SESSION['userID']){ ?>
										<span class='glyphicon glyphicon-envelope'></span>
									<?php }else{ ?>
										<span class='glyphicon glyphicon-send'></span>
									<?php } ?>
								</td>
								<!-- Attachment? -->
									<?php if($message['attachment'] == 'none'){ ?>
										<td></td>
									<?php }else{ ?>
										<td id='download_attached_<?=$message['id']?>'>
											<form action='/message/download_attachment' method='POST'>
												<input type='hidden' name='attachment' value='<?=$message['attachment']?>'>
												<button type='submit'><span class='glyphicon glyphicon-paperclip'></span></button>
											</form>
											
										</td>



									<?php } ?>
								<!-- Sent Date -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?=date('m/d/y',strtotime($message['created_at']))?>
								</td>
								<!-- Sent Time -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?=date('h:i a',strtotime($message['created_at']))?>
								</td>
								<!-- Sent From User Name -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?php if($message['send_from_id'] == $_SESSION['userID']){ ?>
										<p></p>
									<?php }else{ ?>
										<p>Mentor</p>
									<?php } ?>
								</td>
								<!-- Subject -->
								<td data-toggle='modal' data-target='#message<?=$message['id']?>'>
									<?=$message['subject']?>
								</td>
								<!-- Actions - Delete -->
								<td>
									<span class='glyphicon glyphicon-trash'></span>
								</td>
							</tr>
							
							<?php if($message['viewed'] == 'no'){ ?>
							
							<!-- Show Message In Modal -->
							<div class="modal fade" id="message<?=$message['id']?>" tabindex="-1" role="dialog" aria-labelledby="modal_label_<?=$message['id']?>">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title centerText" id="modal_label_<?=$message['id']?>"><?=$message['subject']?></h4>
							      </div>
							      <div class="modal-body">
							        	<div class='col-xs-8 col-xs-offset-2'>
							        		<?=$message['content']?>
							        	</div>
								        <br><br>
								        <hr>
								        <h5>Reply:</h5>
								        <form action='/message/reply' method='POST'>
								        	<input type='hidden' name='send_to_id' value='<?=$message['send_from_id']?>'>
								        	<input type='hidden' name='send_from_id' value='<?=$_SESSION['userID']?>'>
								        	<label class='reply_label'>Subject</label>
								        	<input type='text' class='reply_input' name='subject' value='Re: <?=$message['subject']?>' required>
								        	<br>
								        	<label class='reply_label'>Message Content</label>
								        	<textarea class='reply_input' name='content' required></textarea>
								        	<button type='submit' style='width: 30%; margin-left: 35%;'>SEND&nbsp<span class='glyphicon glyphicon-send'></button>
								        </form>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" id='modal_close<?=$message['id']?>'>Close</button>
							      </div>
							    </div>
							  </div>
							 <?php }else{ ?>
							 <div class="modal fade" id="message<?=$message['id']?>" tabindex="-1" role="dialog" aria-labelledby="modal_label_<?=$message['id']?>">
							  <div class="modal-dialog" role="document">
							    <div class="modal-content">
							      <div class="modal-header">
							        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
							        <h4 class="modal-title centerText" id="modal_label_<?=$message['id']?>"><?=$message['subject']?></h4>
							      </div>
							      <div class="modal-body">
							        	<div class='col-xs-8 col-xs-offset-2'>
							        		<?=$message['content']?>
							        	</div>
								        <br><br>
								        <hr>
								        <h5>Reply:</h5>
								        <form action='/message/reply' method='POST'>
								        	<input type='hidden' name='send_to_id' value='<?=$message['send_from_id']?>'>
								        	<input type='hidden' name='send_from_id' value='<?=$_SESSION['userID']?>'>
								        	<label class='reply_label'>Subject</label>
								        	<input type='text' class='reply_input' name='subject' value='Re: <?=$message['subject']?>' required>
								        	<br>
								        	<label class='reply_label'>Message Content</label>
								        	<textarea class='reply_input' name='content' required></textarea>
								        	<button type='submit' style='width: 30%; margin-left: 35%;'>SEND&nbsp<span class='glyphicon glyphicon-send'></button>
								        </form>
							      </div>
							      <div class="modal-footer">
							        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
							      </div>
							    </div>
							  </div>
							 <?php } ?>
							<script type="text/javascript">
								$("#modal_close<?=$message['id']?>").click(function(){
									$("#form<?=$message['id']?>").submit();
								})
							</script>
							</div>
						<?php } ?>
					</tbody>
				</table>
			</div>
			<!-- END INBOX -->
			<div class='col-xs-10' id='message_mentor'>
				<!-- <div class='col-xs-12 col-sm-10 col-sm-offset-1 col-md-8 col-md-offset-2' style='border:1px solid black;'> -->
					<h3 class='centerText'>Quick Message To Mentor</h3>
					<?php if(isset($_SESSION['upload_attachment_msg'])){ ?>
						<?php if ($_SESSION['upload_attachment_msg'] == ''){ ?>
							<p></p>
						<?php }else{ ?>
							<p class='centerText'><?=$_SESSION['upload_attachment_msg']?></p>
						<?php } ?>
					<?php } ?>


					<?php echo form_open_multipart('upload_message_attachment/do_upload') ?>
						<input type='hidden' name='send_to_id' value='<?=INTVAL($mentor_userID)?>'>
						<input type='hidden' name='send_from_id' value='<?=INTVAL($_SESSION['userID'])?>'>
						<input type='hidden' name='attachment' value='no' id='attachment'>
						<div class="form-group">
					    	<label for="subject" class="col-sm-3 control-label message_form_label">Subject</label>
					    	<div class="col-sm-9">
					      		<input type="text" class="form-control" name='subject' id="subject" placeholder="Subject" required>
					    	</div>
					  	</div>
					  	<div class="form-group">
					    	<label for="content" class="col-sm-3 control-label message_form_label">Message Content</label>
					    	<div class="col-sm-9">
								<textarea class='form-control message_content_input' name='content' id='content' placeholder='Enter Message Content' required></textarea>
					    	</div>
					  	</div>
					  	<div class='form-group'>
					  		<label for='userfile' class='col-sm-3 control-label message_form_label'>Attachment</label>
					  		<div class='col-sm-9'>
					  			<input type='file' class='form-control' name='userfile' id='userfile'>
					  		</div>
					  	</div>
					  	<div class='col-sm-5 col-sm-offset-4'>
							<button type='submit' style='width: 50%; margin-left: 25%;'>SEND&nbsp<span class='glyphicon glyphicon-send'></button>
						</div>
					</form>
				<!-- </div>	 -->
			</div>
		</div>	
	</body>
</html>



<!-- <br><br><br><br><br> -->
<!-- <hr> -->
<!-- <?php var_dump($_SESSION) ?> -->
<!-- <hr> -->
<!-- <?php var_dump($mentor_userID) ?> -->
<!-- <hr> -->
<!-- <?php var_dump($inbox) ?> -->
<!-- <hr> -->
<!-- <?php var_dump($outbox) ?> -->



