<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>UX Academy Student Registration Step 5</title>
	<link rel="stylesheet" type="text/css" href="/assets/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/style.css">
	<link rel="stylesheet" type="text/css" href="/assets/css/step_bar.css">
</head>
<body>
	<div class='registration_container'>	
		
	<div class='row'>
			<div class='progress_bar'>
				<ol class="stepBar step5">
					
					<li class="step">My Info</li>
					<li class='step'>Mentor</li>
					<li class="step">Meeting</li>
					<li class="step">Materials</li>
					<li class="step completed current">Pre-course</li>

				</ol>
			</div>
				<a href="/user/signout" class='pull-right'>LogOut <i class='glyphicon glyphicon-log-out'></i></a>
			<div class='progress_bar_small' hidden='true'>
				dhdh
			</div>
		</div>

		<div class='prep_content'>
			<div class='row'>
				<h4 class='col-md-5 col-sm-6 col-xs-8'>
					UX Academy Pre-Course Requirements:
				</h4>
			</div>
			<div class='row'>
				<div class='col-md-6 col-sm-8 col-xs-12'>
					<h5>A minimum of basic understanding and knowledge of HTML and CSS is fundamental to your success in UX Academy's 6 or 12-week course curriculum. The course material will build off of this understanding. Please dedicate enough time to complete the pre-course assignments.</h5>
				</div>
			</div>
			<div class='row'>
				<ul class='col-xs-11 precourse_list'>
					<li><h5>Acquire an understanding of HTML/CSS:</h5>
						<ul style='list-style: inherit;'>
							<li><a href="http://www.codeacademy.com">Codeacademy.com</a></li>
							<li><a href="http://www.w3schools.com">w3schools.com</a></li>
							<li><a href="http://www.learn.shayhowe.com">learn.shayhowe.com</a></li>
							<li><a href="http://www.dash.generalassemb.ly">dash.generalassemb.ly</a></li>
						</ul>
					</li>
					<li><h5>Read through the following sites:</h5>
						<ul style='list-style: inherit;'>
							<li><a href="http://www.vitsoe.com/gb/about/good-design">Ten Principles For Good Design</a></li>
							<li><a href="http://www.photoshopetiquette.com">Photoshop Etiquette</a></li>
						</ul>
					</li>


				</ul>
			</div>
		</div>
		<div class='row'>
			<a href="/goto/learning-platform"><h2>Continue to Learning Platform</h2></a>
		</div>
	</div>
</body>
</html>