<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin_model extends CI_Model {

	public function register_new_student($student_email, $randPW, $start_date, $program_length){
		$query = 'INSERT INTO user (email, password, access, program_length, start_date) VALUES (?,?,?,?,?)';
		$this->db->query($query, array( 'email' => $student_email, 
										'password' => $randPW, 
										'access' => 'student', 
										'program_length' => $program_length, 
										'start_date' => $start_date)
									);
		$query1 = 'SELECT id FROM user WHERE email = ?';
		$userID = $this->db->query($query1, array('email' => $student_email))->row_array();
		$query2 = 'INSERT INTO meeting_days (userID) VALUES (?)';
		$this->db->query($query2, array('userID' => $userID['id']));
		$query3 = 'INSERT INTO student_info (userID) VALUES (?)';
		$this->db->query($query3, array('userID' => $userID['id']));
		$query4 = 'INSERT INTO quiz_grades (userID) VALUES (?)';
		$this->db->query($query4, array('userID' => $userID['id']));
		return;
	}

	public function get_all_students(){
		$query = "SELECT user.id, user.email, user.start_date, user.age, user.program_length, mentor_info.first_name AS mentor_firstname, mentor_info.last_name AS mentor_lastname, student_info.first_name, student_info.last_name, student_info.city, student_info.state, student_info.profile_pic FROM user LEFT JOIN mentor_info ON user.mentorID = mentor_info.id LEFT JOIN student_info ON user.id = student_info.userID WHERE access = ?";
		$all_students = $this->db->query($query, array('access' => 'student'))->result_array();
		return $all_students;
	}

	public function add_lesson_image($photo_name){
		$query = 'INSERT INTO lesson_images (photo_name) VALUES (?)';
		$this->db->query($query, array('photo_name' => $photo_name));
		return;
	}

	public function get_all_lesson_images(){
		$query =   'SELECT photo_name FROM lesson_images';
		$lesson_images = $this->db->query($query)->result_array();
		return $lesson_images;
	}

	public function add_lesson($lesson){
		$query = 'INSERT INTO lesson_page (subject, subject_title, chapter, chapter_title,lesson, lesson_title, position, tag, content) 
				  VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)';
		$this->db->query($query, array( 'subject' => $lesson[0], 
										'subject_title' => $lesson[1], 
										'chapter' => $lesson[2], 
										'chapter_title' => $lesson[3],
										'lesson' => $lesson[4], 
										'lesson_title' => $lesson[5],
										'position' => $lesson[6], 
										'tag' => $lesson[7], 
										'content' => $lesson[8])
										);
		return;
	}

	// public function add_2_image_row($lesson, $idx){
	// 	var_dump($lesson);
	// 	die(var_dump($idx));
	// 	$query = 'INSERT INTO lesson_page (subject, subject_title, chapter, chapter_title, lesson, lesson_title, position, tag, content, img_position';
	// }

	public function add_quiz_question($quiz){
		$query = 'INSERT INTO quiz (subject, chapter, lesson, question, answer1, answer2, answer3, answer4, correct) VALUES (?,?,?,?,?,?,?,?,?)';
		$this->db->query($query, array( 'subject' => INTVAL($quiz[0]), 'chapter' => INTVAL($quiz[1]), 'lesson' => INTVAL($quiz[2]), 'question' => $quiz[3], 'answer1' => $quiz[4], 'answer2' => $quiz[5], 'answer3' => $quiz[6], 'answer4' => $quiz[7], 'correct' => INTVAL($quiz[8])));
		return;
	}

	public function add_lesson_project($project){
		$query = 'INSERT INTO lesson_project (subject, chapter, lesson, project_title, project_description) VALUES (?,?,?,?,?)';
		$this->db->query($query, array( 'subject' => $project[0], 
										'chapter' => $project[1], 
										'lesson' => $project[2], 
										'project_title' => $project[3], 
										'project_description' => $project[4])
									);
		return;
	}

	public function get_one_lesson($subject, $chapter, $lesson){
		$query =   'SELECT * 
					FROM lesson_page 
					WHERE subject  = ? AND chapter = ? AND lesson = ? 
					ORDER BY position';
		$lesson = $this->db->query($query, array('subject' => $subject, 
												 'chapter' => $chapter, 
												 'lesson' => $lesson))->result_array();
		return $lesson;
	}

	public function get_current_chapters_lessons(){
		$query =   'SELECT subject, subject_title, chapter, chapter_title, lesson, lesson_title 
					FROM lesson_page 
					GROUP BY subject ,chapter , lesson_title 
					ORDER BY subject, chapter, lesson';
		$current_chapters_lessons = $this->db->query($query)->result_array();
		return $current_chapters_lessons;
	}

	public function get_subject_list(){
		$query = 'SELECT subject, subject_title 
				  FROM lesson_page 
				  GROUP BY subject';
		$subject_list = $this->db->query($query)->result_array();
		return $subject_list;
	}

	public function get_chapter_list(){
		$query = 'SELECT subject, chapter, chapter_title 
				  FROM lesson_page 
				  GROUP BY subject, chapter';
		$chapter_list = $this->db->query($query)->result_array();
		return $chapter_list;
	}

	public function get_lesson_list(){
		$query = 'SELECT subject, chapter, lesson 
		          FROM lesson_page 
		          GROUP BY subject, chapter, lesson';
		$lesson_list = $this->db->query($query)->result_array();
		return $lesson_list;
	}
	
}
