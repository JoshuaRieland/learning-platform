<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mentor_schedule_model extends CI_Model {

	// Block Single Day

	public function mentor_block_monday_time($time_slot){
		$mentorID = $_SESSION['mentorID'];
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE monday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE monday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE monday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE monday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE monday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE monday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE monday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE monday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE monday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE monday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE monday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE monday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE monday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE monday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE monday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE monday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE monday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE monday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE monday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE monday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE monday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE monday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE monday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE monday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE monday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE monday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE monday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE monday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE monday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE monday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE monday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE monday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE monday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE monday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE monday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE monday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE monday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE monday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE monday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE monday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE monday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE monday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE monday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE monday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE monday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE monday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE monday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE monday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -99, 'mentorID' => $mentorID));
		}
		return;
	}

	public function mentor_block_tuesday_time($time_slot){
		$mentorID = $_SESSION['mentorID'];
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE tuesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE tuesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE tuesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE tuesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE tuesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE tuesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE tuesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE tuesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE tuesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE tuesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE tuesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE tuesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE tuesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE tuesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE tuesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE tuesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE tuesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE tuesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE tuesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE tuesday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE tuesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE tuesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE tuesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE tuesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE tuesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE tuesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE tuesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE tuesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE tuesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE tuesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE tuesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE tuesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE tuesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE tuesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE tuesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE tuesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE tuesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE tuesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE tuesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE tuesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE tuesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE tuesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE tuesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE tuesday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE tuesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE tuesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE tuesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE tuesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -99, 'mentorID' => $mentorID));
		}
		return;
	}

	public function mentor_block_wednesday_time($time_slot){
		$mentorID = $_SESSION['mentorID'];
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE wednesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE wednesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE wednesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE wednesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE wednesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE wednesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE wednesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE wednesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE wednesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE wednesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE wednesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE wednesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE wednesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE wednesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE wednesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE wednesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE wednesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE wednesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE wednesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE wednesday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE wednesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE wednesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE wednesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE wednesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE wednesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE wednesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE wednesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE wednesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE wednesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE wednesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE wednesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE wednesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE wednesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE wednesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE wednesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE wednesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE wednesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE wednesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE wednesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE wednesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE wednesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE wednesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE wednesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE wednesday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE wednesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE wednesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE wednesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE wednesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -99, 'mentorID' => $mentorID));
		}
		return;
	}

	public function mentor_block_thursday_time($time_slot){
		$mentorID = $_SESSION['mentorID'];
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE thursday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE thursday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE thursday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE thursday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE thursday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE thursday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE thursday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE thursday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE thursday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE thursday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE thursday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE thursday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE thursday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE thursday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE thursday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE thursday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE thursday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE thursday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE thursday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE thursday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE thursday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE thursday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE thursday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE thursday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE thursday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE thursday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE thursday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE thursday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE thursday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE thursday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE thursday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE thursday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE thursday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE thursday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE thursday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE thursday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE thursday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE thursday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE thursday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE thursday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE thursday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE thursday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE thursday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE thursday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE thursday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE thursday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE thursday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE thursday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -99, 'mentorID' => $mentorID));
		}
		return;
	}

	public function mentor_block_friday_time($time_slot){
		$mentorID = $_SESSION['mentorID'];
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE friday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE friday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE friday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE friday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE friday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE friday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE friday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE friday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE friday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE friday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE friday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE friday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE friday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE friday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE friday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE friday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE friday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE friday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE friday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE friday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE friday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE friday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE friday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE friday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE friday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE friday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE friday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE friday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE friday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE friday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE friday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE friday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE friday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE friday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE friday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE friday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE friday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE friday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE friday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE friday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE friday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE friday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE friday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE friday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE friday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE friday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE friday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE friday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -99, 'mentorID' => $mentorID));
		}
		return;
	}

	public function mentor_block_saturday_time($time_slot){
		$mentorID = $_SESSION['mentorID'];
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE saturday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE saturday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE saturday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE saturday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE saturday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE saturday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE saturday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE saturday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE saturday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE saturday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE saturday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE saturday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE saturday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE saturday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE saturday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE saturday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE saturday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE saturday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE saturday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE saturday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE saturday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE saturday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE saturday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE saturday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE saturday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE saturday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE saturday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE saturday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE saturday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE saturday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE saturday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE saturday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE saturday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE saturday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE saturday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE saturday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE saturday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE saturday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE saturday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE saturday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE saturday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE saturday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE saturday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE saturday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE saturday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE saturday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE saturday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -99, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE saturday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -99, 'mentorID' => $mentorID));
		}
		return;
	}

	// Retrieve Mentor Schedule

	public function get_mentor_schedule(){
		
		//Get Monday Schedule
		$queryMonday = 'SELECT * FROM monday WHERE mentorID = ?';
		$mentor_schedule['monday'] = $this->db->query($queryMonday, array('mentorID' => $_SESSION['mentorID']))->row_array();;
		
		//Get Tuesday Schedule
		$queryTuesday = 'SELECT * FROM tuesday WHERE mentorID = ?';
		$mentor_schedule['tuesday'] = $this->db->query($queryTuesday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Wednesday Schedule
		$queryWednesday = 'SELECT * FROM wednesday WHERE mentorID = ?';
		$mentor_schedule['wednesday'] = $this->db->query($queryWednesday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Thursday Schedule
		$queryThursday = 'SELECT * FROM thursday WHERE mentorID = ?';
		$mentor_schedule['thursday'] = $this->db->query($queryThursday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Friday Schedule
		$queryFriday = 'SELECT * FROM friday WHERE mentorID = ?';
		$mentor_schedule['friday'] = $this->db->query($queryFriday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Saturday Schedule
		$querySaturday = 'SELECT * FROM saturday WHERE mentorID = ?';
		$mentor_schedule['saturday'] = $this->db->query($querySaturday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		return $mentor_schedule;  	
	}

	// Clear All Blackouts In Single Day

	public function clear_monday_blackout($time_slot){

		$mentorID = $_SESSION['mentorID'];

		if($time_slot == '1200AM'){
			$query = 'UPDATE monday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE monday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE monday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE monday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE monday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE monday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE monday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE monday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE monday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE monday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE monday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE monday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE monday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE monday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE monday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE monday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE monday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE monday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE monday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE monday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE monday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE monday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE monday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE monday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE monday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE monday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE monday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE monday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE monday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE monday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE monday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE monday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE monday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE monday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE monday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE monday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE monday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE monday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE monday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE monday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE monday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE monday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE monday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE monday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE monday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE monday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE monday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE monday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}

	public function clear_tuesday_blackout($time_slot){

		$mentorID = $_SESSION['mentorID'];

		if($time_slot == '1200AM'){
			$query = 'UPDATE tuesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE tuesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE tuesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE tuesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE tuesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE tuesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE tuesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE tuesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE tuesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE tuesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE tuesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE tuesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE tuesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE tuesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE tuesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE tuesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE tuesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE tuesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE tuesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE tuesday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE tuesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE tuesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE tuesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE tuesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE tuesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE tuesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE tuesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE tuesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE tuesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE tuesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE tuesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE tuesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE tuesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE tuesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE tuesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE tuesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE tuesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE tuesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE tuesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE tuesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE tuesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE tuesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE tuesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE tuesday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE tuesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE tuesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE tuesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE tuesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}

	public function clear_wednesday_blackout($time_slot){

		$mentorID = $_SESSION['mentorID'];

		if($time_slot == '1200AM'){
			$query = 'UPDATE wednesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE wednesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE wednesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE wednesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE wednesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE wednesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE wednesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE wednesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE wednesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE wednesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE wednesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE wednesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE wednesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE wednesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE wednesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE wednesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE wednesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE wednesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE wednesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE wednesday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE wednesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE wednesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE wednesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE wednesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE wednesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE wednesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE wednesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE wednesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE wednesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE wednesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE wednesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE wednesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE wednesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE wednesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE wednesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE wednesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE wednesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE wednesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE wednesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE wednesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE wednesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE wednesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE wednesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE wednesday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE wednesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE wednesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE wednesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE wednesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}

	public function clear_thursday_blackout($time_slot){

		$mentorID = $_SESSION['mentorID'];

		if($time_slot == '1200AM'){
			$query = 'UPDATE thursday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE thursday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE thursday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE thursday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE thursday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE thursday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE thursday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE thursday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE thursday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE thursday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE thursday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE thursday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE thursday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE thursday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE thursday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE thursday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE thursday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE thursday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE thursday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE thursday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE thursday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE thursday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE thursday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE thursday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE thursday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE thursday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE thursday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE thursday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE thursday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE thursday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE thursday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE thursday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE thursday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE thursday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE thursday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE thursday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE thursday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE thursday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE thursday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE thursday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE thursday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE thursday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE thursday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE thursday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE thursday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE thursday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE thursday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE thursday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}

	public function clear_friday_blackout($time_slot){

		$mentorID = $_SESSION['mentorID'];

		if($time_slot == '1200AM'){
			$query = 'UPDATE friday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE friday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE friday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE friday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE friday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE friday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE friday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE friday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE friday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE friday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE friday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE friday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE friday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE friday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE friday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE friday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE friday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE friday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE friday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE friday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE friday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE friday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE friday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE friday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE friday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE friday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE friday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE friday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE friday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE friday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE friday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE friday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE friday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE friday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE friday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE friday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE friday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE friday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE friday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE friday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE friday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE friday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE friday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE friday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE friday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE friday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE friday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE friday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}

	public function clear_saturday_blackout($time_slot){

		$mentorID = $_SESSION['mentorID'];

		if($time_slot == '1200AM'){
			$query = 'UPDATE saturday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE saturday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE saturday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE saturday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE saturday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE saturday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE saturday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE saturday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE saturday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE saturday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE saturday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE saturday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE saturday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE saturday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE saturday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE saturday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE saturday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE saturday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE saturday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930AM'){
			$query = 'UPDATE saturday SET 930AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE saturday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE saturday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE saturday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE saturday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE saturday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE saturday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE saturday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE saturday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE saturday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE saturday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE saturday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE saturday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE saturday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE saturday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE saturday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE saturday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE saturday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE saturday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE saturday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE saturday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE saturday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE saturday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE saturday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '930PM'){
			$query = 'UPDATE saturday SET 930PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('930PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE saturday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE saturday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE saturday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE saturday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}


}
?>