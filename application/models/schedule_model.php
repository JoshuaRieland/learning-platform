<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schedule_model extends CI_Model {

	
	public function get_mentor_schedule(){
		$query = 'SELECT mentorID from user WHERE id = ?';
		$mentorID = INTVAL($this->db->query($query, array('id' => $_SESSION['userID']))->row_array());
		

		//Get Monday Schedule
		$queryMonday = 'SELECT * FROM monday WHERE mentorID = ?';
		$mentor_schedule['monday'] = $this->db->query($queryMonday, array('mentorID' => $_SESSION['mentorID']))->row_array();;
		
		//Get Tuesday Schedule
		$queryTuesday = 'SELECT * FROM tuesday WHERE mentorID = ?';
		$mentor_schedule['tuesday'] = $this->db->query($queryTuesday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Wednesday Schedule
		$queryWednesday = 'SELECT * FROM wednesday WHERE mentorID = ?';
		$mentor_schedule['wednesday'] = $this->db->query($queryWednesday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Thursday Schedule
		$queryThursday = 'SELECT * FROM thursday WHERE mentorID = ?';
		$mentor_schedule['thursday'] = $this->db->query($queryThursday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Friday Schedule
		$queryFriday = 'SELECT * FROM friday WHERE mentorID = ?';
		$mentor_schedule['friday'] = $this->db->query($queryFriday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Saturday Schedule
		$querySaturday = 'SELECT * FROM saturday WHERE mentorID = ?';
		$mentor_schedule['saturday'] = $this->db->query($querySaturday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		return $mentor_schedule;  	
	}

	public function get_mentor_schedule_mentor(){

		//Get Monday Schedule
		$queryMonday = 'SELECT * FROM monday WHERE mentorID = ?';
		$mentor_schedule['monday'] = $this->db->query($queryMonday, array('mentorID' => $_SESSION['mentorID']))->row_array();;
		
		//Get Tuesday Schedule
		$queryTuesday = 'SELECT * FROM tuesday WHERE mentorID = ?';
		$mentor_schedule['tuesday'] = $this->db->query($queryTuesday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Wednesday Schedule
		$queryWednesday = 'SELECT * FROM wednesday WHERE mentorID = ?';
		$mentor_schedule['wednesday'] = $this->db->query($queryWednesday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Thursday Schedule
		$queryThursday = 'SELECT * FROM thursday WHERE mentorID = ?';
		$mentor_schedule['thursday'] = $this->db->query($queryThursday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Friday Schedule
		$queryFriday = 'SELECT * FROM friday WHERE mentorID = ?';
		$mentor_schedule['friday'] = $this->db->query($queryFriday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		//Get Saturday Schedule
		$querySaturday = 'SELECT * FROM saturday WHERE mentorID = ?';
		$mentor_schedule['saturday'] = $this->db->query($querySaturday, array('mentorID' => $_SESSION['mentorID']))->row_array();;

		return $mentor_schedule;  	
	}

	public function get_student_days(){
		$query = 'SELECT * FROM meeting_days WHERE userID = ?';
		$student_meeting_days = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $student_meeting_days;
	}

	public function meeting_days_table_check(){
		$query = 'SELECT id FROM meeting_days WHERE userID = ?';
		$id = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $id;
	}

	public function update_monday($time_slot){
		$query = 'UPDATE meeting_days SET monday = ? WHERE userID = ?';
		$this->db->query($query, array('monday' => $time_slot, 'userID' => $_SESSION['userID']));
	}

	public function update_tuesday($time_slot){
		$query = 'UPDATE meeting_days SET tuesday = ? WHERE userID = ?';
		$this->db->query($query, array('tuesday' => $time_slot, 'userID' => $_SESSION['userID']));
	}

	public function update_wednesday($time_slot){
		$query = 'UPDATE meeting_days SET wednesday = ? WHERE userID = ?';
		$this->db->query($query, array('wednesday' => $time_slot, 'userID' => $_SESSION['userID']));
	}

	public function update_thursday($time_slot){
		$query = 'UPDATE meeting_days SET thursday = ? WHERE userID = ?';
		$this->db->query($query, array('thursday' => $time_slot, 'userID' => $_SESSION['userID']));
	}

	public function update_friday($time_slot){
		$query = 'UPDATE meeting_days SET friday = ? WHERE userID = ?';
		$this->db->query($query, array('friday' => $time_slot, 'userID' => $_SESSION['userID']));
	}

	public function update_saturday($time_slot){
		$query = 'UPDATE meeting_days SET saturday = ? WHERE userID = ?';
		$this->db->query($query, array('saturday' => $time_slot, 'userID' => $_SESSION['userID']));
	}

	public function schedule_monday_meeting($userID, $mentorID, $time_slot){
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE monday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE monday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE monday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE monday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE monday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE monday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE monday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE monday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE monday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE monday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE monday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE monday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE monday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE monday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE monday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE monday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE monday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE monday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE monday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE monday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE monday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE monday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE monday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE monday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE monday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE monday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE monday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE monday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE monday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE monday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE monday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE monday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE monday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE monday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE monday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE monday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE monday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE monday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE monday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE monday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE monday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE monday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE monday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE monday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE monday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE monday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => $userID, 'mentorID' => $mentorID));
		}
		return;
	}

	public function schedule_tuesday_meeting($userID, $mentorID, $time_slot){
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE tuesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE tuesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE tuesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE tuesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE tuesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE tuesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE tuesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE tuesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE tuesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE tuesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE tuesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE tuesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE tuesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE tuesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE tuesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE tuesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE tuesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE tuesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE tuesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE tuesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE tuesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE tuesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE tuesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE tuesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE tuesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE tuesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE tuesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE tuesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE tuesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE tuesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE tuesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE tuesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE tuesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE tuesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE tuesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE tuesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE tuesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE tuesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE tuesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE tuesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE tuesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE tuesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE tuesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE tuesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE tuesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE tuesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => $userID, 'mentorID' => $mentorID));
		}

		return;
	}

	public function schedule_wednesday_meeting($userID, $mentorID, $time_slot){
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE wednesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE wednesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE wednesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE wednesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE wednesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE wednesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE wednesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE wednesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE wednesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE wednesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE wednesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE wednesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE wednesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE wednesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE wednesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE wednesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE wednesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE wednesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE wednesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE wednesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE wednesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE wednesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE wednesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE wednesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE wednesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE wednesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE wednesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE wednesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE wednesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE wednesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE wednesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE wednesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE wednesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE wednesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE wednesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE wednesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE wednesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE wednesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE wednesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE wednesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE wednesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE wednesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE wednesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE wednesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE wednesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE wednesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => $userID, 'mentorID' => $mentorID));
		}

		return;
	}

	public function schedule_thursday_meeting($userID, $mentorID, $time_slot){
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE thursday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE thursday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE thursday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE thursday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE thursday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE thursday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE thursday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE thursday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE thursday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE thursday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE thursday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE thursday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE thursday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE thursday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE thursday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE thursday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE thursday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE thursday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE thursday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE thursday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE thursday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE thursday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE thursday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE thursday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE thursday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE thursday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE thursday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE thursday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE thursday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE thursday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE thursday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE thursday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE thursday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE thursday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE thursday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE thursday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE thursday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE thursday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE thursday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE thursday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE thursday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE thursday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE thursday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE thursday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE thursday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE thursday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => $userID, 'mentorID' => $mentorID));
		}

		return;
	}

	public function schedule_friday_meeting($userID, $mentorID, $time_slot){
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE friday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE friday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE friday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE friday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE friday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE friday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE friday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE friday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE friday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE friday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE friday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE friday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE friday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE friday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE friday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE friday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE friday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE friday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE friday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE friday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE friday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE friday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE friday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE friday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE friday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE friday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE friday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE friday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE friday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE friday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE friday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE friday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE friday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE friday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE friday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE friday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE friday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE friday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE friday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE friday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE friday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE friday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE friday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE friday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE friday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE friday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => $userID, 'mentorID' => $mentorID));
		}

		return;
	}

	public function schedule_saturday_meeting($userID, $mentorID, $time_slot){
		
		if($time_slot == '1200AM'){
			$query = 'UPDATE saturday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$query = 'UPDATE saturday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$query = 'UPDATE saturday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$query = 'UPDATE saturday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$query = 'UPDATE saturday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$query = 'UPDATE saturday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$query = 'UPDATE saturday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$query = 'UPDATE saturday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$query = 'UPDATE saturday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$query = 'UPDATE saturday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$query = 'UPDATE saturday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$query = 'UPDATE saturday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$query = 'UPDATE saturday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$query = 'UPDATE saturday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$query = 'UPDATE saturday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$query = 'UPDATE saturday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$query = 'UPDATE saturday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$query = 'UPDATE saturday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$query = 'UPDATE saturday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$query = 'UPDATE saturday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$query = 'UPDATE saturday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$query = 'UPDATE saturday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$query = 'UPDATE saturday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130AM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$query = 'UPDATE saturday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$query = 'UPDATE saturday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$query = 'UPDATE saturday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$query = 'UPDATE saturday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('130PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$query = 'UPDATE saturday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('200PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$query = 'UPDATE saturday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('230PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$query = 'UPDATE saturday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('300PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$query = 'UPDATE saturday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('330PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$query = 'UPDATE saturday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('400PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$query = 'UPDATE saturday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('430PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$query = 'UPDATE saturday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('500PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$query = 'UPDATE saturday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('530PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$query = 'UPDATE saturday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('600PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$query = 'UPDATE saturday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('630PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$query = 'UPDATE saturday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('700PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$query = 'UPDATE saturday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('730PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$query = 'UPDATE saturday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('800PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$query = 'UPDATE saturday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('830PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$query = 'UPDATE saturday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('900PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$query = 'UPDATE saturday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1000PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$query = 'UPDATE saturday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1030PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$query = 'UPDATE saturday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1100PM' => $userID, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$query = 'UPDATE saturday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($query, array('1130PM' => $userID, 'mentorID' => $mentorID));
		}

		return;
	}

	public function drop_monday($time_slot){
		$queryA = 'UPDATE meeting_days SET monday = ? WHERE userID = ?';
		$this->db->query($queryA, array('monday' => 'none', 'userID' => $_SESSION['userID']));
		$mentorID = $_SESSION['mentorID'];
		if($time_slot == '1200AM'){
			$queryB = 'UPDATE monday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$queryB = 'UPDATE monday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$queryB = 'UPDATE monday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$queryB = 'UPDATE monday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$queryB = 'UPDATE monday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$queryB = 'UPDATE monday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$queryB = 'UPDATE monday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$queryB = 'UPDATE monday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$queryB = 'UPDATE monday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$queryB = 'UPDATE monday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$queryB = 'UPDATE monday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$queryB = 'UPDATE monday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$queryB = 'UPDATE monday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$queryB = 'UPDATE monday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$queryB = 'UPDATE monday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$queryB = 'UPDATE monday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$queryB = 'UPDATE monday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$queryB = 'UPDATE monday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$queryB = 'UPDATE monday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$queryB = 'UPDATE monday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$queryB = 'UPDATE monday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$queryB = 'UPDATE monday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$queryB = 'UPDATE monday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$queryB = 'UPDATE monday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$queryB = 'UPDATE monday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$queryB = 'UPDATE monday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$queryB = 'UPDATE monday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$queryB = 'UPDATE monday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$queryB = 'UPDATE monday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$queryB = 'UPDATE monday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$queryB = 'UPDATE monday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$queryB = 'UPDATE monday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$queryB = 'UPDATE monday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$queryB = 'UPDATE monday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$queryB = 'UPDATE monday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$queryB = 'UPDATE monday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$queryB = 'UPDATE monday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$queryB = 'UPDATE monday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$queryB = 'UPDATE monday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$queryB = 'UPDATE monday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$queryB = 'UPDATE monday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$queryB = 'UPDATE monday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$queryB = 'UPDATE monday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$queryB = 'UPDATE monday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$queryB = 'UPDATE monday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$queryB = 'UPDATE monday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}
	public function drop_tuesday($time_slot){
		$queryA = 'UPDATE meeting_days SET tuesday = ? WHERE userID = ?';
		$this->db->query($queryA, array('tuesday' => 'none', 'userID' => $_SESSION['userID']));
		$mentorID = $_SESSION['mentorID'];
		if($time_slot == '1200AM'){
			$queryB = 'UPDATE tuesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$queryB = 'UPDATE tuesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$queryB = 'UPDATE tuesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$queryB = 'UPDATE tuesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$queryB = 'UPDATE tuesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$queryB = 'UPDATE tuesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$queryB = 'UPDATE tuesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$queryB = 'UPDATE tuesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$queryB = 'UPDATE tuesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$queryB = 'UPDATE tuesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$queryB = 'UPDATE tuesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$queryB = 'UPDATE tuesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$queryB = 'UPDATE tuesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$queryB = 'UPDATE tuesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$queryB = 'UPDATE tuesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$queryB = 'UPDATE tuesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$queryB = 'UPDATE tuesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$queryB = 'UPDATE tuesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$queryB = 'UPDATE tuesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$queryB = 'UPDATE tuesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$queryB = 'UPDATE tuesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$queryB = 'UPDATE tuesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$queryB = 'UPDATE tuesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$queryB = 'UPDATE tuesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$queryB = 'UPDATE tuesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$queryB = 'UPDATE tuesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$queryB = 'UPDATE tuesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$queryB = 'UPDATE tuesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$queryB = 'UPDATE tuesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$queryB = 'UPDATE tuesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$queryB = 'UPDATE tuesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$queryB = 'UPDATE tuesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$queryB = 'UPDATE tuesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$queryB = 'UPDATE tuesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$queryB = 'UPDATE tuesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$queryB = 'UPDATE tuesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$queryB = 'UPDATE tuesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$queryB = 'UPDATE tuesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$queryB = 'UPDATE tuesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$queryB = 'UPDATE tuesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$queryB = 'UPDATE tuesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$queryB = 'UPDATE tuesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$queryB = 'UPDATE tuesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$queryB = 'UPDATE tuesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$queryB = 'UPDATE tuesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$queryB = 'UPDATE tuesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}
	public function drop_wednesday($time_slot){
		$queryA = 'UPDATE meeting_days SET wednesday = ? WHERE userID = ?';
		$this->db->query($queryA, array('wednesday' => 'none', 'userID' => $_SESSION['userID']));
		$mentorID = $_SESSION['mentorID'];
		if($time_slot == '1200AM'){
			$queryB = 'UPDATE wednesday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$queryB = 'UPDATE wednesday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$queryB = 'UPDATE wednesday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$queryB = 'UPDATE wednesday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$queryB = 'UPDATE wednesday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$queryB = 'UPDATE wednesday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$queryB = 'UPDATE wednesday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$queryB = 'UPDATE wednesday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$queryB = 'UPDATE wednesday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$queryB = 'UPDATE wednesday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$queryB = 'UPDATE wednesday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$queryB = 'UPDATE wednesday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$queryB = 'UPDATE wednesday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$queryB = 'UPDATE wednesday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$queryB = 'UPDATE wednesday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$queryB = 'UPDATE wednesday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$queryB = 'UPDATE wednesday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$queryB = 'UPDATE wednesday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$queryB = 'UPDATE wednesday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$queryB = 'UPDATE wednesday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$queryB = 'UPDATE wednesday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$queryB = 'UPDATE wednesday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$queryB = 'UPDATE wednesday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$queryB = 'UPDATE wednesday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$queryB = 'UPDATE wednesday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$queryB = 'UPDATE wednesday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$queryB = 'UPDATE wednesday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$queryB = 'UPDATE wednesday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$queryB = 'UPDATE wednesday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$queryB = 'UPDATE wednesday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$queryB = 'UPDATE wednesday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$queryB = 'UPDATE wednesday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$queryB = 'UPDATE wednesday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$queryB = 'UPDATE wednesday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$queryB = 'UPDATE wednesday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$queryB = 'UPDATE wednesday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$queryB = 'UPDATE wednesday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$queryB = 'UPDATE wednesday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$queryB = 'UPDATE wednesday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$queryB = 'UPDATE wednesday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$queryB = 'UPDATE wednesday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$queryB = 'UPDATE wednesday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$queryB = 'UPDATE wednesday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$queryB = 'UPDATE wednesday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$queryB = 'UPDATE wednesday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$queryB = 'UPDATE wednesday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}
	public function drop_thursday($time_slot){
		$queryA = 'UPDATE meeting_days SET thursday = ? WHERE userID = ?';
		$this->db->query($queryA, array('thursday' => 'none', 'userID' => $_SESSION['userID']));
		$mentorID = $_SESSION['mentorID'];
		if($time_slot == '1200AM'){
			$queryB = 'UPDATE thursday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$queryB = 'UPDATE thursday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$queryB = 'UPDATE thursday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$queryB = 'UPDATE thursday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$queryB = 'UPDATE thursday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$queryB = 'UPDATE thursday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$queryB = 'UPDATE thursday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$queryB = 'UPDATE thursday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$queryB = 'UPDATE thursday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$queryB = 'UPDATE thursday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$queryB = 'UPDATE thursday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$queryB = 'UPDATE thursday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$queryB = 'UPDATE thursday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$queryB = 'UPDATE thursday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$queryB = 'UPDATE thursday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$queryB = 'UPDATE thursday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$queryB = 'UPDATE thursday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$queryB = 'UPDATE thursday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$queryB = 'UPDATE thursday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$queryB = 'UPDATE thursday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$queryB = 'UPDATE thursday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$queryB = 'UPDATE thursday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$queryB = 'UPDATE thursday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$queryB = 'UPDATE thursday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$queryB = 'UPDATE thursday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$queryB = 'UPDATE thursday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$queryB = 'UPDATE thursday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$queryB = 'UPDATE thursday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$queryB = 'UPDATE thursday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$queryB = 'UPDATE thursday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$queryB = 'UPDATE thursday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$queryB = 'UPDATE thursday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$queryB = 'UPDATE thursday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$queryB = 'UPDATE thursday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$queryB = 'UPDATE thursday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$queryB = 'UPDATE thursday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$queryB = 'UPDATE thursday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$queryB = 'UPDATE thursday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$queryB = 'UPDATE thursday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$queryB = 'UPDATE thursday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$queryB = 'UPDATE thursday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$queryB = 'UPDATE thursday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$queryB = 'UPDATE thursday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$queryB = 'UPDATE thursday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$queryB = 'UPDATE thursday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$queryB = 'UPDATE thursday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}
	public function drop_friday($time_slot){
		$queryA = 'UPDATE meeting_days SET friday = ? WHERE userID = ?';
		$this->db->query($queryA, array('friday' => 'none', 'userID' => $_SESSION['userID']));
		$mentorID = $_SESSION['mentorID'];
		if($time_slot == '1200AM'){
			$queryB = 'UPDATE friday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$queryB = 'UPDATE friday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$queryB = 'UPDATE friday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$queryB = 'UPDATE friday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$queryB = 'UPDATE friday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$queryB = 'UPDATE friday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$queryB = 'UPDATE friday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$queryB = 'UPDATE friday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$queryB = 'UPDATE friday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$queryB = 'UPDATE friday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$queryB = 'UPDATE friday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$queryB = 'UPDATE friday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$queryB = 'UPDATE friday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$queryB = 'UPDATE friday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$queryB = 'UPDATE friday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$queryB = 'UPDATE friday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$queryB = 'UPDATE friday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$queryB = 'UPDATE friday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$queryB = 'UPDATE friday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$queryB = 'UPDATE friday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$queryB = 'UPDATE friday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$queryB = 'UPDATE friday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$queryB = 'UPDATE friday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$queryB = 'UPDATE friday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$queryB = 'UPDATE friday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$queryB = 'UPDATE friday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$queryB = 'UPDATE friday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$queryB = 'UPDATE friday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$queryB = 'UPDATE friday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$queryB = 'UPDATE friday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$queryB = 'UPDATE friday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$queryB = 'UPDATE friday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$queryB = 'UPDATE friday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$queryB = 'UPDATE friday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$queryB = 'UPDATE friday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$queryB = 'UPDATE friday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$queryB = 'UPDATE friday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$queryB = 'UPDATE friday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$queryB = 'UPDATE friday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$queryB = 'UPDATE friday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$queryB = 'UPDATE friday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$queryB = 'UPDATE friday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$queryB = 'UPDATE friday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$queryB = 'UPDATE friday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$queryB = 'UPDATE friday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$queryB = 'UPDATE friday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}
	public function drop_saturday($time_slot){
		$queryA = 'UPDATE meeting_days SET saturday = ? WHERE userID = ?';
		$this->db->query($queryA, array('saturday' => 'none', 'userID' => $_SESSION['userID']));
		$mentorID = $_SESSION['mentorID'];
		if($time_slot == '1200AM'){
			$queryB = 'UPDATE saturday SET 1200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230AM'){
			$queryB = 'UPDATE saturday SET 1230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100AM'){
			$queryB = 'UPDATE saturday SET 100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130AM'){
			$queryB = 'UPDATE saturday SET 130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200AM'){
			$queryB = 'UPDATE saturday SET 200AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230AM'){
			$queryB = 'UPDATE saturday SET 230AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300AM'){
			$queryB = 'UPDATE saturday SET 300AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330AM'){
			$queryB = 'UPDATE saturday SET 330AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400AM'){
			$queryB = 'UPDATE saturday SET 400AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430AM'){
			$queryB = 'UPDATE saturday SET 430AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500AM'){
			$queryB = 'UPDATE saturday SET 500AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530AM'){
			$queryB = 'UPDATE saturday SET 530AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600AM'){
			$queryB = 'UPDATE saturday SET 600AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630AM'){
			$queryB = 'UPDATE saturday SET 630AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700AM'){
			$queryB = 'UPDATE saturday SET 700AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730AM'){
			$queryB = 'UPDATE saturday SET 730AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800AM'){
			$queryB = 'UPDATE saturday SET 800AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830AM'){
			$queryB = 'UPDATE saturday SET 830AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900AM'){
			$queryB = 'UPDATE saturday SET 900AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000AM'){
			$queryB = 'UPDATE saturday SET 1000AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030AM'){
			$queryB = 'UPDATE saturday SET 1030AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100AM'){
			$queryB = 'UPDATE saturday SET 1100AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130AM'){
			$queryB = 'UPDATE saturday SET 1130AM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130AM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1200PM'){
			$queryB = 'UPDATE saturday SET 1200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1230PM'){
			$queryB = 'UPDATE saturday SET 1230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '100PM'){
			$queryB = 'UPDATE saturday SET 100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '130PM'){
			$queryB = 'UPDATE saturday SET 130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('130PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '200PM'){
			$queryB = 'UPDATE saturday SET 200PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('200PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '230PM'){
			$queryB = 'UPDATE saturday SET 230PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('230PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '300PM'){
			$queryB = 'UPDATE saturday SET 300PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('300PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '330PM'){
			$queryB = 'UPDATE saturday SET 330PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('330PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '400PM'){
			$queryB = 'UPDATE saturday SET 400PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('400PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '430PM'){
			$queryB = 'UPDATE saturday SET 430PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('430PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '500PM'){
			$queryB = 'UPDATE saturday SET 500PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('500PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '530PM'){
			$queryB = 'UPDATE saturday SET 530PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('530PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '600PM'){
			$queryB = 'UPDATE saturday SET 600PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('600PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '630PM'){
			$queryB = 'UPDATE saturday SET 630PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('630PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '700PM'){
			$queryB = 'UPDATE saturday SET 700PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('700PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '730PM'){
			$queryB = 'UPDATE saturday SET 730PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('730PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '800PM'){
			$queryB = 'UPDATE saturday SET 800PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('800PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '830PM'){
			$queryB = 'UPDATE saturday SET 830PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('830PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '900PM'){
			$queryB = 'UPDATE saturday SET 900PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('900PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1000PM'){
			$queryB = 'UPDATE saturday SET 1000PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1000PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1030PM'){
			$queryB = 'UPDATE saturday SET 1030PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1030PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1100PM'){
			$queryB = 'UPDATE saturday SET 1100PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1100PM' => -1, 'mentorID' => $mentorID));
		}
		if($time_slot == '1130PM'){
			$queryB = 'UPDATE saturday SET 1130PM = ? WHERE mentorID = ?';
			$this->db->query($queryB, array('1130PM' => -1, 'mentorID' => $mentorID));
		}
		return;
	}
}
