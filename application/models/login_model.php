<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login_model extends CI_Model {

	public function getLoginCredentials($email){
		$query = 'SELECT * FROM user WHERE email = ?';
		$login_credentials = $this->db->query($query, array('email' => $email))->row_array();
		return $login_credentials;
	}

	public function updatePassword($password_info){
		$query = 'UPDATE user SET password = ?, salt = ? WHERE id = ?';
		$updated = $this->db->query($query, array('password' => $password_info['password'], 'salt' => $password_info['salt'], 'id' => $_SESSION['userID']));
		return $updated;
	}
	
}
