<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Register_model extends CI_Model {

	public function new_student_register($register_info){
		$query = 'INSERT INTO user (email, password, access, program_length, start_date) VALUES (?,?,?,?,?)';
		$this->db->query($query, array('email' => $register_info['email'],
									   'password' => $register_info['randPW'],
									   'access' => 'student',
									   'program_length' => $register_info['program_length'],
									   'start_date' => $register_info['start_date'])
										);
		$query1 = 'SELECT id FROM user WHERE email = ?';
		$userID = $this->db->query($query1, array('email' => $register_info['email']))->row_array();
		$query2 = 'INSERT INTO meeting_days (userID) VALUES (?)';
		$this->db->query($query2, array('userID' => $userID['id']));
		$query3 = 'INSERT INTO student_info (userID) VALUES (?)';
		$this->db->query($query3, array('userID' => $userID['id']));
		$query4 = 'INSERT INTO quiz_grades (userID) VALUES (?)';
		$this->db->query($query4, array('userID' => $userID['id']));
		return $userID;
	}



	//---------- STEP 1 QUERIES -----------//

	public function step_1($step_1_info){
		$query = 'INSERT INTO student_info (userID, first_name, last_name, gender, age, city, state, zip_code) VALUES (?,?,?,?,?,?,?,?)';
		$registered = $this->db->query($query, array('userID' => $_SESSION['userID'],'first_name' => $step_1_info['first_name'], 'last_name' => $step_1_info['last_name'], 'gender' => $step_1_info['gender'], 'age' => $step_1_info['age'], 'city' => $step_1_info['city'], 'state' => $step_1_info['state'], 'zip_code' => $step_1_info['zip_code']));
		return $registered;
	}

	public function update_step_1($step_1_info){
		$query = 'UPDATE student_info SET first_name = ?, last_name = ?, gender = ?, age = ?, city = ?, state = ?, zip_code = ? WHERE id = ?';
		// die(var_dump($query));
		$registered = $this->db->query($query, array('first_name' => $step_1_info['first_name'], 'last_name' => $step_1_info['last_name'], 'gender' => $step_1_info['gender'], 'age' => $step_1_info['age'], 'city' => $step_1_info['city'], 'state' => $step_1_info['state'], 'zip_code' => $step_1_info['zip_code'], 'id' => $step_1_info['studentID']));
		return $registered;
	}

	public function complete_step_1(){
		$query = 'UPDATE user SET registration_step1 = ?, registration_step2 = ? WHERE id = ?';
		$completed = $this->db->query($query, array('registration_step_1' => 'completed', 'registration_step_2' => 'current', 'id' => $_SESSION['userID']));
		return $completed;
	}

	public function update_step1_check(){
		$query = 'SELECT id FROM student_info WHERE userID = ?';
		$update_student = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $update_student;
	}

	public function update_step1_mentor_check(){
		$query = 'SELECT id FROM mentor_info WHERE userID = ?';
		$update_mentor = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $update_mentor;
	}

	public function insert_mentor_info($step_1_mentor_info){
		$query = 'INSERT INTO mentor_info (userID, first_name, last_name, gender, age, city, state, zip_code) VALUES (?,?,?,?,?,?,?,?)';
		$registered = $this->db->query($query, array('userID' => $step_1_mentor_info['userID'], 'first_name' => $step_1_mentor_info['first_name'], 'last_name' => $step_1_mentor_info['last_name'], 'gender' => $step_1_mentor_info['gender'], 'age' => $step_1_mentor_info['age'], 'city' => $step_1_mentor_info['city'], 'state' => $step_1_mentor_info['state'], 'zip_code' => $step_1_mentor_info['zip_code']));
		return $registered;
	}

	public function update_mentor_info($id, $step_1_mentor_info){
		$query = 'UPDATE mentor_info SET first_name = ?, last_name = ?, gender = ?, age = ?, city = ?, state = ?, zip_code = ? WHERE id = ?';
		$registered = $this->db->query($query, array('first_name' => $step_1_mentor_info['first_name'], 'last_name' => $step_1_mentor_info['last_name'], 'gender' => $step_1_mentor_info['gender'], 'age' => $step_1_mentor_info['age'], 'city' => $step_1_mentor_info['age'], 'state' => $step_1_mentor_info['state'], 'zip_code' => $step_1_mentor_info['zip_code'], 'id' => $id));
		return $registered;
	}

	public function update_profile_pic($photo_name){
		$query = 'UPDATE student_info SET profile_pic = ? WHERE userID = ?';
		$this->db->query($query, array('profile_pic' => $photo_name, 'userID' => $_SESSION['userID']));
		return;	
	}

	public function update_profile_pic_mentor($photo_name){
		$query = 'UPDATE mentor_info SET profile_pic = ? WHERE userID = ?';
		$this->db->query($query, array('profile_pic' => $photo_name, 'userID' => $_SESSION['userID']));
		return;	
	}

	public function get_student_info(){
		$query = 'SELECT * FROM student_info WHERE userID = ?';
		$student_info = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $student_info;
	}


	//---------- STEP 2 QUERIES -----------//
					  

	public function get_all_mentors(){
		$query = 'SELECT * FROM mentor_info WHERE student_count < 20';
		$all_mentors = $this->db->query($query)->result_array();
		return $all_mentors;
	}

	public function mentor_assigned_check(){
		$query = 'SELECT mentorID FROM user WHERE id = ?';
		$mentor_assigned = $this->db->query($query, array('id' => $_SESSION['userID']))->row_array();
		return $mentor_assigned;
	}

	public function step_2($step_2_info){
		$query = 'UPDATE user SET mentorID = ? WHERE id = ?';
		$registered = $this->db->query($query, array('mentorID' => $step_2_info['mentorID'], 'id' => $_SESSION['userID']));
		return $registered;
	}

	public function complete_step_2(){
		$query = 'UPDATE user SET registration_step2 = ?, registration_step3 = ? WHERE id = ?';
		$completed = $this->db->query($query, array('registration_step2' => 'completed', 'registration_step3' => 'current', 'id' => $_SESSION['userID']));
		return $completed;
	}

	public function get_current_mentor(){
		$query = 'SELECT mentorID from user WHERE id =?';
		$current_mentor = $this->db->query($query, array('id' => $_SESSION['userID']))->row_array();
		return INTVAL($current_mentor);
	}

	public function get_mentor_info(){
		$query = 'SELECT * FROM mentor_info WHERE id = ?';
		$mentor_info = $this->db->query($query, array('id' => $_SESSION['mentorID']))->row_array();
		return $mentor_info;
	}

	public function step_2_get_mentor_counts($mentor){
		$query = 'SELECT id, student_count FROM mentor_info WHERE id = ?';
		$mentor_info['current'] = $this->db->query($query, array('id' => $mentor['current']))->row_array();
		$mentor_info['new'] = $this->db->query($query, array('id' => $mentor['new']))->row_array();
		return $mentor_info;
	}	

	public function update_mentor_counts($id, $count){
		$query = 'UPDATE mentor_info SET student_count = ? WHERE id = ?';
		$updated = $this->db->query($query, array('student_count' => $count, 'id' => $id));
		if($updated){
			return $updated;
		}else{die('Error With DB Connection');}
	}

	public function mentor_complete_registration(){
		$query = 'UPDATE user SET registration_step2 = ? WHERE id = ?';
	}

	//---------- STEP 3 QUERIES ----------//

	public function get_mentor_id(){
		$query = 'SELECT id FROM mentor_info WHERE userID = ?';
		$mentor_id = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $mentor_id;
	}

	public function complete_step_3(){
		$query = 'UPDATE user SET registration_step3 = ?, registration_step4 = ? WHERE id = ?';
		$completed = $this->db->query($query, array('registration_step3' => 'completed', 'registration_step4' => 'current', 'id' => $_SESSION['userID']));
		return $completed;
	}

	//---------- STEP 4 QUERIES ----------//

	public function complete_step_4(){
		$query = 'UPDATE user SET registration_step4 = ?, registration_step5 = ? WHERE id = ?';
		$completed = $this->db->query($query, array('registration_step4' => 'completed', 'registration_step5' => 'current', 'id' => $_SESSION['userID']));
		return $completed;
	}

	//---------- STEP 5 QUERIES ----------//

	public function complete_step_5(){
		$query = 'UPDATE user SET registration_step5 = ? WHERE id = ?';
		$completed = $this->db->query($query, array('registration_step5' => 'completed', 'id' => $_SESSION['userID']));
		return $completed;
	}

}
