<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Student_model extends CI_Model {

	// STUDENT MESSAGING

	public function send_message($message){
		$query = 'INSERT INTO message_log (send_to_id, send_from_id, subject, content, attachment, viewed, created_at) VALUES (?,?,?,?,?,?,NOW())';
		$this->db->query($query, array('send_to_id' => $message['send_to_id'], 'send_from_id' => $message['send_from_id'], 'subject' => $message['subject'], 'content' => $message['content'], 'attachment' => $message['attachment'], 'viewed' => $message['viewed']));
		return;
	}

	public function get_inbox(){
		$query = 'SELECT * FROM message_log WHERE send_to_id = ? ORDER BY created_at';
		$inbox = $this->db->query($query, array('send_to_id' => $_SESSION['userID']))->result_array();
		return $inbox;
	}

	public function get_outbox(){
		$query = 'SELECT * FROM message_log WHERE send_from_id = ? ORDER BY created_at';
		$outbox = $this->db->query($query, array('send_from_id' => $_SESSION['userID']))->result_array();
		return $outbox;
	}

	public function get_mentor_userID($mentorID){
		$query = 'SELECT userID FROM mentor_info WHERE id = ?';
		$mentor_userID = $this->db->query($query, array('id' => $mentorID))->row_array();
		return $mentor_userID;
	}

	public function message_viewed($info){
		$query = 'UPDATE message_log SET viewed = ? WHERE id = ?';
		$this->db->query($query, array('viewed' => 'yes', 'id' => INTVAL($info)));
		return;
	}

	public function message_reply($reply){
		$query = 'INSERT INTO message_log (send_to_id, send_from_id, subject, content, attachment, viewed, created_at) VALUES (?,?,?,?,?,?, NOW())';
		$this->db->query($query, array('send_to_id' => $reply['send_to_id'], 'send_from_id' => $reply['send_from_id'], 'subject' => $reply['subject'], 'content' => $reply['content'], 'attachment' => 'no', 'viewed' => 'no'));
		$return;
	}
	
}
