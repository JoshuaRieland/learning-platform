<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Course_model extends CI_Model {


	public function get_subject_list(){
		$query = 'SELECT subject, subject_title FROM lesson_page GROUP BY subject ORDER BY subject';
		$subject_list = $this->db->query($query)->result_array();
		return $subject_list;
	}

	public function get_all_chapters(){
		$query = 'SELECT subject, subject_title, chapter, chapter_title FROM lesson_page GROUP BY subject,chapter ORDER BY subject, chapter';
		$all_chapters = $this->db->query($query)->result_array();
		return $all_chapters;
	}

	public function get_all_lessons(){
		$query = 'SELECT subject, subject_title, chapter, chapter_title, lesson, lesson_title FROM lesson_page GROUP BY subject, chapter, lesson ORDER BY subject, chapter, lesson';
		$all_lessons = $this->db->query($query)->result_array();
		return $all_lessons;
	}

	public function get_one_chapter($subject, $chapter){
		$query = 'SELECT * FROM lesson_page WHERE subject = ? AND chapter = ? AND lesson = ? ORDER BY position';
		$chapter_content = $this->db->query($query, array('subject' => $subject, 'chapter' => $chapter, 'lesson' => 1))->result_array();
		return $chapter_content;

	}

	public function get_one_lesson($subject, $chapter, $lesson){
		$query = 'SELECT * FROM lesson_page WHERE subject = ? AND chapter = ? AND lesson = ? ORDER BY position';
		$chapter_content = $this->db->query($query, array('subject' => $subject, 'chapter' => $chapter, 'lesson' => $lesson))->result_array();
		return $chapter_content;
	}

	public function get_chapter_lessons($subject, $chapter){
		$query = 'SELECT subject, chapter, lesson, lesson_title FROM lesson_page WHERE subject = ? AND chapter = ? GROUP BY lesson';
		$chapter_lessons = $this->db->query($query, array('subject' => $subject, 'chapter' => $chapter))->result_array();
		return $chapter_lessons;
	}
	
	public function get_quiz($subject, $chapter, $lesson){
		$query = 'SELECT * FROM quiz WHERE subject = ? AND chapter = ? AND lesson = ? ORDER BY id';
		$quiz = $this->db->query($query, array('subject' => $subject, 'chapter' => $chapter, 'lesson' => $lesson))->result_array();
		return $quiz;
	}

	public function quiz_grade($grade, $quiz_lesson){
		$query = "UPDATE quiz_grades SET " .$quiz_lesson. " = ? WHERE userID = ?";
		$this->db->query($query, array($quiz_lesson => $grade, 'userID' => $_SESSION['userID']));
		return;
	}

	public function get_quiz_score($quiz_lesson){
		$query = "SELECT " .$quiz_lesson. " FROM quiz_grades WHERE userID = ?";
		$quiz_score = $this->db->query($query, array('userID' => $_SESSION['userID']))->row_array();
		return $quiz_score;
	}

}
